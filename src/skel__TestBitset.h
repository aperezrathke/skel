//****************************************************************************
// skel__TestBitset.h
//****************************************************************************

/**
 * Commandlet for testing basic bit set operations
 *
 * Usage:
 * -test_bitset
 */

#ifdef skel__TestBitset_h
#   error "Bitset Test included multiple times!"
#endif  // skel__TestBitset_h
#define skel__TestBitset_h

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_TESTS

#include "skel__FlatBitsetArray.h"
#include "skel__Logf.h"
#include "skel__PooledBitsetArray.h"

#include "skel__Assert.h"
#include "skel__CmdOptsMap.h"
#include "skel__ExitCodes.h"

namespace {
    template <typename t_BitsetArray>
    int do_bitset_test(const char* str_type) {
        // The number of bits for a single bit set
        const size_t NUM_BITS = 700;

        // The total number of bit sets in the collection
        const size_t NUM_BITSETS = 1000;

        // A collection of bit sets
        t_BitsetArray array(NUM_BITS, NUM_BITSETS);

        // Test copy construction
        typename t_BitsetArray::bitset_handle_t handle_(array[0]);

        // A reference bit set to test against
        skel__Bitset reference_bitset(NUM_BITS);

        // Test that the number of bits for the array matches parameter
        skel__Assert(array.num_bits() == NUM_BITS);

        // Test that the number of bit sets for the array matches parameter
        skel__Assert(array.size() == NUM_BITSETS);

        // Indices of bits to flip to true
        const size_t BITS_TO_SET_TRUE[] = {0,   1,   15,  16,  67,  149, 257,
                                           258, 352, 491, 492, 517, 601, 655,
                                           656, 657, 696, 697, 698, 699};

        // Total number of bits to set to true
        const size_t NUM_BITS_TO_SET_TRUE =
            sizeof(BITS_TO_SET_TRUE) / sizeof(size_t);

        // Set bits to true in reference bit set as well as pooled bit set
        size_t i = 0;
        for (i = 0; i < NUM_BITS_TO_SET_TRUE; ++i) {
            reference_bitset.set(BITS_TO_SET_TRUE[i]);
            array[0].set(BITS_TO_SET_TRUE[i]);
        }

        // Test that pooled bit set matches reference bit set
        skel__Assert(array[0] == reference_bitset);
        skel__Assert(handle_ == reference_bitset);

        // Test that a pooled bit set doesn't match reference bit set
        skel__Assert(array[1] != reference_bitset);

        // Test that assignment of pooled bit sets match
        array[1] = array[0];
        skel__Assert(array[1] == array[0]);

        // Test that assigned pooled bit set matches reference
        skel__Assert(array[1] == reference_bitset);

        // Test operator |= between two pooled bit sets
        // Bit sets should be initialized to all zeros
        array[2] |= array[1];
        skel__Assert(array[2] == array[1]);
        skel__Assert(array[2] != array[3]);

        // Test operator &= between two pooled bit sets
        // Bit sets should be initialized to all zeros
        array[3] &= array[1];
        skel__Assert(array[3] == array[4]);
        skel__Assert(array[3] != array[1]);
        skel__Assert(array[3] != reference_bitset);

        // Access last element
        array[NUM_BITSETS - 1] = array[0];

        // Test iteration over a pooled bit set
        skel__Assert(array[0] == reference_bitset);
        skel__Assert(handle_ == reference_bitset);
        i = 0;
        for (size_t ix_bit = array[0].find_first();
             ix_bit != skel__PooledBitsetArray::npos;
             ix_bit = array[0].find_next(ix_bit)) {
            skel__Logf("--bit found at: %d\n", (int)ix_bit);
            skel__Assert(i < NUM_BITS_TO_SET_TRUE);
            skel__Assert(ix_bit == BITS_TO_SET_TRUE[i]);
            skel__Assert(array[1].test(ix_bit));
            skel__Assert(reference_bitset.test(ix_bit));
            ++i;
        }

        // Test conversion to a dynamic_bitset
        skel__Bitset copy_from_pooled(NUM_BITS);
        array[0].to_hard_bitset(copy_from_pooled);
        skel__Assert(copy_from_pooled == reference_bitset);
        skel__Assert(reference_bitset == copy_from_pooled);

        // Test expected count matches
        skel__Assert(reference_bitset.count() == NUM_BITS_TO_SET_TRUE);
        skel__WrappedBitset wrapped_bitset(SKEL___WRAP_BITSET(reference_bitset));
        skel__Assert(wrapped_bitset.count() == NUM_BITS_TO_SET_TRUE);
        skel__Logf("reference bit count: %d\n", (int)(reference_bitset.count()));
        skel__Logf("wrapped bit count: %d\n", (int)(wrapped_bitset.count()));

        skel__Logf("Bit set test finished for %s.\n", str_type);
        return skel__ExitCode_normal;
    }
}  // namespace

/**
 * Entry point for test
 */
int skel__TestBitsetMain(const skel__CmdOptsMap& cmd_opts) {
    const int rc_pooled =
        do_bitset_test<skel__PooledBitsetArray>("pooled array");
    const int rc_flat = do_bitset_test<skel__FlatBitsetArray>("flat array");
    return ((rc_pooled == rc_flat) && (rc_pooled == skel__ExitCode_normal))
               ? skel__ExitCode_normal
               : skel__ExitCode_error;
}

#endif  // SKEL___BUILD_ENABLE_TESTS
