//****************************************************************************
// skel__Filesystem.h
//****************************************************************************

/**
 * @brief Wraps boost or std filesystem library
 */

#ifndef skel__Filesystem_h
#define skel__Filesystem_h

#include "skel__Build.h"

#include <string>

/**
 * Defer to boost filesystem, this may later be swapped to std namespace
 */
#include <boost/filesystem.hpp>
#define skel__Fs boost::filesystem

/**
 * Creates any directories needed for parameter path
 * @param dir_path - path to directory
 * @return result of skel__Fs::create_directories(...) for dir_path
 */
extern bool skel__Fs_create_dirs(const skel__Fs::path& dir_path);

/**
 * Creates any parent directories needed for parameter file path
 * @param file_path - path to file
 * @return result of skel__Fs::create_directories(...) for parent directories
 */
extern bool skel__Fs_create_parent_dirs(const skel__Fs::path& file_path);

/**
 * Creates lexically normal (removes extra path separators and dots) directory
 * path without trailing path separator slash(es)
 * @param dir_path - directory path to normalize
 */
extern void skel__Fs_normalize_dir_path(skel__Fs::path& dir_path);
extern void skel__Fs_normalize_dir_path(std::string& dir_path);

#endif  // skel__Filesystem_h
