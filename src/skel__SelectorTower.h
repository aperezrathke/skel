//****************************************************************************
// skel__SelectorTower.h
//****************************************************************************

/**
 * @brief - Samples a vector element according to the discrete probability
 *  distribution defined by the vector
 */

#ifndef skel__SelectorTower_h
#define skel__SelectorTower_h

#include "skel__Build.h"
#include "skel__Assert.h"
#include "skel__Rand.h"
#include "skel__Types.h"

/**
 * Basic idea:
 *  1) Compute CDF of empirical distribution
 *  2) Generate a uniform random variable U in [0,1)
 *  3) Select first element with CDF <= U
 */
class skel__SelectorTower {
public:
    /**
     * @param p_select - a vector of probabilities - must sum to 1.0 and must
     *  have at least 1 element
     * @param rng - handle to random number generator
     * @return an index [0, size-1] where each index i has probability
     *  p_select[i] of being chosen
     */
    static inline skel__UInt rand_select_index(const skel__VecCol& p_select,
                                               skel__Rand& rng) {
        const skel__UInt n_choices = SKEL___TO_UINT(p_select.n_elem);
        skel__AssertRealEq(skel__MatrixUtils::sum(p_select),
                           SKEL___TO_REAL(1.0));
        skel__Assert(n_choices > 0);

        // Uniformly select a real number in [0, 1)
        const skel__Real r_choice = rng.unif_real();
        skel__Assert(r_choice <= skel__MatrixUtils::sum(p_select));

        // Used to identify which bucket r_choice falls within
        skel__Real p_sum = SKEL___TO_REAL(0.0);

        // Determine which index to select
        for (skel__UInt i = 0; i < (n_choices - 1); ++i) {
            p_sum += p_select.at(i);
            if (r_choice <= p_sum) {
                // Element found, return it
                return i;
            }
        }

        // Else, last element is chosen
        skel__Assert(r_choice <= (p_sum + p_select.at(n_choices - 1)));
        return (n_choices - 1);
    }

    /**
     * @param p_select - a vector of possibly unnormalized probabilities - must
     *  sum to 'z' and must have at least 1 element
     * @param z - the normalizing constant = sum(p_select)
     * @param rng - handle to random number generator
     * @return an index [0, size-1] where each index i has probability
     *  p_select[i] / z of being chosen
     */
    static inline skel__UInt rand_select_index(const skel__VecCol& p_select,
                                               const skel__Real z,
                                               skel__Rand& rng) {
        const skel__UInt n_choices = SKEL___TO_UINT(p_select.n_elem);
        skel__AssertRealEq(skel__MatrixUtils::sum(p_select), z);
        skel__Assert(n_choices > 0);

        // Uniformly select a real number in [0, z)
        const skel__Real r_choice = rng.unif_real(0, z);
        skel__Assert(r_choice <= z);

        // Used to identify which bucket r_choice falls within
        skel__Real p_sum = SKEL___TO_REAL(0.0);

        // Determine which index to select
        for (skel__UInt i = 0; i < (n_choices - 1); ++i) {
            p_sum += p_select.at(i);
            if (r_choice <= p_sum) {
                // Element found, return it
                return i;
            }
        }

        // Else, last element is chosen
        skel__Assert(r_choice <= (p_sum + p_select.at(n_choices - 1)));
        return (n_choices - 1);
    }
};

#endif  // skel__SelectorTower_h
