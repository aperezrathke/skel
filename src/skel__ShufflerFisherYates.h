//****************************************************************************
// skel__ShufflerFisherYates.h
//****************************************************************************

/**
 * @brief - Generates unbiased permutations
 */

/**
 * @WARNING - THIS UTILITY WILL NOT WORK WITH 0-LENGTH VECTORS! THE CLIENT
 *  IS RESPONSIBLE FOR PASSING IN NON-EMPTY VECTORS. THERE ARE ASSERTS TO
 *  TO CATCH THIS EDGE CASE BUT ASSERTS ARE NOT ALWAYS ENABLED - SUCH AS
 *  FOR RELEASE BUILDS.
 */

/**
 * Note, template argument 't_ivec_arma' assumes an integer vector type that
 *  has interface consistent with the Armadillo matrix library:
 *
 *  http://arma.sourceforge.net/
 */

#ifndef skel__ShufflerFisherYates_h
#define skel__ShufflerFisherYates_h

#include "skel__Build.h"
#include "skel__Assert.h"
#include "skel__Rand.h"
#include "skel__Types.h"

#include <algorithm>

/**
 * Utility for generating a Fisher-Yates shuffle. See:
 *   https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
 *   https://eli.thegreenplace.net/2010/05/28/the-intuition-behind-fisher-yates-shuffling/
 *   https://stackoverflow.com/questions/9345087/choose-m-elements-randomly-from-a-vector-containing-n-elements
 * A Fisher-Yates shuffle generates an unbiased permutation. In other words,
 * all permutations are equally likely. This algorithm is also more efficient
 * when you only require a partial permutation of an index set.
 */
class skel__ShufflerFisherYatesCore {
public:
    /**
     * Generates a sorted index vector from [0, n-1]. In other words, the
     * output vector will have elements in order: 0 1 ... (n-1)
     * @param out - unsigned integer vector for storing sorted index set
     * @param n - the index range to sort [0, (n-1)]
     */
    template <typename t_ivec_arma = skel__UIVecCol>
    inline static void get_sorted_indices(t_ivec_arma& out_sorted,
                                          const skel__UInt n) {
        skel__Assert(n > SKEL___TO_UINT(0));
        out_sorted =
            skel__MatrixUtils::linspace<t_ivec_arma>(SKEL___TO_MAT_SZ_T(0),
                                                     SKEL___TO_MAT_SZ_T(n - 1),
                                                     SKEL___TO_MAT_SZ_T(n));
        skel__Assert(out_sorted.is_sorted());
        skel__Assert(out_sorted.n_elem == SKEL___TO_MAT_SZ_T(n));
    }

    /**
     * Performs complete permutation of all elements
     * @param out_perm - output permuted index vector
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = skel__UIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm, skel__Rand& rng) {
        skel__Assert(out_perm.n_elem > SKEL___TO_MAT_SZ_T(0));
        shuffle(out_perm,
                SKEL___TO_UINT(0),
                SKEL___TO_UINT(out_perm.n_elem - 1),
                SKEL___TO_UINT(out_perm.n_elem - 1),
                rng);
    }

    /**
     * Performs partial permutation of first 'm' elements. Considers all
     * elements of candidates for the permutation.
     * @param out_perm - output permuted index vector up to first m elements
     * @param m - only the first 'm' elements will be permuted where
     *  0 < m <= n == out_perm.n_elem
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = skel__UIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm,
                               const skel__UInt m,
                               skel__Rand& rng) {
        skel__AssertBoundsInc(
            m, SKEL___TO_UINT(1), SKEL___TO_UINT(out_perm.n_elem));
        shuffle(out_perm,
                SKEL___TO_UINT(0),
                m - SKEL___TO_UINT(1),
                SKEL___TO_UINT(out_perm.n_elem - 1),
                rng);
    }

    /**
     * Performs partial permutation of elements in range [ix_min, ix_max] with
     * all elements in [ix_min, ix_pool] as possible candidates and the
     * following inequalities hold: ix_min <= ix_max and ix_max <= ix_pool
     * @param out_perm - output permuted index vector, only elements in range
     *  [ix_min, ix_max] are considered permuted; however, elements in
     *  [ix_max+1, ix_pool] may be modified but are not considered as part of
     *  final permutation.
     * @param ix_min - lower bound of range to permute, must be <= ix_max
     * @param ix_max - upper bound of range to permute, must be >= ix_min and
     *  <= ix_pool
     * @param ix_pool - upper bound of candidate elements considered for
     *  permuting, considered elements are those in range [ix_min, ix_pool],
     *  must be >= ix_max
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = skel__UIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm,
                               const skel__UInt ix_min,
                               const skel__UInt ix_max,
                               const skel__UInt ix_pool,
                               skel__Rand& rng) {
        skel__Assert(out_perm.n_elem > SKEL___TO_MAT_SZ_T(0));
        skel__AssertBounds(
            ix_min, SKEL___TO_UINT(0), SKEL___TO_UINT(out_perm.n_elem));
        skel__AssertBounds(
            ix_max, SKEL___TO_UINT(0), SKEL___TO_UINT(out_perm.n_elem));
        skel__AssertBounds(
            ix_pool, SKEL___TO_UINT(0), SKEL___TO_UINT(out_perm.n_elem));
        skel__Assert(ix_min <= ix_max);
        skel__Assert(ix_max <= ix_pool);
        skel__Assert((ix_pool - ix_min) > SKEL___TO_UINT(0));
        for (skel__UInt i = ix_min; i <= ix_max; ++i) {
            const skel__UInt ix_swap = rng.unif_int(i, ix_pool);
            skel__AssertBoundsInc(ix_swap, i, ix_pool);
            std::swap(out_perm.at(SKEL___TO_MAT_SZ_T(i)),
                      out_perm.at(SKEL___TO_MAT_SZ_T(ix_swap)));
        }
    }
};

/**
 * Encapsulated Fisher-Yates object, maintains internal index buffers
 */
template <typename t_ivec_arma = skel__UIVecCol>
class skel__ShufflerFisherYates {
public:
    /**
     * Default constructor - uninitialized!
     */
    skel__ShufflerFisherYates() {}

    /**
     * Constructor
     * @param n - number of indices that need shuffling
     */
    explicit skel__ShufflerFisherYates(const skel__UInt n)
        : sorted(n), perm(n) {
        skel__Assert(n > SKEL___TO_UINT(0));
        skel__ShufflerFisherYatesCore::get_sorted_indices(sorted, n);
    }

    /**
     * Initialize shuffler with 'n' elements, where n > 0
     * @param n - number of indices that may be shuffled
     */
    void init(const skel__UInt n) {
        skel__Assert(n > SKEL___TO_UINT(0));
        sorted.set_size(n);
        perm.set_size(n);
        skel__ShufflerFisherYatesCore::get_sorted_indices(sorted, n);
    }

    /**
     * Obtain partially shuffled index set, compute shuffle for first 'm'
     * elements only (with all elements as possible candidates)
     * @param m - first 0 < m <= n elements of index set will be shuffled
     * @param rng - random number generator
     * @return index set with first m elements shuffled
     */
    const t_ivec_arma& shuffle(const skel__UInt m, skel__Rand& rng) {
        skel__Assert(m > SKEL___TO_UINT(0));
        skel__Assert(m <= SKEL___TO_UINT(sorted.n_elem));
        perm = sorted;
        skel__ShufflerFisherYatesCore::shuffle(perm, m, rng);
        return this->get_view(m);
    }

    /**
     * @WARNING - DO NOT CALL THIS METHOD UNLESS YOU KNOW WHAT YOUR ARE DOING!
     * @return View of internal permutation buffer as size 'm'
     */
    inline const t_ivec_arma& get_view(const skel__UInt m) {
        skel__Assert(m > SKEL___TO_UINT(0));
        skel__Assert(m <= SKEL___TO_UINT(perm.n_elem));
        // return aliased view for vector of length 'm'
        new (&perm_view) t_ivec_arma(perm.memptr(),
                                     SKEL___TO_MAT_SZ_T(m),
                                     false, /* copy */
                                     true); /* strict*/
        return perm_view;
    }

private:
    // Sorted index set with elements [0 to n-1]
    t_ivec_arma sorted;
    // Scratch buffer for permuting sorted index set
    t_ivec_arma perm;
    // Aliased view of permuted buffer of size m
    t_ivec_arma perm_view;
};

#endif  // skel__ShufflerFisherYates_h
