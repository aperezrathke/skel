//****************************************************************************
// skel__Config.h
//
// Class for unifying command line and file (eg INI) user options
//****************************************************************************

#ifndef skel__Config_h
#define skel__Config_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__CmdOptsMap.h"
#include "skel__ConfigFile.h"
#include "skel__Opts.h"
#include "skel__SmartPtr.h"
#include "skel__Types.h"

#include <string>
#include <vector>

/**
 * Reads and stores users configuration parameters.
 *
 * Provides interface so that rest of application does not have to worry about
 * whether its options are coming from the command line or from a INI file
 */
class skel__Config
    : public skel__SmartPtr::enable_shared_from_this<skel__Config> {
public:
    /**
     * Typedefs
     */
    typedef skel__SmartPtr::shared_ptr<skel__Config> skel__SpConfig_t;
    typedef skel__SmartPtr::shared_ptr<const skel__Config>
        skel__SpConstConfig_t;
    typedef skel__SmartPtr::weak_ptr<skel__Config> skel__WpConfig_t;
    typedef skel__SmartPtr::weak_ptr<const skel__Config> skel__WpConstConfig_t;

    //************************************************************************
    // Public methods
    //************************************************************************

    /**
     * Clears configuration options
     */
    void clear();

    /**
     * Initializes from command line
     *
     * @param cmd_opts - command line options
     * @param should_clear - If TRUE, erases existing state in this object
     */
    void init(const skel__CmdOptsMap& cmd_opts,
              const skel__Bool should_clear = skel__TRUE);

    /**
     * Outputs configuration options to stdout
     */
    void print() const;

    /**
     * Search for value mapped to option key
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param out_val - The output value mapped to parameter opt_key,
     *      unmodified if opt_key does not exist in command options map
     * @param ix - The option identifier
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    template <class T>
    skel__Bool read_into(T& out_val,
                         const skel__OptE ix,
                         const skel__Bool check_parent = skel__TRUE) const {
        // Check command line first
        if (m_cmd_opts.read_into(out_val, skel__Opt_get_cmd_switch(ix))) {
            return skel__TRUE;
        }
        // Else check INI file
        else if (m_cfg_file.read_into(out_val, skel__Opt_get_ini_key(ix))) {
            return skel__TRUE;
        }
        // Else check INI archetype
        else if (m_cfg_arch.read_into(out_val, skel__Opt_get_ini_key(ix))) {
            return skel__TRUE;
        }
        // Else check the parent configuration
        else if (check_parent) {
            skel__SpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->read_into(
                    out_val, ix, skel__TRUE /*check_parent*/);
            }
        }

        // Key not found, return false
        return skel__FALSE;
    }

    /**
     * @WARNING - DOES NOT PERFORM PATH RESOLUTION!
     * @out_val - Searches for first parameter value mapped to a matching key
     *  in 'ixs' array where all command line matches have precedence over INI
     * @param ixs - Set of option keys to search for mapped value, the key
     *  priority is as follows: All command line variants have higher priority
     *  than INI, then earlier indices within array have priority over later
     * @param check_parent - If true, then parent configuration (if valid) will
     *  be recursively checked for the set of option keys
     * @return index of option read or ixs.size() if not found
     */
    size_t read_into(std::string& out_val,
                     const std::vector<skel__OptE>& ixs,
                     const skel__Bool check_parent = skel__TRUE) const;

    /**
     * PERFORMS PATH RESOLUTION!
     * @out_val - Searches for first parameter value mapped to a matching key
     *  in 'ixs' array where all command line matches have precedence over INI
     * @param ixs - Set of option keys to search for mapped value, the key
     *  priority is as follows: All command line variants have higher priority
     *  than INI, then earlier indices within array have priority over later
     * @param should_resolve - Parallel vector of same size as 'ixs' with i-th
     *  element TRUE if option is a path that must be resolved, FALSE o/w
     * @param check_parent - If true, then parent configuration (if valid) will
     *  be recursively checked for the set of option keys
     * @return index of option read or ixs.size() if not found
     */
    size_t read_into(std::string& out_val,
                     const std::vector<skel__OptE>& ixs,
                     const std::vector<skel__Bool>& should_resolve,
                     const skel__Bool check_parent = skel__TRUE) const;

    /**
     * Search for value mapped to option vector key. Vector options are of INI
     * format: <opt_name>_<off> = <value> where 'off' is a non-negative integer
     * index. This method returns a SINGLE value mapped to the option offset.
     * Vector options are assumed to use 0-based indexing and be declared
     * sequentially (no skips).
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param out_val - The output value mapped to parameter opt_key,
     *      unmodified if opt_key does not exist in command options map
     * @param ix - The option prefix identifier
     * @param off - The option offset
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    template <class T>
    skel__Bool read_into_vec(T& out_val,
                             const skel__OptE ix,
                             const skel__UInt off,
                             const skel__Bool check_parent = skel__TRUE) const {
        // Check command line first
        const std::string suffix(std::string("_") + skel__2Str(off));
        if (m_cmd_opts.read_into(out_val,
                                 skel__Opt_get_cmd_switch(ix) + suffix)) {
            return skel__TRUE;
        }
        // Else check INI file
        /* else */
        if (m_cfg_file.read_into(out_val, skel__Opt_get_ini_key(ix) + suffix)) {
            return skel__TRUE;
        }
        // Else check INI archetype
        /* else */
        if (m_cfg_arch.read_into(out_val, skel__Opt_get_ini_key(ix) + suffix)) {
            return skel__TRUE;
        }
        // Else check the parent configuration
        /* else */
        if (check_parent) {
            skel__SpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->read_into_vec(
                    out_val, ix, off, skel__TRUE /*check_parent*/);
            }
        }

        // Key not found, return false
        return skel__FALSE;
    }

    /**
     * Obtain path value resolved according to the rules (in order of
     * precedence)
     *  - if not found, path argument is unmodified
     *  - if from command line, output path is unmodified
     *  - if from config file and absolute, output path is unmodified
     *  - if from config file and relative, output path is resolved relative
     *      to config file directory
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param path - Output path, unmodified if key not found
     * @param ix - Option identifier
     * @return TRUE if key exists, FALSE otherwise. If key exists and value
     *  is from command line, then output path argument is same as command
     *  line. However, if key exists and value is from configuration file,
     *  then relative paths are resolved to be relative to the configuration
     *  file directory. Note that command line values override any
     *  configuration file values.
     */
    skel__Bool resolve_path(std::string& path,
                            const skel__OptE ix,
                            const skel__Bool check_parent = skel__TRUE) const;

    /**
     * Resolve path for vectorized options
     * @param path - Output path obtained using resolve_path() semantics
     * @param ix - Option identifier
     * @param off - The option offset
     * @return TRUE if key exists, FALSE o/w
     */
    skel__Bool resolve_path_vec(
        std::string& path,
        const skel__OptE ix,
        const skel__UInt off,
        const skel__Bool check_parent = skel__TRUE) const;

    /**
     * @param ix - The option identifier
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    inline skel__Bool key_exists(
        const skel__OptE ix,
        const skel__Bool check_parent = skel__TRUE) const {
        // Check command line first
        if (m_cmd_opts.key_exists(skel__Opt_get_cmd_switch(ix))) {
            return skel__TRUE;
        }
        // Else check INI file
        else if (m_cfg_file.key_exists(skel__Opt_get_ini_key(ix))) {
            return skel__TRUE;
        }
        // Else check INI archetype
        else if (m_cfg_arch.key_exists(skel__Opt_get_ini_key(ix))) {
            return skel__TRUE;
        }
        // Else, check the parent configuration
        else if (check_parent) {
            skel__SpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->key_exists(ix, skel__TRUE /*check_parent*/);
            }
        }
        // Key not found, return false
        return skel__FALSE;
    }

    /**
     * Allow clients to set options programmatically as well. Note: all
     * options are cleared if init() is called with should_clear = TRUE.
     *
     * @param ix - The option identifier
     * @param val - The value to assign
     */
    template <typename t_>
    void set_option(const skel__OptE ix, const t_& val) {
        // Store in command line options as those always take priority
        m_cmd_opts.set_option(skel__Opt_get_cmd_switch(ix), val);
    }

    /**
     * Overload for char*
     * Allow clients to set options programmatically as well. Note: all
     * options are cleared if init() is called with should_clear = TRUE.
     *
     * @param ix - The option identifier
     * @param val - The value to assign
     */
    void set_option(const skel__OptE ix, const char* val) {
        // Store in command line options as those always take priority
        m_cmd_opts.set_option(skel__Opt_get_cmd_switch(ix), std::string(val));
    }

    /**
     * Set option if not already set (can be used for setting defaults)
     * @param ix - The option identifier
     * @param val - The value to assign
     * @return TRUE if option was set, FALSE otherwise (e.g. if option already
     *  specified)
     */
    template <typename t_>
    inline skel__Bool set_option_if_absent(const skel__OptE ix, const t_& val) {
        if (!key_exists(ix)) {
            // Option was not previously set
            set_option(ix, val);
            // Option was successfully set
            return skel__TRUE;
        }
        // Option was not set as it exists already
        return skel__FALSE;
    }

    /**
     * @return pointer to child configuration
     */
    inline skel__SpConfig_t get_child() const { return m_child; };

    /**
     * Overwrite child configuration
     * @param child - child configuration
     */
    void set_child(skel__SpConfig_t child);

private:
    /**
     * Internal print adjusted for child level
     */
    void print(const skel__UInt level) const;

    /**
     * Checks if child configuration should be initialized
     */
    void conditional_init_child();

    /**
     * Actual initialization for a child configuration
     * @param parent - The parent configuration
     * @param cfg_file - Child INI files
     * Note: child command line options are extracted from parent command line
     */
    void init(skel__SpConstConfig_t parent, const skel__ConfigFile& cfg_file);

    /**
     * Command line options mapping:
     * Used for storing <key=string, value=string> tuples obtained from
     * command line
     */
    skel__CmdOptsMap m_cmd_opts;

    /**
     * INI file options mapping:
     * Used for storing <key=string, value=string> tuples obtained from INI
     * configuration file
     */
    skel__ConfigFile m_cfg_file;

    /**
     * INI file archetype:
     * Settings not found in m_cfg_file will be deferred to archetype
     */
    skel__ConfigFile m_cfg_arch;

    /**
     * Pointer to possible parent configuration. If a key,value pair is
     * missing in this config, the parent is checked for the value.
     * Using weak_ptr to prevent circular reference issues.
     */
    skel__WpConstConfig_t m_parent;

    /**
     * The parent configuration owns the memory for any child configurations.
     * If all parent configurations are destroyed, then the child
     * configuration will be destroyed as well.
     * Note: the parent configuration does not defer to the child
     * configuration for any key, value pairs. However, client code may
     * utilize the child configurations for initializing sub-simulations of
     * the client program.
     */
    skel__SpConfig_t m_child;
};

/**
 * Typedefs
 */
typedef skel__Config::skel__SpConfig_t skel__SpConfig_t;
typedef skel__Config::skel__WpConfig_t skel__WpConfig_t;
typedef skel__Config::skel__SpConstConfig_t skel__SpConstConfig_t;
typedef skel__Config::skel__WpConstConfig_t skel__WpConstConfig_t;

#endif  // skel__Config_h
