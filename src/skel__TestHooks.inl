//****************************************************************************
// skel__TestHooks.inl
//****************************************************************************

/**
 * General work flow for adding a test commandlet:
 * 1) Create a new commandlet header file skel__Test<name>.h
 * 2) Within test commandlet header file, follow header guard and build flags
 *      policy as in other test commandlets:
 *      - throw error if header included multiple times
 *      - wrap code in #ifdef SKEL___BUILD_ENABLE_TESTS
 * 3) Within test commandlet header file, wrap utility methods and class in
 *      anonymous namespace
 * 4) Within test commandlet header file, define skel__Test<name>Main(opts)
 *      method (outside of anonymous namespace)
 * 5) In skel__Opts.h, within the test commandlet options demarcated by build flag,
 *      add skel__Opt_test_<name> to the set of enums
 * 6) In skel__Opts.cpp, in the section of SKEL___OPT_INFOS demarcated by build flag,
 *      add line DECLARE_CMD_UNARY_OPT("test_<name>", "<description>").
 *      - make sure this is in same order as enum structure in skel__Opts.h
 * 7.) In skel__TestHooks.inl, include test commandlet header file
 * 8.) In skel__TestHooks.inl, add to the GAppInfo structure the test commandlet
 *      details
 */

// Not meant to be included more than once
#ifdef skel__TestHooks_inl
#   error "Test Hooks included multiple times!"
#endif // skel__TestHooks_inl
#define skel__TestHooks_inl

// Implementation of test commandlet hooks extracted to separate file to avoid
// cluttering skel__Main.cpp

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_TESTS

#include "skel__AppHooks.h"
#include "skel__ExitCodes.h"

// Include additional tests here
#include "skel__TestBitset.h"
#include "skel__TestCsppline.h"
#include "skel__TestDirichletDistribution.h"
#include "skel__TestHash.h"
#include "skel__TestGlobalInit.h"
#ifdef SKEL___BUILD_ENABLE_THREADS
#   include "skel__TestParallelMapper.h"
#endif // SKEL___BUILD_ENABLE_THREADS
#include "skel__TestRng.h"

// Register test commandlets here
static struct skel__AppInfo GTests[] =
{
      { skel__Opt_get_cmd_switch(skel__Opt_test_bitset), &skel__TestBitsetMain }
    , { skel__Opt_get_cmd_switch(skel__Opt_test_csppline), &skel__TestCspplineMain }
    , { skel__Opt_get_cmd_switch(skel__test_dir_distrib), &skel__TestDirichletDistributionMain }
    , { skel__Opt_get_cmd_switch(skel__Opt_test_global_init), &skel__TestGlobalInitMain }
    , { skel__Opt_get_cmd_switch(skel__Opt_test_hash), &skel__TestHashMain }
    , { skel__Opt_get_cmd_switch(skel__Opt_test_rng), &skel__TestRngMain }
#ifdef SKEL___BUILD_ENABLE_THREADS
    , { skel__Opt_get_cmd_switch(skel__Opt_test_parallel_mapper), &skel__TestParallelMapperMain }
#endif // SKEL___BUILD_ENABLE_THREADS
};

// Tests are enabled, route to the proper hook:
// Parse command line and run any tests specified
#define SKEL___CONDITIONAL_RUN_TESTS( cmd_opts ) SKEL___CONDITIONAL_RUN_APPS( cmd_opts, GTests )

#else

// Tests are not enabled, strip away call to hook
#define SKEL___CONDITIONAL_RUN_TESTS( cmd_opts ) ((void)0)

#endif // SKEL___BUILD_ENABLE_TESTS
