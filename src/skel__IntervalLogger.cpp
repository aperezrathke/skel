//****************************************************************************
// skel__IntervalLogger.cpp
//****************************************************************************

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_INTERVAL_LOGGING

#include "skel__IntervalLogger.h"

namespace skel__Stats {
    /**
     * The static descriptions used for interval logging
     */
    const char* GCounterIntervalLoggerDescs[skel__STAT_IntervalLoggerMax] = {
          "Total execution time:"
    };

} // end of skel__Stats namespace

#endif  // SKEL___BUILD_ENABLE_INTERVAL_LOGGING
