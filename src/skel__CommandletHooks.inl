//****************************************************************************
// skel__CommandletHooks.inl
//****************************************************************************

/**
 * General work flow for adding a commandlet:
 * 1) Create a new commandlet header file skel__Cmdlet<name>.h
 * 2) Within commandlet header file, follow header guard and build flags
 *      policy as in other commandlets:
 *      - throw error if header included multiple times
 *      - wrap code in #ifdef SKEL___BUILD_ENABLE_COMMANDLETS
 * 3) Within commandlet header file, wrap utility methods and class in
 *      anonymous namespace
 * 4) Within commandlet header file, define skel__Cmdlet<name>Main(opts) method
 *      (outside of anonymous namespace)
 * 5) In skel__Opts.h, within the commandlet options demarcated by build flag, add
 *      skel__Opt_cmdlet_<name> to the set of enums
 * 6) In skel__Opts.cpp, in the section of SKEL___OPT_INFOS demarcated by build flag,
 *      add line DECLARE_CMD_UNARY_OPT("cmdlet_<name>", "<description>").
 *      - make sure this is in same order as enum structure in skel__Opts.h
 * 7.) In skel__CommandletHooks.inl, include commandlet header file
 * 8.) In skel__CommandletHooks.inl, add to the GAppInfo structure the commandlet
 *      details
 */

// Not meant to be included more than once
#ifdef skel__CommandletHooks_inl
#   error "Commandlet Hooks included multiple times!"
#endif // skel__CommandletHooks_inl
#define skel__CommandletHooks_inl

// Implementation of commandlet hooks extracted to separate file to avoid
// cluttering skel__Main.cpp

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_COMMANDLETS

#include "skel__AppHooks.h"
#include "skel__ExitCodes.h"

// Include additional commandlets here

// Include printf
#include <stdio.h>

// Placeholder main that dumps information about project
// Usage:
// -cmdlet_info
static int skel__CmdletInfoMain(const skel__CmdOptsMap& cmd_opts) {
    printf("Application framework built with SKELeton C++ utilities.\n");
    return skel__ExitCode_normal;
}

// Register commandlets here
static struct skel__AppInfo GCommandlets[] = {
      { skel__Opt_get_cmd_switch(skel__Opt_cmdlet_info), &skel__CmdletInfoMain }
};

// Commandlets are enabled, route to the proper commandlet hook:
// Parse command line and run any commandlets specified
#define SKEL___CONDITIONAL_RUN_COMMANDLETS(cmd_opts) \
    SKEL___CONDITIONAL_RUN_APPS(cmd_opts, GCommandlets)

#else

// Commandlets are not enabled, strip away call to commandlet hook
#define SKEL___CONDITIONAL_RUN_COMMANDLETS( cmd_opts ) ((void)0)

#endif // SKEL___BUILD_ENABLE_COMMANDLETS
