//****************************************************************************
// skel__TestParallelMapper.h
//****************************************************************************

/**
 * Commandlet for testing simple usage of a parallel mapper
 *
 * Usage:
 * -test_parallel_mapper
 */

#ifdef skel__TestParallelMapper_h
#   error "Test Parallel Mapper included multiple times!"
#endif  // skel__TestParallelMapper_h
#define skel__TestParallelMapper_h

#include "skel__Build.h"

#if defined(SKEL___BUILD_ENABLE_TESTS) && defined(SKEL___BUILD_ENABLE_THREADS)

#include "skel__CmdOptsMap.h"
#include "skel__ExitCodes.h"
#include "skel__Logf.h"
#include "skel__Thread.h"

namespace {
    class skel__TestMappableWork : public skel__MappableWork {
    public:
        /**
         * Assigns an array for processing
         */
        explicit skel__TestMappableWork(std::vector<std::string>& array_to_process)
            : m_cached_size(SKEL___TO_UINT(array_to_process.size())),
              m_array_to_process(array_to_process) {}

        /**
         * Defines what work is to be performed for the parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         * @return TRUE if work still to be done, FALSE if finished
         */
        virtual skel__Bool do_range(const skel__UInt ix_start,
                                    const skel__UInt count,
                                    const skel__UInt thread_id) {
            const skel__UInt end = std::min(ix_start + count, m_cached_size);
            for (skel__UInt i = ix_start; i < end; ++i) {
                skel__Logf("THREAD %u: (%u, %u) -> %s\n",
                           thread_id,
                           i,
                           count,
                           m_array_to_process[i].c_str());
            }

            return end != m_cached_size;
        }

    private:
        const skel__UInt m_cached_size;
        std::vector<std::string>& m_array_to_process;
    };
}  // namespace

/**
 * Entry point for test
 */
int skel__TestParallelMapperMain(const skel__CmdOptsMap& cmd_opts) {
    skel__Logf("Running Test Parallel Mapper.\n");

    skel__ParallelMapper pm;
    pm.init(SKEL___DEFAULT_NUM_THREADS);
    pm.log_status();

    skel__Logf("\n-------------------\nFirst test:\n");

    std::vector<std::string> data;
    data.push_back("0-This");
    data.push_back("1-is");
    data.push_back("2-a");
    data.push_back("3-test");
    data.push_back("4-of");
    data.push_back("5-the");
    data.push_back("6-parallel");
    data.push_back("7-mapper.");
    data.push_back("8-We");
    data.push_back("9-are");
    data.push_back("10-printing");
    data.push_back("11-out");
    data.push_back("12-some");
    data.push_back("13-words");
    data.push_back("14-to");
    data.push_back("15-observe");
    data.push_back("16-how");
    data.push_back("17-well");
    data.push_back("18-the");
    data.push_back("19-threads");
    data.push_back("20-run.");

    skel__TestMappableWork work1(data);

    pm.wait_until_work_finished(work1, 5 /* inc_by */);

    skel__Logf("\n-------------------\nSecond test:\n");

    data.push_back("21-Let's");
    data.push_back("22-try");
    data.push_back("23-this");
    data.push_back("24-again.");

    skel__TestMappableWork work2(data);

    pm.wait_until_work_finished(work2, 3 /* inc_by */);

    skel__Logf("\n-------------------\nWork has finished. Exiting.\n");

    return skel__ExitCode_normal;
}

#endif  // SKEL___BUILD_ENABLE_TESTS && SKEL___BUILD_ENABLE_THREADS
