//****************************************************************************
// skel__TestHash.h
//****************************************************************************

/**
 * Commandlet for testing the hash interface
 *
 * Usage:
 * -test_hash
 */

#ifdef skel__TestHash_h
#   error "Test Hash included multiple times!"
#endif  // skel__TestHash_h
#define skel__TestHash_h

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_TESTS

#include "skel__Assert.h"
#include "skel__CmdOptsMap.h"
#include "skel__ExitCodes.h"
#include "skel__HashMap.h"
#include "skel__Types.h"

#include <iostream>
#include <limits>

/**
 * Entry point for test
 */
int skel__TestHashMain(const skel__CmdOptsMap& cmd_opts) {
    // Hash map from key = unsigned integer to value = unsigned integer
    typedef SKEL___HASH_MAP<unsigned int, unsigned int> skel__UIntToUIntMap;
    skel__UIntToUIntMap hash_map;
    typedef skel__UIntToUIntMap::key_type key_t;
    typedef skel__UIntToUIntMap::value_type::second_type val_t;

    std::cout << "Running Test Hash." << std::endl;

    const skel__UInt MIN_SIZE = 12000;
    SKEL___HASH_RESERVE(hash_map, MIN_SIZE);
    std::cout << "Starting bucket count: " << hash_map.bucket_count()
              << std::endl;
    skel__Assert(hash_map.bucket_count() >= MIN_SIZE);

    const key_t KEYS[] = {
        2,
        4,
        101,
        0xDEADBEEF,
        std::numeric_limits<skel__UIntToUIntMap::key_type>::max(),
        std::numeric_limits<skel__UIntToUIntMap::key_type>::max() - 1};

    const val_t VALS[] = {100, 200, 5000, 7331, 0xDEADBEEF, 0x8BADF00D};

    const skel__UInt N = sizeof(KEYS) / sizeof(key_t);
    skel__Assert((sizeof(VALS) / sizeof(val_t)) == N);

    skel__UInt i = 0;
    for (i = 0; i < N; ++i) {
        hash_map[KEYS[i]] = VALS[i];
        std::cout << "Inserted (" << KEYS[i] << ", " << hash_map[KEYS[i]] << ")"
                  << std::endl;
    }

    for (i = 0; i < N; ++i) {
        skel__Assert(hash_map.find(KEYS[i]) != hash_map.end());
    }

    skel__Assert(hash_map.find(0x1CEB00DA) == hash_map.end());

    std::cout << "Ending bucket count: " << hash_map.bucket_count()
              << std::endl;
    skel__Assert(hash_map.bucket_count() >= MIN_SIZE);

    return skel__ExitCode_normal;
}

#endif  // SKEL___BUILD_ENABLE_TESTS
