//****************************************************************************
// skel__ParallelMapper.h
//****************************************************************************

/**
 * @brief Utility calls the same operation on each element of an array but in
 * parallel among a parameter number of threads. It's like Map-Reduce but
 * without the reduce.
 */

#ifndef skel__ParallelMapper_h
#define skel__ParallelMapper_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_THREADS

#include "skel__Thread.h"
#include "skel__Types.h"

//****************************************************************************
// skel__MappableWork
//****************************************************************************

/**
 * Interface for an array to be processed in parallel
 */
class skel__MappableWork {
public:
    /**
     * Virtual destructor
     */
    virtual ~skel__MappableWork() {}

    /**
     * Defines what work is to be performed for the parameter range
     * @param ix_start - the starting index to perform work at
     * @param count - the number of work units to perform beginning at
     *  ix_start
     * @param thread_id - a unique calling thread identifier
     * @return TRUE if work still to be done, FALSE if finished
     */
    virtual skel__Bool do_range(const skel__UInt ix_start,
                                const skel__UInt count,
                                const skel__UInt thread_id) = 0;
};

//****************************************************************************
// skel__MapperThread
//****************************************************************************

class skel__ParallelMapper;

/**
 * Wrapper around system thread.
 */
class skel__MapperThread {
public:
    /**
     * Constructs mapper thread
     * @param mapper - owning mapper pool
     * @param thread_id - a unique identifier for this thread
     */
    skel__MapperThread(skel__ParallelMapper& mapper,
                       const skel__UInt thread_id);

    /**
     * Destructor
     */
    ~skel__MapperThread();

    /**
     * Thread entry point - analogous to process main
     */
    void thread_main();

    /**
     * Our thread identifier
     */
    const skel__UInt m_id;

private:
    // Prohibit copy and assignment
    skel__MapperThread(const skel__MapperThread&);
    skel__MapperThread& operator=(const skel__MapperThread&);

    /**
     * @return TRUE if thread should exit, FALSE o/w
     */
    skel__Bool should_exit() const;

    /**
     * Waits until signaled to start
     * Also notifies mapper thread when all workers have become idle
     */
    void wait_until_work_ready() const;

    /**
     * Keeps grabbing chunks of work until none is available
     */
    void do_available_work() const;

    /**
     * Parent mapper owning this thread
     */
    skel__ParallelMapper& m_mapper;

    /**
     * The actual thread object
     */
    skel__Thread_t* mp_sys_thread;
};

//****************************************************************************
// skel__ParallelMapper
//****************************************************************************

class skel__ParallelMapper {

public:
    /**
     * Default constructor
     */
    skel__ParallelMapper();

    /**
     * Destructor
     */
    ~skel__ParallelMapper();

    /**
     * Can only be called once, initializes thread pool
     */
    void init(const skel__UInt num_threads);

    /**
     * Address the following issue:
     * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
     * Basically on windows, if threads are still "alive" before main thread
     * exits, then the program will hang. Therefore, we need to destroy our
     * threads prior to main() exiting.
     */
    void teardown();

    /**
     * Processes work object and blocks calling thread until finished.
     * @param work - the work to process in parallel
     * @param inc_by - the chunk size of each work unit processed by a thread
     */
    void wait_until_work_finished(skel__MappableWork& work,
                                  const skel__UInt inc_by);

    /**
     * Outputs mapper status
     */
    void log_status() const;

private:
    // Prohibit copy and assignment
    skel__ParallelMapper(const skel__ParallelMapper&);
    skel__ParallelMapper& operator=(const skel__ParallelMapper&);

    /**
     * @return TRUE if threads should exit, FALSE o/w
     */
    skel__Bool exiting() const;

    /**
     * Barrier method - waits until worker threads have
     * blocked on condition variable used for work
     * available notification. This helps avoid race
     * conditions that could otherwise occur if work
     * becomes available before worker threads block
     * (which would cause cond. var::notify_all() to be
     * called before worker thread is waiting on it, which
     * when worker thread does finally call wait, will cause
     * the thread to block instead, which leads to deadlock
     * as main thread is counting on workers to decrement
     * m_num_active_workers to 0).
     */
    void wait_until_all_workers_idle(skel__Lock_t& lk);

    /**
     * The work currently being undertaken - client is responsible for
     * managing this memory - the mapper does not delete this work!
     */
    skel__MappableWork* mp_work;

    /**
     * The amount to increment the iteration counter by. Essentially defines
     * the size of a work package a thread performs before attempting to get
     * the next work package.
     */
    skel__AtomicUInt m_inc_by;

    /**
     * An atomic counter - allows atomic operations such as incrementing and
     * decrementing the counter value. This counter is used for iterating over
     * an array of work.
     */
    skel__AtomicUInt m_iter;

    /**
     * Atomic counter used for keeping track of which threads have outstanding
     * work. It is the responsibility of the threads to update this
     * counter when they complete and then notify parent mapper of this event.
     */
    skel__AtomicUInt m_num_active_workers;

    /**
     * Counter to signal that threads should be exiting
     */
    skel__AtomicUInt m_should_exit;

    /**
     * Mutex used for synchronizing access to condition variables
     */
    skel__Mutex_t m_mutex;

    /**
     * Used for notifying threads that work is ready to be processed
     */
    skel__ConditionVariable_t m_cv_work_begin;

    /**
     * Used for notifying the main thread that all worker threads have finished
     */
    skel__ConditionVariable_t m_cv_all_workers_idle;

    /**
     * Our pool of worker threads
     */
    std::vector<skel__MapperThread*> m_pool;

    /**
     * Give threads direct access
     */
    friend class skel__MapperThread;
};

#endif  // SKEL___BUILD_ENABLE_THREADS

#endif  // skel__ParallelMapper_h
