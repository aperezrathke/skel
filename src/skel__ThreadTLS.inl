//****************************************************************************
// skel__ThreadTLS.inl
//****************************************************************************

/**
 * Used for (re)setting TLS accessor macros based on build conditions
 */

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_THREADS

// Wrapping multi-line macros in do { ... } while(0) so that they behave
// similar to single-line statements.
// http://www.pixelstech.net/article/1390482950-do-%7B-%7D-while-%280%29-in-macros

/**
 * Undefine any previously existing macros
 */

#ifdef SKEL___DECLARE_TLS_DATA
#   undef SKEL___DECLARE_TLS_DATA
#endif  // SKEL___DECLARE_TLS_DATA

#ifdef SKEL___ACCESS_TLS_DATA
#   undef SKEL___ACCESS_TLS_DATA
#endif  // SKEL___ACCESS_TLS_DATA

#ifdef SKEL___INIT_TLS_DATA_0_PARAM
#   undef SKEL___INIT_TLS_DATA_0_PARAM
#endif  // SKEL___INIT_TLS_DATA_0_PARAM

#ifdef SKEL___INIT_TLS_DATA_FOR_EACH_1_PARAM
#   undef SKEL___INIT_TLS_DATA_FOR_EACH_1_PARAM
#endif  // SKEL___INIT_TLS_DATA_FOR_EACH_1_PARAM

#ifdef SKEL___INIT_TLS_DATA_FOR_EACH_2_PARAM
#   undef SKEL___INIT_TLS_DATA_FOR_EACH_2_PARAM
#endif  // SKEL___INIT_TLS_DATA_FOR_EACH_2_PARAM

#ifdef SKEL___TLS_DATA_FOR_EACH_0_PARAM
#   undef SKEL___TLS_DATA_FOR_EACH_0_PARAM
#endif  // SKEL___TLS_DATA_FOR_EACH_0_PARAM

#define SKEL___DECLARE_TLS_DATA(type, name) std::vector<type> name

#define SKEL___ACCESS_TLS_DATA(name) name[SKEL___THREAD_ID_VAR_NAME]

/**
 * @Warning
 * TLS Data is allocated under the assumption that the worker threads and main
 * thread are never running/accessing TLS at the same time. If this is ever
 * *NOT* the case - then we must allocate an extra slot for the main thread,
 * else it just reuses the TLS data allocated for the 0th worker thread.
 *
 * Also, if main and worker are accessing TLS, then the thread ids of the
 * worker threads must start from 1 (and not 0).
 */

#define SKEL___INIT_TLS_DATA_0_PARAM(name)                             \
    do {                                                               \
        name.clear();                                                  \
        skel__Assert(skel__G::SKEL___NUM_WORKER_THREADS_VAR_NAME > 0); \
        name.resize(skel__G::SKEL___NUM_WORKER_THREADS_VAR_NAME);      \
    } while (0)

#define SKEL___INIT_TLS_DATA_FOR_EACH_1_PARAM(name, memb_func, param_1) \
    do {                                                                \
        SKEL___INIT_TLS_DATA_0_PARAM(name);                             \
        for (skel__UInt it_tls_ = 0;                                    \
             it_tls_ < skel__G::SKEL___NUM_WORKER_THREADS_VAR_NAME;     \
             ++it_tls_) {                                               \
            name[it_tls_].memb_func(param_1);                           \
        }                                                               \
    } while (0)

#define SKEL___INIT_TLS_DATA_FOR_EACH_2_PARAM(                      \
    name, memb_func, param_1, param_2)                              \
    do {                                                            \
        SKEL___INIT_TLS_DATA_0_PARAM(name);                         \
        for (skel__UInt it_tls_ = 0;                                \
             it_tls_ < skel__G::SKEL___NUM_WORKER_THREADS_VAR_NAME; \
             ++it_tls_) {                                           \
            name[it_tls_].memb_func(param_1, param_2);              \
        }                                                           \
    } while (0)

#define SKEL___TLS_DATA_FOR_EACH_0_PARAM(name, memb_func)           \
    do {                                                            \
        for (skel__UInt it_tls_ = 0;                                \
             it_tls_ < skel__G::SKEL___NUM_WORKER_THREADS_VAR_NAME; \
             ++it_tls_) {                                           \
            name[it_tls_].memb_func();                              \
        }                                                           \
    } while (0)

#else

#include "skel__ThreadNullTLS.inl"

#endif  // SKEL___BUILD_ENABLE_THREADS
