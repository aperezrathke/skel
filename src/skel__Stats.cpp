//****************************************************************************
// skel__Stats.cpp
//****************************************************************************

#include "skel__Build.h"
#include "skel__Stats.h"

#ifdef SKEL___BUILD_ENABLE_STATS

#include "skel__Assert.h"
#include "skel__Logf.h"

#include <string>

namespace skel__Stats {

#ifdef SKEL___STATS_DECLARE_NAME
#   error SKEL___STATS_DECLARE_NAME already defined!
#endif  // SKEL___STATS_DECLARE_NAME

#define SKEL___STATS_DECLARE_NAME(statId) #statId

/**
 * All timer stat instances
 */
TimerStat GTimerStats[skel__STAT_TimerMAX];

/**
 * The name of each timer stat
 */
const char* GTimerStatsNames[skel__STAT_TimerMAX] = {
    SKEL___STATS_DECLARE_NAME(skel__STAT_GlobalInitTime)
};

/**
 * All counter stat instances
 */
CounterStat GCounterStats[skel__STAT_CounterMAX];

/**
 * The name of each counter stat
 */
const char* GCounterStatsNames[skel__STAT_CounterMAX] = {
    SKEL___STATS_DECLARE_NAME(skel__STAT_GlobalInitCount)
};

#undef SKEL___STATS_DECLARE_NAME

/**
 * Report to standard out
 */
void report_stats() {
    // Header
    skel__Logf("STATS REPORT\n");
    skel__Logf("------------\n");

    // Timers
    skel__Logf("Timer stats:\n");
    unsigned int i = 0;
    for (i = 0; i < skel__STAT_TimerMAX; ++i) {
        skel__Logf("\t%s: %f minutes\n",
                   GTimerStatsNames[i],
                   (((double)GTimerStats[i].get_total_time()) / 60.0));
    }

    // Counters
    skel__Logf("Counter stats:\n");
    for (i = 0; i < skel__STAT_CounterMAX; ++i) {
        skel__Logf("\t%s: %u\n",
                   GCounterStatsNames[i],
                   GCounterStats[i].get_counter());
    }
}

}  // namespace skel__Stats

#endif  // SKEL___BUILD_ENABLE_STATS
