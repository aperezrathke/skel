//****************************************************************************
// skel__ParserRle.h
//****************************************************************************

/**
 * Utility for parsing homogeneous typed run-length-encoded (RLE) files. e.g:
 *  |run 1| <delim> |value 1|
 *  |run 2| <delim> |value 2|
 *  ...
 *  |run n| <delim> |value n|
 *
 * where |run i| is a positive integer, <delim> is a delimiting character that
 * can be whitespace or comma, and all |value i| entries are a scalar values
 * all of same type (e.g. all must be integers, floats, or strings). String
 * values must not contain any whitespace. Any line beginning with a '#' is
 * considered a comment and is ignored.
 */

#ifndef skel__ParserRle_h
#define skel__ParserRle_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__Logf.h"
#include "skel__StringUtils.h"
#include "skel__Types.h"

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <string>
#include <vector>

//****************************************************************************
// skel__ParserRle
//****************************************************************************

namespace skel__ParserRle {
/**
 * Parses run-length encoded (RLE) values in delimited format:
 *
 *  <run : +integer> <delim> <value : float|integer|string>
 *  ...
 * Where "run" specifies the number of consecutive entries with the given
 * value and "delim" is a comma or whitespace character. Furthermore, "value"
 * must not contain any whitespace!
 *
 * Example:
 *  6, 55.0
 * 12, 100.0
 *  7, 27.5
 *
 * This will assign the first 6 entries a value of 55.0, the next 12 entries a
 * value of 100.0, and the last 7 entries a value of 27.5.
 *
 * Note: lines beginning with '#' character are considered comments and
 * are ignored.
 *
 * @param out - container will contain expanded values
 * @param fpath - path to run-length encoded plain-text file
 * @return uTRUE if file successfully parsed, skel__FALSE o/w
 */
template <typename t_value, typename t_container>
skel__Bool read(t_container& out, const std::string& fpath) {
    enum { eRle_run = 0, eRle_val, eRle_num };
    skel__UInt run;
    t_value val;
    std::ifstream fin(fpath.c_str());
    std::string line;
    std::vector<std::string> tokens;
    int line_num = 1;
    out.clear();
    std::vector<t_value> values;
    while (!fin.eof()) {
        std::getline(fin, line);
        // Skip empty lines
        boost::trim(line);
        if (line.empty()) {
            ++line_num;
            continue;
        }
        // Skip comment characters
        if (line[0] == '#') {
            ++line_num;
            continue;
        }
        // Parse into tokens - split on commas or whitespace
        tokens.clear();
        boost::split(tokens,
                     line,
                     boost::is_any_of(", \t\r\n"),
                     boost::token_compress_on);
        // Expect two tokens, error out if any other case
        if (tokens.size() != eRle_num) {
            skel__Logf(
                "Error: %s has malformed line# %d. Expected %d tokens but "
                "found %d.\n",
                fpath.c_str(),
                line_num,
                ((int)eRle_num),
                ((int)tokens.size()));
            fin.close();
            return skel__FALSE;
        }
        // To be safe prior to conversion, trim leading/trailing whitespace
        for (size_t i = 0; i < tokens.size(); ++i) {
            boost::trim(tokens[i]);
        }
        // Convert run length
        const int run_signed =
            skel__StringUtils::string_as_T<int>(tokens[eRle_run]);
        if (run_signed <= 0) {
            skel__Logf("Error: %s has malformed line# %d. Expected run > 0.\n",
                       fpath.c_str(),
                       line_num);
            fin.close();
            return skel__FALSE;
        }
        run = SKEL___TO_UINT(run_signed);
        // Convert string to value type
        val = skel__StringUtils::string_as_T<t_value>(tokens[eRle_val]);
        // Expand and append new values
        for (skel__UInt i = 0; i < run; ++i) {
            values.push_back(val);
        }
        // Update line count
        ++line_num;
    }
    fin.close();
    // Convert to output container format
    if (!values.empty()) {
        out.resize(values.size());
        std::copy(values.begin(), values.end(), out.begin());
    }
    // Return true if we read some values, false otherwise
    return !values.empty();
}

}  // namespace skel__ParserRle

#endif  // skel__ParserRle_h
