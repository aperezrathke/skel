//****************************************************************************
// skel__Opts.cpp
//
// Program options
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__Opts.h"
#include "skel__Logf.h"

#include <stdio.h>

//****************************************************************************
// Implementation
//****************************************************************************

// Macros to declare option elements
#define UNARY_OPT_PREFIX "-"
#define BINARY_OPT_PREFIX "--"
#define DECLARE_CMD_UNARY_OPT(key, desc) \
    { "", UNARY_OPT_PREFIX key, desc }
#define DECLARE_CMD_BINARY_OPT(key, desc) \
    { "", BINARY_OPT_PREFIX key, desc }
#define DECLARE_INI_OPT(key, desc) \
    { key, BINARY_OPT_PREFIX key, desc }

/**
 * Array of option info structures. Array is indexed according to skel__Opt enum.
 */
const struct skel__OptInfo SKEL___OPT_INFOS[skel__Opt_NUM_OPTS] = {
      DECLARE_CMD_UNARY_OPT("help", "Shows command line options and how to use program.")
    , DECLARE_CMD_UNARY_OPT("app_help", "Shows command line options and how to use program.")
    , DECLARE_CMD_BINARY_OPT("conf", "Specifies path to INI configuration file. Usage: --conf <filename>")
    , DECLARE_INI_OPT("conf_child", " Specifies optional path to a child configuration. Can be specified on command line only for root parent, else must be specified via ini. Usage --conf_child <filename>")
    , DECLARE_INI_OPT("arch", "[Optional] Specifies path to archetype INI configuration. Options not found in main INI will be searched for in archetype - useful for setting common shared options and/or defaults. Usage --arch <filename>")
    , DECLARE_INI_OPT("output_dir", "Specifies directory to store generated data. Usage --output_dir <dirpath>")
#ifdef SKEL___BUILD_ENABLE_THREADS
    , DECLARE_INI_OPT("num_worker_threads", "Specifies the number of worker threads. Usage --num_worker_threads <positive integer>")
#endif // SKEL___BUILD_ENABLE_THREADS
#ifdef SKEL___BUILD_ENABLE_COMMANDLETS
    , DECLARE_CMD_UNARY_OPT("cmdlet_info", "Prints basic information about program.")
#endif // SKEL___BUILD_ENABLE_COMMANDLETS
#ifdef SKEL___BUILD_ENABLE_TESTS
    , DECLARE_CMD_UNARY_OPT("test_bitset", "Tests basic bit set operations")
    , DECLARE_CMD_UNARY_OPT("test_csppline", "Tests smoothed cubic splines")
    , DECLARE_CMD_UNARY_OPT("test_dir_distrib", "Tests Dirichlet distribution utilities")
    , DECLARE_CMD_UNARY_OPT("test_global_init", "Tests global initialization")
    , DECLARE_CMD_UNARY_OPT("test_hash", "Tests basic hash operations")
    , DECLARE_CMD_UNARY_OPT("test_rng", "Tests random number generator")
#ifdef SKEL___BUILD_ENABLE_THREADS
    , DECLARE_CMD_UNARY_OPT("test_parallel_mapper", "Tests simple use case of parallel mapper")
#endif // SKEL___BUILD_ENABLE_THREADS
#endif // SKEL___BUILD_ENABLE_TESTS
};

/**
 * Callback to print usage for a single switch
 */
void skel__Opt_print_usage(const skel__OptE ix) {
    const skel__OptInfo& o = SKEL___OPT_INFOS[ix];
    skel__Logf("---------------------------------\n");
    skel__Logf("INI key: %s\n", o.ini_key);
    skel__Logf("command line switch: %s\n", o.cmd_switch);
    skel__Logf("%s\n", o.desc);
}

/**
 * Callback for --help switch
 */
void skel__Opt_print_usage_all() {
    skel__Logf(
        "#################################\nOptions\n##########################"
        "#######\n");
    skel__Logf(
        "Most program options can be specified via command line or via a INI "
        "file.\n");
    skel__Logf(
        "If an option has no INI key, then it can only be specified by "
        "command line.\n");
    skel__Logf("Options:\n");
    // @TODO - would be nice to format into a table
    for (int i = 0; i < skel__Opt_NUM_OPTS; ++i) {
        skel__Opt_print_usage(static_cast<skel__OptE>(i));
    }
}
