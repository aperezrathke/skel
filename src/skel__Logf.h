//****************************************************************************
// skel__Logf.h
//****************************************************************************

#ifndef skel__Logf_h
#define skel__Logf_h

#include "skel__Build.h"

/**
 * A logging macro useful for debugging
 */
#ifdef SKEL___BUILD_ENABLE_LOGGING
#   include <stdio.h>
    // Flush stdout so that batch scripts can keep us updated
#   define skel__Logf(...)       \
        do {                     \
            printf(__VA_ARGS__); \
            fflush(stdout);      \
        } while (0)
#else
#   define skel__Logf(...)
#endif  // SKEL___BUILD_ENABLE_LOGGING

#endif  // skel__Logf_h
