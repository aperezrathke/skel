//****************************************************************************
// skel__MemAlign.h
//****************************************************************************

/**
 * Portable utilities for (de)allocating aligned memory.
 *
 * Usage:
 *  skel___aligned_malloc(...) allocates aligned memory
 *  skel___aligned_free(...) deallocates aligned memory
 *
 * Memory allocated with skel___aligned_malloc must be freed with
 * skel___aligned_free!
 */

#ifndef skel__MemAlign_h
#define skel__MemAlign_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__Assert.h"

//****************************************************************************
// Macros
//****************************************************************************

#ifdef SKEL___BUILD_ENABLE_BOOST_MEM_ALIGN
    /**
     * Boost alignment routines
     */
    #include <boost/align/aligned_alloc.hpp>

    #define skel___aligned_malloc_unchecked(ptr__, size__, align__, type__) \
        ptr__ = (type__*)boost::alignment::aligned_alloc(align__, size__)

    #define skel___aligned_free(ptr__) boost::alignment::aligned_free(ptr__)

#elif defined(SKEL___BUILD_COMPILER_MSVC)
    /**
     * MSCV alignment routines
     */
    #include <malloc.h>

    #define skel___aligned_malloc_unchecked(ptr__, size__, align__, type__) \
        ptr__ = (type__*)_aligned_malloc(size__, align__)

    #define skel___aligned_free(ptr__) _aligned_free(ptr__)

#elif defined(SKEL___BUILD_COMPILER_ICC)
    /**
     * Intel compiler alignment routines
     */
    #define skel___aligned_malloc_unchecked(ptr__, size__, align__, type__) \
        ptr__ = (type__*)_mm_malloc(size__, align__)

    #define skel___aligned_free(ptr__) _mm_free(ptr__)

#elif defined(SKEL___BUILD_COMPILER_GCC)
    /**
     * GCC (POSIX) alignment routines
     */
    #include <stdlib.h>

    #define skel___aligned_malloc_unchecked(ptr__, size__, align__, type__) \
        do {                                                                \
            void** pptr__ = (void**)(&(ptr__));                             \
            posix_memalign(pptr__, align__, size__);                        \
        } while (0)

    #define skel___aligned_free(ptr__) free(ptr__);

#else
    /**
     * Platform not found!
     */
    #error Unrecognized platform
#endif  // SKEL___BUILD_ENABLE_BOOST_MEM_ALIGN

#define skel___is_aligned(ptr__, align__) \
    (((uintptr_t)(const void*)(ptr__)) % (align__) == 0)

/**
 * skel___aligned_malloc
 *  @param ptr__ - Pointer to be assigned with allocated, aligned memory
 *  @param size__ - Number of bytes to allocate
 *  @param align__ - Target memory alignment in bytes
 *  @param type__ - The target type that pointer is pointing to
 *
 * Allocated memory must be freed using skel___aligned_free(ptr__)
 */
#define skel___aligned_malloc(ptr__, size__, align__, type__)            \
    do {                                                                 \
        skel___aligned_malloc_unchecked(ptr__, size__, align__, type__); \
        skel__Assert(skel___is_aligned(ptr__, align__));                 \
    } while (0)

#endif  // skel__MemAlign_h
