//****************************************************************************
// skel__Armadillo.h
//****************************************************************************

/**
 * @brief Armadillo include which incorporates build flags
 */

#ifndef skel__Armadillo_h
#define skel__Armadillo_h

#include "skel__Build.h"
#include "skel__Types.h"

// Check if we don't want armadillo debug information (e.g. during release)
// Also, define NDEBUG before including this external library just in case it
// still has internal asserts that aren't disabled by ARMA_NO_DEBUG.
#ifndef SKEL___BUILD_ARMADILLO_DEBUG
#   ifndef ARMA_NO_DEBUG
#       define ARMA_NO_DEBUG // tell armadillo to avoid debug checks
#   endif
#   ifndef NDEBUG // for good measure, disable asserts for this unit
#       define SKEL___NDEBUG_TRANSIENT // this means enable asserts after include
#       define NDEBUG
#   endif // NDEBUG
#   include <armadillo>
#   ifdef SKEL___NDEBUG_TRANSIENT // NDEBUG wasn't defined before, so undef it
#       undef SKEL___NDEBUG_TRANSIENT
#       undef NDEBUG
#   endif // SKEL___NDEBUG_TRANSIENT
#else
#   include <armadillo> // else, lets include extra debug checks
#endif // SKEL___BUILD_ARMADILLO_DEBUG

// Wrap namespace
#define skel__MatrixUtils arma

/**
 * Matrix for real values
 */
typedef skel__MatrixUtils::Mat<skel__Real> skel__Matrix;

/**
 * Matrix for unsigned integers
 */
typedef skel__MatrixUtils::Mat<skel__UInt> skel__UIMatrix;

/**
 * Column vector for real values
 */
typedef skel__MatrixUtils::Col<skel__Real> skel__VecCol;

/**
 * Row vector for real values
 */
typedef skel__MatrixUtils::Row<skel__Real> skel__VecRow;

/**
 * Column vector for unsigned integers
 */
typedef skel__MatrixUtils::Col<skel__UInt> skel__UIVecCol;

/**
 * Column vector for boolean values
 */
typedef skel__MatrixUtils::Col<skel__Bool> skel__BoolVecCol;

/**
 * Matrix indexing type
 */
typedef skel__MatrixUtils::uword skel__MatSz_t;

/**
 * Column vector of matrix indexing type
 */
typedef skel__MatrixUtils::Col<skel__MatSz_t> skel__MatSzVecCol;

/**
 * Macro for casting to matrix indexing type
 */
#define SKEL___TO_MAT_SZ_T(i) static_cast<skel__MatSz_t>((i))

#endif  // skel__Armadillo_h
