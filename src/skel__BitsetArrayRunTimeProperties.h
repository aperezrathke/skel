//****************************************************************************
// skel__BitsetArrayRunTimeProperties.h
//****************************************************************************

#ifndef skel__BitsetArrayRunTimeProperties_h
#define skel__BitsetArrayRunTimeProperties_h

/**
 * Run time properties policy for use in skel__BitsetHandle. Serves as an
 * interface to obtain properties common to all bit sets within the array.
 */
template <typename t_BitsetArray>
class skel__BitsetArrayRunTimeProperties {
public:
    /**
     * @return The number of bits in the bit set
     */
    inline typename t_BitsetArray::size_type size() const {
        return m_parent_bit_set_array.num_bits();
    }

    /**
     * @return The number of blocks necessary to represent a bit set
     */
    inline typename t_BitsetArray::size_type num_blocks() const {
        return m_parent_bit_set_array.num_blocks();
    }

    /**
     * @return The number of bytes necessary to represent a bit set
     */
    inline typename t_BitsetArray::size_type num_bytes() const {
        return m_parent_bit_set_array.num_bytes();
    }

private:
    /**
     * Private constructor
     */
    explicit skel__BitsetArrayRunTimeProperties(
        const t_BitsetArray& parent_bit_set_array)
        : m_parent_bit_set_array(parent_bit_set_array) {}

    /**
     * A reference to parent bit set array containing this bit set
     */
    const t_BitsetArray& m_parent_bit_set_array;

    // Allow t_BitsetArray to do as it pleases
    friend t_BitsetArray;
};

#endif  // skel__BitsetArrayRunTimeProperties_h
