//****************************************************************************
// skel__Globals.h
//****************************************************************************

/**
 * Houses global data that is configurable at run time
 */

#ifndef skel__Globals_h
#define skel__Globals_h

#include "skel__Build.h"
#include "skel__Config.h"
#include "skel__Rand.h"
#include "skel__Thread.h"

/**
 * Namespace to house global data
 */
namespace skel__G {
/**
 * If threads enabled, this value will hold the number of worker threads
 * available (the main thread is not included in this count). To access:
 * skel__G::NUM_WORKER_THREADS_VAR_NAME (will cause build errors if threads not
 * enabled).
 */
SKEL___EXTERN_NUM_WORKER_THREADS

/**
 * If threads enabled, this declares an extern for a parallel mapper. To
 * access: skel__G:PARALLEL_MAPPER_VAR_NAME (will cause build errors if threads
 * not enabled).
 */
SKEL___EXTERN_PARALLEL_MAPPER

/**
 * Random number generator (each thread has its own instance)
 */
extern SKEL___DECLARE_TLS_DATA(skel__Rand, rng);

/**
 * Initialize global data from runtime configuration
 */
extern void init(skel__SpConstConfig_t cfg);

/**
 * Initializes global data to default values
 */
extern void default_init();

/**
 * If global data is initialized, teardown() must be called before main()
 * exits!
 * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
 * This seems to be a deadlock bug caused by Microsoft's implementation of
 * std::thread
 */
extern void teardown();

}  // namespace skel__G

/**
 * Macro for accessing the global random number generator
 */
#define skel__Rng SKEL___ACCESS_TLS_DATA(skel__G::rng)

#endif  // skel__Globals_h
