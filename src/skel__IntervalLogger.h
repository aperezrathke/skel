//****************************************************************************
// skel__IntervalLogger.h
//****************************************************************************

/**
 * @brief Simple utility for reporting time taken by a code region for each
 *   execution of that code.
 */

#ifndef skel__IntervalLogger_h
#define skel__IntervalLogger_h

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_INTERVAL_LOGGING

#include "skel__AutoTimer.h"
#include "skel__Logf.h"

/**
 * Enumerated unique identifiers for all interval logger stats
 */
enum skel__AllIntervalLoggerStats {
    // Total execution time
    skel__STAT_TotalExecInterval = 0,
    // ADD PROJECT SPECIFIC STATS HERE
    skel__STAT_IntervalLoggerMax
};

#define SKEL___SCOPED_INTERVAL_LOGGER(stat_id) \
    skel__Stats::ScopedStatIntervalLogger ScopedLogger_##stat_id(stat_id)

namespace skel__Stats {
/**
 * The static descriptions used for interval logging
 */
extern const char* GCounterIntervalLoggerDescs[skel__STAT_IntervalLoggerMax];

/**
 * A synchronous timer for a single interval. Interval time span is
 * reported on destruction
 */
class ScopedStatIntervalLogger {
public:
    explicit ScopedStatIntervalLogger(
        const skel__AllIntervalLoggerStats stat_id)
        : m_stat_id(stat_id) {}

    ~ScopedStatIntervalLogger() {
        skel__Logf("%s\n", GCounterIntervalLoggerDescs[m_stat_id]);
    }

private:
    const skel__AllIntervalLoggerStats m_stat_id;
    skel__AutoTimer m_timer;
};
}  // namespace skel__Stats

#else

#define SKEL___SCOPED_INTERVAL_LOGGER(stat_id) ((void)0)

#endif  // SKEL___BUILD_ENABLE_INTERVAL_LOGGING

#endif  // skel__IntervalLogger_h
