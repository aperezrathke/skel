//****************************************************************************
// skel__HashMap.h
//****************************************************************************

#ifndef skel__HashMap_h
#define skel__HashMap_h

/**
 * @brief An associative array implemented as a hash table
 */

#include "skel__Build.h"

/**
 * Select between standard vs boost implementations
 */
#ifdef SKEL___BUILD_CXX_11
#   include <unordered_map>
#   define SKEL___HASH_MAP std::unordered_map
#else
#   include <boost/unordered_map.hpp>
#   define SKEL___HASH_MAP boost::unordered_map
#endif  // SKEL___BUILD_CXX_11

/**
 * [May] pre-allocate storage for 'n' elements
 */
#define SKEL___HASH_RESERVE(hash_, n_) (hash_).reserve(n_)

#endif  // skel__HashMap_h
