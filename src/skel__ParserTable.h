//****************************************************************************
// skel__ParserTable.h
//****************************************************************************

/**
 * Utility for parsing comma or whitespace delimited tabular data files.
 */

#ifndef skel__ParserTable_h
#define skel__ParserTable_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__Assert.h"
#include "skel__StringUtils.h"
#include "skel__Types.h"

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <string>
#include <vector>

//****************************************************************************
// skel__ParserTable
//****************************************************************************

class skel__ParserTable {
public:
    /**
     * Reads delimited tabular data. Delimiting character between successive
     * elements may be a comma (',') or whitespace. Because whitespace is a
     * delimiter, element values may not contain whitespace. Lines starting
     * with a '#' character are considered comments and ignored.
     *
     * Data format follows pattern
     *
     *  # Optional header row:
     *  <col label 0> <delim> <col label 1> <delim> ... <col label N>
     *  # First data row:
     *  <elem 00> <delim> <elem 01> <delim> ... <elem 0N>
     *  # Second data row
     *  <elem 10> <delim> <elem 11> <delim> ... <elem 0N>
     *  ...
     *  # K-th data row
     *  <elem K0> <delim> <elem K1> <delim> ... <elem K0>
     *
     * Rows may be jagged (unequal sizes).
     * @param fpath - path to tabular data file
     * @param has_header - TRUE if first row has column labels, FALSE o/w
     * @param is_jagged - TRUE if rows may be unequal sizes, FALSE o/w
     * @return TRUE if table parsing succeeded, FALSE o/w
     */
    skel__Bool read(const std::string& fpath,
                    const skel__Bool has_header = skel__FALSE,
                    const skel__Bool is_jagged = skel__FALSE) {
        // Open target file
        std::ifstream fin(fpath.c_str());
        // Parse table
        const skel__Bool b_okay = this->read(fin, has_header, is_jagged);
        // Close file
        fin.close();
        // Return TRUE if parsing succeeded, FALSE o/w
        return b_okay;
    }

    /**
     * Reads delimited tabular data. Delimiting character between successive
     * elements may be a comma (',') or whitespace. Because whitespace is a
     * delimiter, element values may not contain whitespace. Lines starting
     * with a '#' character are considered comments and ignored.
     *
     * Data format follows pattern
     *
     *  # Optional header row:
     *  <col label 0> <delim> <col label 1> <delim> ... <col label N>
     *  # First data row:
     *  <elem 00> <delim> <elem 01> <delim> ... <elem 0N>
     *  # Second data row
     *  <elem 10> <delim> <elem 11> <delim> ... <elem 0N>
     *  ...
     *  # K-th data row
     *  <elem K0> <delim> <elem K1> <delim> ... <elem K0>
     *
     * Rows may be jagged (unequal sizes).
     * @param fin - OPEN file handle, CLIENT IS RESPONSIBLE FOR CLOSING HANDLE
     * @param has_header - TRUE if first row has column labels, FALSE o/w
     * @param is_jagged - TRUE if rows may be unequal sizes, FALSE o/w
     * @return TRUE if table parsing succeeded, FALSE o/w
     */
    skel__Bool read(std::istream& fin,
                    const skel__Bool has_header = skel__FALSE,
                    const skel__Bool is_jagged = skel__FALSE) {
        // Reset table data
        m_header.clear();
        m_table.clear();
        // Open target file
        std::string line;
        std::vector<std::string> tokens;
        int line_num = 0;
        // Parse first row
        size_t target_size = 0;
        while (!fin.eof()) {
            ++line_num;
            if (this->parse_row(fin, line, tokens)) {
                skel__Assert(!tokens.empty());
                target_size = tokens.size();
                if (has_header) {
                    m_header = tokens;
                } else {
                    m_table.push_back(tokens);
                }
                break;
            }
        }
        // Parse remaining rows
        while (!fin.eof()) {
            ++line_num;
            if (this->parse_row(fin, line, tokens)) {
                skel__Assert(!tokens.empty());
                if (!is_jagged && (tokens.size() != target_size)) {
                    skel__Assert(skel__FALSE);
                    return skel__FALSE;
                }
                // Append row
                m_table.push_back(tokens);
            }
        }
        // Return TRUE if we read some values, FALSE otherwise
        return !m_table.empty();
    }

    /**
     * @param row - 0-based row index
     * @param col - 0-based column index
     * @return typed value at (row, col)
     */
    template <typename t_value>
    inline t_value get(const skel__UInt row, const skel__UInt col) const {
        skel__AssertBounds(row, 0, SKEL___TO_UINT(m_table.size()));
        skel__AssertBounds(col, 0, SKEL___TO_UINT(m_table[row].size()));
        return skel__StringUtils::string_as_T<t_value>(m_table[row][col]);
    }

    /**
     * For trusted data, this method can be significantly faster (~7X) than
     * corresponding member get<t_value_uint>(row, col).
     * @param row - 0-based row index
     * @param col - 0-based column index
     * @return unsigned integral value at (row, col)
     */
    template <typename t_value_uint>
    inline t_value_uint get_uint_naive(const skel__UInt row,
                                       const skel__UInt col) const {
        skel__AssertBounds(row, 0, SKEL___TO_UINT(m_table.size()));
        skel__AssertBounds(col, 0, SKEL___TO_UINT(m_table[row].size()));
        return skel__StringUtils::string_as_T_UINT_naive<t_value_uint>(
            m_table[row][col]);
    }

    /**
     * Obtain typed table row
     * @param out - output vector type, must support member methods:
     *      .clear(), .resize(), and operator[]
     * @param row - 0-based row index
     */
    template <typename t_value, typename t_vec>
    inline void get_row(t_vec& out, const skel__UInt row) const {
        skel__AssertBounds(row, 0, SKEL___TO_UINT(m_table.size()));
        out.clear();
        const skel__UInt n_cols = SKEL___TO_UINT(m_table[row].size());
        out.resize(n_cols);
        for (skel__UInt col = 0; col < n_cols; ++col) {
            out[col] = this->get<t_value>(row, col);
        }
    }

    /**
     * Obtain typed table column - elements from jagged rows without a
     * corresponding column entry are set to default t_value(). Therefore,
     * output container will always be of num_rows() size and client must be
     * aware of any rows which only contain the default elements.
     * @param out - output vector type, must support member methods:
     *      .clear(), .resize(), and operator[]
     * @param col - 0-based column index
     */
    template <typename t_value, typename t_vec>
    inline void get_col(t_vec& out, const skel__UInt col) const {
        out.clear();
        const skel__UInt n_rows = this->num_rows();
        out.resize(n_rows);
        for (skel__UInt row = 0; row < n_rows; ++row) {
            // Handle jagged rows
            if (col < this->num_cols(row)) {
                out[row] = this->get<t_value>(row, col);
            } else {
                out[row] = t_value();
            }
        }
    }

    /**
     * @return number of rows in parsed table, 0 if not yet parsed
     */
    inline skel__UInt num_rows() const {
        return SKEL___TO_UINT(m_table.size());
    }

    /**
     * @return number of columns for target row, 0 if not yet parsed
     */
    inline skel__UInt num_cols(const skel__UInt row = 0) const {
        skel__Assert(m_table.empty() || (row < SKEL___TO_UINT(m_table.size())));
        if (row < m_table.size()) {
            return SKEL___TO_UINT(m_table[row].size());
        }
        return SKEL___TO_UINT(0);
    }

    /**
     * @return name of target column
     */
    const std::string& get_col_name(const skel__UInt col) const {
        skel__AssertBounds(col, 0, SKEL___TO_UINT(m_header.size()));
        return m_header[col];
    }

    /**
     * @return header row
     */
    const std::vector<std::string>& get_col_names() const {
        return this->get_header();
    }

    /**
     * @return header row
     */
    const std::vector<std::string>& get_header() const { return m_header; }

private:
    /**
     * Utility for parsing a single table row
     */
    static bool parse_row(std::istream& fin,
                          std::string& line,
                          std::vector<std::string>& tokens) {
        skel__Assert(fin.good());
        tokens.clear();
        std::getline(fin, line);
        // Skip empty lines
        boost::trim(line);
        if ((!line.empty()) && (line[0] != '#')) {
            boost::split(tokens,
                         line,
                         boost::is_any_of(", \t\r\n"),
                         boost::token_compress_on);
#ifdef SKEL___BUILD_ENABLE_ROBUST_PARSER_CHECKS
            // Verify individual token trimming is unnecessary
            std::vector<std::string> tokens_trimmed = tokens;
            for (size_t i = 0; i < tokens_trimmed.size(); ++i) {
                boost::trim(tokens_trimmed[i]);
            }
            skel__Assert(tokens_trimmed == tokens);
#endif  // SKEL___BUILD_ENABLE_ROBUST_PARSER_CHECKS
        }
        return !tokens.empty();
    }

    /**
     * Row type - collection of strings
     */
    typedef std::vector<std::string> row_t;

    /**
     * Table type - supports jagged rows
     */
    typedef std::vector<row_t> table_t;

    /**
     * Collection of column names, may be empty
     */
    row_t m_header;

    /**
     * 2-D array of table values, may be jagged
     */
    table_t m_table;
};

#endif  // skel__ParserTable_h
