//****************************************************************************
// skel__ExitCodes.h
//****************************************************************************

/**
 * Defines the program exit codes
 */

#ifndef skel__ExitCodes_h
#define skel__ExitCodes_h

#include "skel__Build.h"

/**
 * Exit codes
 */
enum skel__ExitCode {
    /**
     * Program terminated normally
     */
    skel__ExitCode_normal = 0,
    /**
     * Program terminated with error
     */
    skel__ExitCode_error
};

#endif  // skel__ExitCodes_h
