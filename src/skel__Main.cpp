//****************************************************************************
// skel__Main.cpp
//
// Main program entry point
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__AppHooks.h"
#include "skel__Apps.h"
#include "skel__CmdOptsMap.h"
#include "skel__Config.h"
#include "skel__ExitCodes.h"
#include "skel__IntervalLogger.h"
#include "skel__Logf.h"
#include "skel__Opts.h"

// Commandlet integration
#include "skel__CommandletHooks.inl"

// Test integration
#include "skel__TestHooks.inl"

//****************************************************************************
// Help utility
//****************************************************************************

/**
 * App prints usage information
 *
 * Usage:
 * -help | -app_help
 */
int skel__AppHelpMain(const skel__CmdOptsMap& cmd_opts) {
    skel__Opt_print_usage_all();

    // Signify we finished okay
    return skel__ExitCode_normal;
}

//****************************************************************************
// App registration
//****************************************************************************

// Register applications here
const struct skel__AppInfo GApps[] = {
    {skel__Opt_get_cmd_switch(skel__Opt_help), &skel__AppHelpMain},
    {skel__Opt_get_cmd_switch(skel__Opt_app_help), &skel__AppHelpMain}
};

/**
 * Wrapper for executing main applications
 */
static int skel__RunApps(const skel__CmdOptsMap& cmd_opts) {
    //******************************
    // Main application starts here:
    //******************************
    SKEL___CONDITIONAL_RUN_APPS(cmd_opts, GApps);
    // If we reach here, we didn't find an application to run
    return skel__ExitCode_error;
}

//****************************************************************************
// Main
//****************************************************************************

int main(const int argc, const char** argv) {
    SKEL___SCOPED_INTERVAL_LOGGER(skel__STAT_TotalExecInterval);

    // Format command line options into <key, value> mappings
    skel__CmdOptsMap cmd_opts;
    if (!cmd_opts.parse(argc, argv)) {
        skel__Logf(
            "Unrecoverable error - unable to parse command line options.\n");
        return skel__ExitCode_error;
    }

    // Commandlets!
    SKEL___CONDITIONAL_RUN_COMMANDLETS(cmd_opts);

    // Tests!
    SKEL___CONDITIONAL_RUN_TESTS(cmd_opts);

    // Run main application(s)
    return skel__RunApps(cmd_opts);
}
