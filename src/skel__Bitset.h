//****************************************************************************
// skel__Bitset.h
//****************************************************************************

/**
 * Our 'hard' bit set type - a bit set class that will allocate and manage its
 * underlying bit buffer.
 *
 * This class complements the 'soft' bit set handle which does not manage its
 * underlying bit buffer.
 */

#ifndef skel__Bitset_h
#define skel__Bitset_h

#include "skel__Build.h"
#include "skel__BitsetCompileTimeProperties.h"

#include <boost/dynamic_bitset.hpp>

// Note - changing the block type here will also change the block type for the
// pooled bit set array and its handle.
typedef boost::dynamic_bitset<
    skel__BitsetCompileTimeProperties::block_type /*Block*, Allocator*/>
    skel__Bitset;

#endif  // skel__Bitset_h
