//****************************************************************************
// skel__Opts.h
//****************************************************************************

/**
 * Defines the program options
 */

#ifndef skel__Opts_h
#define skel__Opts_h

#include "skel__Build.h"
#include "skel__Assert.h"

/**
 * Enumerated user options
 */
enum skel__OptE {
    /**
     * Prints command line options and how to use program
     * Note: option can only be specified via command line
     */
      skel__Opt_help = 0

    /**
     * Same as skel__Opt_help
     */
    , skel__Opt_app_help

    /**
     * Specifies path to configuration INI file
     * Note: option can only be specified via command line
     */
    , skel__Opt_conf

    /**
     * Specifies optional path to a child configuration
     * Can be specified on command line only for root parent, else must be
     * specified via ini
     */
    , skel__Opt_conf_child

    /**
     * Archetype configuration used by INI file, option values not specified
     * by main INI will be searched for in the archetype - meant to specify
     * defaults and/or common option settings
     */
    , skel__Opt_arch

    /**
     * Specifies directory to store output data
     */
    , skel__Opt_output_dir

#ifdef SKEL___BUILD_ENABLE_THREADS
    /**
     * Specifies the number of worker threads
     */
    , skel__Opt_num_worker_threads
#endif // SKEL___BUILD_ENABLE_THREADS

// END INTERACTION CONSTRAINT OPTIONS
#ifdef SKEL___BUILD_ENABLE_COMMANDLETS

    /**
     * Prints basic information about program.
     */
    , skel__Opt_cmdlet_info
#endif // SKEL___BUILD_ENABLE_COMMANDLETS

#ifdef SKEL___BUILD_ENABLE_TESTS

    /**
     * Tests basic bit set operations
     */
    , skel__Opt_test_bitset

    /**
     * Tests smoothed cubic splines
     */
    , skel__Opt_test_csppline

    /**
     * Tests Dirichlet distribution utility
     */
    , skel__test_dir_distrib

    /**
     * Tests global initialization
     */
    , skel__Opt_test_global_init

    /**
     * Tests basic hash operations
     */
    , skel__Opt_test_hash

    /**
     * Tests random number generator
     */
    , skel__Opt_test_rng

#ifdef SKEL___BUILD_ENABLE_THREADS
    /**
     * Tests simple use case for parallel mapper
     */
    , skel__Opt_test_parallel_mapper
#endif // SKEL___BUILD_ENABLE_THREADS

#endif // SKEL___BUILD_ENABLE_TESTS

    /**
     * Total number of options, not an actual option
     */
    , skel__Opt_NUM_OPTS
};

/**
 * Info containing the option key, switch, and help description
 */
struct skel__OptInfo {
    /**
     * The key string for use in INI
     */
    const char* ini_key;

    /**
     * The key string for use on the command line
     */
    const char* cmd_switch;

    /**
     * The description output when user types '--help'
     */
    const char* desc;
};

/**
 * Array of option info structures. Array is indexed according to skel__Opt
 * enum.
 */
extern const struct skel__OptInfo SKEL___OPT_INFOS[skel__Opt_NUM_OPTS];

/**
 * Callback to print usage for a single switch
 */
extern void skel__Opt_print_usage(const skel__OptE ix);

/**
 * Callback for --help switch
 */
extern void skel__Opt_print_usage_all();

/**
 * Accessor for returning the INI key associated with option
 *
 * @param ix - enumerated index of option
 * @return the INI key string associated with option
 */
inline const char* skel__Opt_get_ini_key(const enum skel__OptE ix) {
    skel__AssertBounds(ix, 0, skel__Opt_NUM_OPTS);
    return SKEL___OPT_INFOS[ix].ini_key;
}

/**
 * Accessor for returning the command line switch associated with option
 *
 * @param ix - enumerated index of option
 * @return the command line switch string associated with option
 */
inline const char* skel__Opt_get_cmd_switch(const enum skel__OptE ix) {
    skel__AssertBounds(ix, 0, skel__Opt_NUM_OPTS);
    return SKEL___OPT_INFOS[ix].cmd_switch;
}

/**
 * Function pointer to accessor function
 * Makes it easier for client functions to remain agnostic of INI vs command
 * line keys
 */
typedef const char* (*skel__Opt_get_key_func)(const enum skel__OptE);

#endif  // skel__Opts_h
