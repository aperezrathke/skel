//****************************************************************************
// skel__Thread.h
//****************************************************************************

/**
 * Our threading specification
 */

#ifndef skel__Thread_h
#define skel__Thread_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__Logf.h"

#include <time.h>

#ifdef SKEL___BUILD_ENABLE_THREADS

#include "skel__Assert.h"

#include <vector>

#ifdef SKEL___BUILD_CXX_11
    #include <atomic>
    #include <condition_variable>
    #include <thread>
    #define skel__Th std
#else
    #include <boost/atomic.hpp>
    #include <boost/thread.hpp>
    #include <boost/thread/condition_variable.hpp>
    #define skel__Th boost
#endif  // SKEL___BUILD_CXX_11

//****************************************************************************
// Defines
//****************************************************************************

// The number of threads to initialize by default
#define SKEL___DEFAULT_NUM_THREADS \
    (std::max<skel__UInt>(skel__Th::thread::hardware_concurrency(), 1))

//****************************************************************************
// Typedefs
//****************************************************************************

typedef skel__Th::thread skel__Thread_t;
typedef skel__Th::mutex skel__Mutex_t;
typedef skel__Th::unique_lock<skel__Mutex_t> skel__Lock_t;
typedef skel__Th::lock_guard<skel__Mutex_t> skel__ScopedLock_t;
typedef skel__Th::condition_variable skel__ConditionVariable_t;
typedef skel__Th::atomic<unsigned int> skel__AtomicUInt;
typedef skel__Th::atomic<time_t> skel__AtomicTime_t;
typedef skel__Th::atomic<double> skel__AtomicDouble_t;

/**
 * Handle atomic add for double type
 * @param atomic_a - atomic double to increment by b
 * @param double_b - amount to increment a by
 * https://stackoverflow.com/questions/23116279/how-to-perform-basic-operations-with-stdatomic-when-the-type-is-not-integral
 */
#define SKEL___ATOMIC_ADD_DOUBLE(atomic_a, double_b)                           \
    do {                                                                       \
        double a_loaded = atomic_a.load();                                     \
        while (!atomic_a.compare_exchange_weak(a_loaded, a_loaded + double_b)) \
            ;                                                                  \
    } while (0)

//****************************************************************************
// Parallel mapper
//****************************************************************************

#include "skel__ParallelMapper.h"

//****************************************************************************
// TLS - Our home-brewed thread local storage system
//****************************************************************************

/**
 * Am rolling own TLS system because I don't trust boost TLS or
 * std::thread::id to not trap to the kernel.
 */

#define SKEL___MAIN_THREAD_ID_0_ARG 0

#define SKEL___MAIN_THREAD_ID_ARG , SKEL___MAIN_THREAD_ID_0_ARG

#define SKEL___THREAD_ID_VAR_NAME tId__

#define SKEL___THREAD_ID_0_ARG SKEL___THREAD_ID_VAR_NAME

#define SKEL___THREAD_ID_ARG , SKEL___THREAD_ID_VAR_NAME

#define SKEL___THREAD_ID_0_PARAM const skel__UInt SKEL___THREAD_ID_VAR_NAME

#define SKEL___THREAD_ID_PARAM , SKEL___THREAD_ID_0_PARAM

#define SKEL___NUM_WORKER_THREADS_VAR_NAME num_worker_threads

#define SKEL___EXTERN_NUM_WORKER_THREADS \
    extern skel__UInt SKEL___NUM_WORKER_THREADS_VAR_NAME;

#define SKEL___DECLARE_NUM_WORKER_THREADS \
    skel__UInt SKEL___NUM_WORKER_THREADS_VAR_NAME;

#define SKEL___PARALLEL_MAPPER_VAR_NAME par_map

#define SKEL___EXTERN_PARALLEL_MAPPER \
    extern skel__ParallelMapper SKEL___PARALLEL_MAPPER_VAR_NAME;

#define SKEL___DECLARE_PARALLEL_MAPPER \
    skel__ParallelMapper SKEL___PARALLEL_MAPPER_VAR_NAME;

#define skel__ParMap skel__G::SKEL___PARALLEL_MAPPER_VAR_NAME

#define SKEL___SET_NUM_WORKER_THREADS(config, num) \
    config->set_option(skel__Opt_num_worker_threads, num)

#define SKEL___LOG_NUM_WORKER_THREADS            \
    skel__Logf("Available worker threads: %u\n", \
               skel__G::SKEL___NUM_WORKER_THREADS_VAR_NAME)

#else  // !defined(SKEL___BUILD_ENABLED_THREADS)

typedef unsigned int skel__AtomicUInt;

typedef time_t skel__AtomicTime_t;

typedef double skel__AtomicDouble_t;

#define SKEL___ATOMIC_ADD_DOUBLE(atomic_a, double_b) \
    do {                                             \
        atomic_a += double_b;                        \
    } while (0)

#define SKEL___MAIN_THREAD_ID_0_ARG

#define SKEL___MAIN_THREAD_ID_ARG

#define SKEL___THREAD_ID_0_ARG

#define SKEL___THREAD_ID_ARG

#define SKEL___THREAD_ID_0_PARAM

#define SKEL___THREAD_ID_PARAM

#define SKEL___EXTERN_NUM_WORKER_THREADS

#define SKEL___DECLARE_NUM_WORKER_THREADS

#define SKEL___EXTERN_PARALLEL_MAPPER

#define SKEL___DECLARE_PARALLEL_MAPPER

#define SKEL___SET_NUM_WORKER_THREADS(config, num) ((void)0)

#define SKEL___LOG_NUM_WORKER_THREADS ((void)0)

#endif  // SKEL___BUILD_ENABLE_THREADS

// Include default thread local storage accessors
#include "skel__ThreadTLS.inl"

#endif  // skel__Thread_h
