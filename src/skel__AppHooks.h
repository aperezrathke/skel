//****************************************************************************
// skel__AppHooks.h
//****************************************************************************

/**
 * @brief Generic system for registering and launching applications that can
 * be executed.
 */

#ifndef skel__AppHooks_h
#define skel__AppHooks_h

#include "skel__Build.h"
#include "skel__CmdOptsMap.h"
#include "skel__Types.h"

/**
 * Application info structure
 */
struct skel__AppInfo {
    /**
     * The name of the application
     */
    const char* name;
    /**
     * The callback to run the application
     */
    int (*fp_main)(const skel__CmdOptsMap&);
};

/**
 * @return true if command line says we should run parameter commandlet, false
 * otherwise
 */
inline skel__Bool skel__ShouldRunApp(const skel__CmdOptsMap& cmd_opts,
                                     const char* app_name) {
    return cmd_opts.key_exists(app_name);
}

/**
 * Parse command line and run any utilities specified
 * Note: do { ... } while(0) allows multi-line macro to behave like single-line
 */
#define SKEL___CONDITIONAL_RUN_APPS(cmd_opts, apps_array)                \
    do {                                                                 \
        const int NUM_APPS = sizeof(apps_array) / sizeof(skel__AppInfo); \
        for (int ix_app = 0; ix_app < NUM_APPS; ++ix_app) {              \
            if (skel__ShouldRunApp(cmd_opts, apps_array[ix_app].name)) { \
                return (*(apps_array[ix_app].fp_main))(cmd_opts);        \
            }                                                            \
        }                                                                \
    } while (0)

#endif  // skel__AppHooks_h
