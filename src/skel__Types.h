//****************************************************************************
// skel__Types.h
//****************************************************************************

/**
 * @brief Defines core primitive types
 */

#ifndef skel__Types_h
#define skel__Types_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"

#include <limits>

//****************************************************************************
// Types
//****************************************************************************

/**
 * Our default floating point type
 */
typedef double skel__Real;

/**
 * Macro for casting to skel__Real
 */
#define SKEL___TO_REAL(x) (static_cast<skel__Real>((x)))

/**
 * Maximum positive real value
 * Note: Maximum negative real value is -SKEL___REAL_MAX!
 */
#define SKEL___REAL_MAX (std::numeric_limits<skel__Real>::max())

/**
 * The difference between 1.0 and the next representable value of the given
 * floating-point type. Can be used for checking floating-point "equality".
 * Explicitly Using float type because thats the threshold level of precision
 * we need (and want) for most comparison operations.
 */
#define SKEL___REAL_EPS SKEL___TO_REAL(std::numeric_limits<float>::epsilon())

/**
 * Macro for comparing real valued equality
 */
#define SKEL___REAL_CMP_EQ(a, b) \
    (std::abs((a) - (b)) < (SKEL___TO_REAL(2.0) * SKEL___REAL_EPS))

/**
 * Macro for comparing real valued a <= b
 */
#define SKEL___REAL_CMP_LTE(a, b) (SKEL___REAL_CMP_EQ((a), (b)) || ((a) < (b)))

/**
 * Macro for comparing real valued a >= b
 */
#define SKEL___REAL_CMP_GTE(a, b) SKEL___REAL_CMP_LTE((b), (a))

/**
 * Boolean type
 */
typedef unsigned int skel__Bool;

#ifdef skel__TRUE
#   error skel__TRUE already defined!
#endif  // skel__TRUE

#define skel__TRUE 1

#ifdef skel__FALSE
#   error skel__FALSE already defined!
#endif  // skel__FALSE

#define skel__FALSE 0

/**
 * Unsigned integer type
 */
typedef unsigned int skel__UInt;

/**
 * Macro for casting to skel__UInt
 */
#define SKEL___TO_UINT(x) (static_cast<skel__UInt>((x)))

/**
 * The maximum skel__UInt value
 */
#define SKEL___UINT_MAX (std::numeric_limits<skel__UInt>::max())

/**
 * Macro which is 1 if unsigned integer argument is power of 2, 0 o/w
 */
#define SKEL___UINT_IS_POW_2(ui) ((ui) && !((ui) & ((ui)-1)))

/**
 * Macro to clip a scalar 'x' to be in range [lo, hi]
 */
#define SKEL___CLIP(x_, lo_, hi_) std::max(std::min((x_), (hi_)), (lo_))

/**
 * Matrix type
 */
#include "skel__Armadillo.h"

#endif  // skel__Types_h
