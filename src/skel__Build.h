//****************************************************************************
// skel__Build.h
//****************************************************************************

/**
 * @brief Global build defines
 */

#ifndef skel__Build_h
#define skel__Build_h

/**
 * OS detection
 */
#ifdef _WIN32
#   define SKEL___BUILD_OS_WINDOWS
#else
#   define SKEL___BUILD_OS_UNIX
#endif // _WIN32

/**
 * Build types
 */
#ifndef SKEL___BUILD_TYPE_DEBUG
// _DEBUG is defined by Visual Studio (non-ANSI compliant)
// NDEBUG is defined by CMake for GCC release builds
// NDEBUG is part of C++ ANSI standard for disabling assertions
#   if defined(_DEBUG) || (defined(SKEL___BUILD_OS_UNIX) && !defined(NDEBUG))
#       define SKEL___BUILD_TYPE_DEBUG
#   else
// Switch to force enable debug build
//#       define SKEL___BUILD_TYPE_DEBUG
#   endif
#endif // SKEL___BUILD_TYPE_DEBUG

/**
 * Compiler detection
 */
#ifdef _MSC_VER
#   define SKEL___BUILD_COMPILER_MSVC
#elif (defined(__ICC) || defined(__ICL) || defined(__INTEL_COMPILER))
#   define SKEL___BUILD_COMPILER_ICC
#elif (defined(__GNUC__))
#   define SKEL___BUILD_COMPILER_GCC
#else
#   error Unrecognized compiler
#endif

/**
 * C++11
 */
// http://stackoverflow.com/questions/5047971/how-do-i-check-for-c11-support
#ifndef SKEL___BUILD_CXX_11
#   ifdef SKEL___BUILD_COMPILER_MSVC
#       if (__cplusplus >= 199711L)
#           define SKEL___BUILD_CXX_11
#       endif
#   elif (__cplusplus >= 201103L)
#       define SKEL___BUILD_CXX_11
#   endif
#endif // SKEL___BUILD_CXX_11

/**
 * This master switch toggles assertion checking
 * (will enable even if NDEBUG is defined)
 */
//#define SKEL___BUILD_ENABLE_ASSERT

// Allow assertions to be on by default for debug builds and off o/w
#ifndef SKEL___BUILD_ENABLE_ASSERT
#   ifdef SKEL___BUILD_TYPE_DEBUG
#       define SKEL___BUILD_ENABLE_ASSERT
#   else
#       ifndef NDEBUG
#           define NDEBUG
#       endif // NDEBUG
#   endif // SKEL___BUILD_TYPE_DEBUG
#endif // SKEL___BUILD_ENABLE_ASSERT

/**
 * Performs extra string parsing checks to verify data imported properly.
 * These extra checks can be significantly slower and should only be enabled
 * if data integrity is of concern.
 */
#ifndef SKEL___BUILD_ENABLE_ROBUST_PARSER_CHECKS
#   ifdef SKEL___BUILD_ENABLE_ASSERT
//#       define SKEL___BUILD_ENABLE_ROBUST_PARSER_CHECKS
#   endif // SKEL___BUILD_ENABLE_ASSERT
#endif // SKEL___BUILD_ENABLE_ROBUST_PARSER_CHECKS

/**
 * Enables logging utilities
 */
#ifndef SKEL___BUILD_ENABLE_LOGGING
// Comment out line below to disable logging
#   define SKEL___BUILD_ENABLE_LOGGING
#endif // SKEL___BUILD_ENABLE_LOGGING

/**
 * Switch to toggle stats profiling
 */
#ifndef SKEL___BUILD_ENABLE_STATS
#   ifdef SKEL___BUILD_TYPE_DEBUG
#       define SKEL___BUILD_ENABLE_STATS
#   else
// Uncomment line below to enable stats in release
//#       define SKEL___BUILD_ENABLE_STATS
#   endif // SKEL___BUILD_TYPE_DEBUG
#endif // SKEL___BUILD_ENABLE_STATS

/** 
 * Interval logging allows use of timers which auto report
 * durations to console, hence dependence on logging
 */
#if !defined(SKEL___BUILD_ENABLE_INTERVAL_LOGGING) && defined(SKEL___BUILD_ENABLE_LOGGING)
#   if defined(SKEL___BUILD_ENABLE_STATS)
#       define SKEL___BUILD_ENABLE_INTERVAL_LOGGING
#   else
// Uncomment line below to override enable even when stats are disabled
//#       define SKEL___BUILD_ENABLE_INTERVAL_LOGGING
#   endif // SKEL___BUILD_ENABLE_STATS
#endif // SKEL___BUILD_ENABLE_INTERVAL_LOGGING

/**
 * This switch toggles if commandlets should be enabled
 */
#ifndef SKEL___BUILD_ENABLE_COMMANDLETS
#   if defined(SKEL___BUILD_TYPE_DEBUG)
#       define SKEL___BUILD_ENABLE_COMMANDLETS
#   else
// Uncomment line below to enable commandlets in release
//#      define SKEL___BUILD_ENABLE_COMMANDLETS
#   endif // SKEL___BUILD_TYPE_DEBUG
#endif // SKEL___BUILD_ENABLE_COMMANDLETS

/**
 * This switch toggles if tests should be enabled
 */
#ifndef SKEL___BUILD_ENABLE_TESTS
#   if defined(SKEL___BUILD_TYPE_DEBUG) 
#       define SKEL___BUILD_ENABLE_TESTS
#   else
// Uncomment line below to enable tests in release
//#       define SKEL___BUILD_ENABLE_TESTS
#   endif // SKEL___BUILD_TYPE_DEBUG
#endif // SKEL___BUILD_ENABLE_TEST

/**
 * This switch toggles if armadillo library should be built with extra debug
 * information such as bounds checking.
 */
// Allow armadillo debug information for debug builds and off o/w
#ifndef SKEL___BUILD_ARMADILLO_DEBUG
#   ifdef SKEL___BUILD_TYPE_DEBUG
#       define SKEL___BUILD_ARMADILLO_DEBUG
#   else
#       define ARMA_NO_DEBUG
#   endif // SKEL___BUILD_TYPE_DEBUG
#endif // SKEL___BUILD_ARMADILLO_DEBUG

/**
 * Switch to toggle seeding the random number generator
 */
#ifndef SKEL___BUILD_SEED_RAND
#   if !defined(SKEL___BUILD_TYPE_DEBUG) \
        && !defined(SKEL___BUILD_ENABLE_STATS) \
        && !defined(SKEL___BUILD_ENABLE_TESTS)
#       define SKEL___BUILD_SEED_RAND
#   endif
#endif // SKEL___BUILD_SEED_RAND

/**
 * Allow threaded utilities
 */
#ifndef SKEL___BUILD_ENABLE_THREADS
#   ifdef SKEL___BUILD_COMPILER_MSVC
// (Un) comment below to toggle usage of threading in visual studio
//#       define SKEL___BUILD_ENABLE_THREADS
#   else
 // Note, for Linux builds, we typically prefer the CMake build system to
 // configure this flag; therefore, it should be undefined by default.
 // However, to override build system behavior, simply enable it here.
 //#       define SKEL___BUILD_ENABLE_THREADS
#   endif // SKEL___BUILD_COMPILER_MSVC
#endif // SKEL___BUILD_ENABLE_THREADS

#ifndef SKEL___BUILD_ENABLE_BOOST_MEM_ALIGN
// (Un) comment below to toggle usage of Boost for aligned allocations
//#   define SKEL___BUILD_ENABLE_BOOST_MEM_ALIGN
#endif // SKEL___BUILD_ENABLE_BOOST_MEM_ALIGN

/**
 * Allows bitsets to use 64-bit block size
 */
#ifndef SKEL___BUILD_ENABLE_64BIT_BLOCK
// (Un) comment below to toggle usage of 64 bit blocks
#   define SKEL___BUILD_ENABLE_64BIT_BLOCK
#endif // SKEL___BUILD_ENABLE_64BIT_BLOCK

/**
 * Allows use of popcnt intrinsic for counting number
 * of bits in a dynamic bitset
 */
#ifndef SKEL___BUILD_ENABLE_POPCNT
// (Un) comment below to toggle usage of popcnt intrinsic
#   define SKEL___BUILD_ENABLE_POPCNT
#endif // SKEL___BUILD_ENABLE_POPCNT

/**
 * Prefix to denote command line arguments meant for child configurations
 */
#define SKEL___CONFIG_CHILD_CMDARG_PREFIX "."

#endif  // skel__Build_h
