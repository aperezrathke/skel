//****************************************************************************
// skel__TestGlobalInit.h
//****************************************************************************

/**
 * Commandlet for testing initialization of global data. This serves as the
 * basic reference template for writing a minimal application.
 *
 * Usage:
 * -test_global_init |optional configuration params|
 */

#ifdef skel__TestGlobalInit_h
#   error "Test Global Init included multiple times!"
#endif  // skel__TestGlobalInit_h
#define skel__TestGlobalInit_h

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_TESTS

#include "skel__Config.h"
#include "skel__ExitCodes.h"
#include "skel__Globals.h"
#include "skel__Stats.h"

/**
 * Entry point for test
 */
int skel__TestGlobalInitMain(const skel__CmdOptsMap& cmd_opts) {
    // Create configuration
    skel__SpConfig_t config = skel__SmartPtr::make_shared<skel__Config>();
    // Parse command line and any INI configuration (override defaults)
    config->init(cmd_opts);

    // Output configuration
    skel__Logf("Testing global initialization: configuration set to:\n");
    config->print();

    // Initialize globals
    skel__Logf("Testing global initialization: calling init...\n");
    skel__G::init(config);

    SKEL___LOG_NUM_WORKER_THREADS;

    /**
     * Do application work here!
     */

    SKEL___STATS_REPORT;

    // Destroy globals
    skel__Logf("Testing global initialization: calling teardown...\n");
    skel__G::teardown();

    // We consider it a success if we make it here without crashing!
    skel__Logf("Testing global initialization: test finished.\n");
    return skel__ExitCode_normal;
}

#endif  // SKEL___BUILD_ENABLE_TESTS
