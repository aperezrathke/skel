//****************************************************************************
// skel__Stats.h
//****************************************************************************

/**
 * @brief Simple stats structures for profiling and tracking counts
 */

#ifndef skel__Stats_h
#define skel__Stats_h

#include "skel__Build.h"

// For convenience - include interval logging
#include "skel__IntervalLogger.h"

#ifdef SKEL___BUILD_ENABLE_STATS

#include "skel__Assert.h"
#include "skel__Logf.h"
#include "skel__Thread.h"
#include "skel__Time.h"
#include "skel__Types.h"

/**
 * Enumerated unique identifiers for all timer stats
 */
enum skel__AllTimerStats {
    skel__STAT_GlobalInitTime = 0,
    skel__STAT_TimerMAX
};

/**
 * Enumerated unique identifiers for all counter stats
 */
enum skel__AllCounterStats {
    skel__STAT_GlobalInitCount = 0,
    skel__STAT_CounterMAX
};

#define SKEL___SCOPED_STAT_TIMER(stat_id) \
    skel__Stats::ScopedStatTimer ScopedTimer_##stat_id(stat_id)

#define SKEL___INC_STAT_COUNTER(stat_id) \
    skel__Stats::GCounterStats[stat_id].inc_by(1)

#define SKEL___INC_STAT_COUNTER_BY(stat_id, amount) \
    skel__Stats::GCounterStats[stat_id].inc_by(amount)

namespace skel__Stats {

/**
 * For timing function calls
 */
class TimerStat {
public:
    TimerStat() : m_total_time(0.0) {
#ifdef SKEL___BUILD_ENABLE_THREADS
        skel__Assert(m_total_time.is_lock_free());
#endif  // SKEL___BUILD_ENABLE_THREADS
    }

    inline void inc_total_time(const double t) {
        SKEL___ATOMIC_ADD_DOUBLE(m_total_time, t);
    }
    inline double get_total_time() const { return m_total_time; }

private:
    /**
     * The total time of this stat
     */
    skel__AtomicDouble_t m_total_time;
};

/**
 * For counting how many times an event occurs
 */
class CounterStat {
public:
    CounterStat() : m_counter(0) {
#ifdef SKEL___BUILD_ENABLE_THREADS
        skel__Assert(m_counter.is_lock_free());
#endif  // SKEL___BUILD_ENABLE_THREADS
    }

    inline void inc_by(const skel__UInt amount) { m_counter += amount; }
    inline skel__UInt get_counter() const { return m_counter; }

private:
    skel__AtomicUInt m_counter;
};

/**
 * All the timer stat instances
 */
extern TimerStat GTimerStats[skel__STAT_TimerMAX];

/**
 * All the counter stat instances
 */
extern CounterStat GCounterStats[skel__STAT_CounterMAX];

/**
 * A synchronous timer which updates on destruction
 */
class ScopedStatTimer {
public:
    explicit ScopedStatTimer(const skel__AllTimerStats stat_id)
        : m_stat_id(stat_id), m_start_time(skel___time_secs_wall()) {}

    ~ScopedStatTimer() {
        GTimerStats[m_stat_id].inc_total_time(skel___time_secs_wall() -
                                              m_start_time);
    }

private:
    const skel__AllTimerStats m_stat_id;
    const double m_start_time;
};

/**
 * Report stats - print to stdout
 */
extern void report_stats();

}  // namespace skel__Stats

#define SKEL___STATS_REPORT skel__Stats::report_stats()

#else  // !SKEL____BUILD_ENABLE_STATS

#define SKEL___SCOPED_STAT_TIMER(stat_id) ((void)0)
#define SKEL___INC_STAT_COUNTER(stat_id) ((void)0)
#define SKEL___INC_STAT_COUNTER_BY(stat_id, amount) ((void)0)
#define SKEL___STATS_REPORT ((void)0)

#endif  // SKEL___BUILD_ENABLE_STATS

#endif  // skel__Stats_h
