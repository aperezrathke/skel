//****************************************************************************
// skel__SmartPtr.h
//****************************************************************************

/**
 * @brief Selects between C++11 and boost smart pointer implementations
 */

#ifndef skel__SmartPtr_h
#define skel__SmartPtr_h

#include "skel__Build.h"

// Check if C++11 is enabled
#ifdef SKEL___BUILD_CXX_11
#   include <memory>
#   define skel__SmartPtr std
#else
// Else, defer to boost implementation
#   include <boost/enable_shared_from_this.hpp>
#   include <boost/make_shared.hpp>
#   include <boost/shared_ptr.hpp>
#   include <boost/weak_ptr.hpp>
#   define skel__SmartPtr boost
#endif

#endif  // skel__SmartPtr_h
