//****************************************************************************
// skel__ThreadNullTLS.inl
//****************************************************************************

/**
 * WARNING: OVERWRITES THREAD LOCAL STORAGE (TLS) MACROS TO NULL
 * IMPLEMENTATION.
 *
 * This can be useful if you want to a serial implementation of a class that
 * will only ever run on a single thread but in a multi-threaded environment.
 *
 * MAKE SURE YOU KNOW WHAT YOU ARE DOING WHEN INCLUDING THIS FILE AS THESE
 * MACROS WILL OVERWRITE ANY COMMON DEFINES WITHIN skel__Thread.h!!!
 */

/**
 * Undefine any previously existing macros
 */

#ifdef SKEL___DECLARE_TLS_DATA
#   undef SKEL___DECLARE_TLS_DATA
#endif  // SKEL___DECLARE_TLS_DATA

#ifdef SKEL___ACCESS_TLS_DATA
#   undef SKEL___ACCESS_TLS_DATA
#endif  // SKEL___ACCESS_TLS_DATA

#ifdef SKEL___INIT_TLS_DATA_0_PARAM
#   undef SKEL___INIT_TLS_DATA_0_PARAM
#endif  // SKEL___INIT_TLS_DATA_0_PARAM

#ifdef SKEL___INIT_TLS_DATA_FOR_EACH_1_PARAM
#   undef SKEL___INIT_TLS_DATA_FOR_EACH_1_PARAM
#endif  // SKEL___INIT_TLS_DATA_FOR_EACH_1_PARAM

#ifdef SKEL___INIT_TLS_DATA_FOR_EACH_2_PARAM
#   undef SKEL___INIT_TLS_DATA_FOR_EACH_2_PARAM
#endif  // SKEL___INIT_TLS_DATA_FOR_EACH_2_PARAM

#ifdef SKEL___TLS_DATA_FOR_EACH_0_PARAM
#   undef SKEL___TLS_DATA_FOR_EACH_0_PARAM
#endif  // SKEL___TLS_DATA_FOR_EACH_0_PARAM

/**
 * Redefine macros
 */

#define SKEL___DECLARE_TLS_DATA(type, name) type name

#define SKEL___ACCESS_TLS_DATA(name) name

#define SKEL___INIT_TLS_DATA_0_PARAM(name) ((void)0)

#define SKEL___INIT_TLS_DATA_FOR_EACH_1_PARAM(name, memb_func, param_1) \
    name.memb_func(param_1)

#define SKEL___INIT_TLS_DATA_FOR_EACH_2_PARAM( \
    name, memb_func, param_1, param_2)         \
    name.memb_func(param_1, param_2)

#define SKEL___TLS_DATA_FOR_EACH_0_PARAM(name, memb_func) name.memb_func()
