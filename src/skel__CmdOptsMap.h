//****************************************************************************
// skel__CmdOptsMap.h
//****************************************************************************

/**
 * @brief Utilities for parsing stdin command line
 */

#ifndef skel__CmdOptsMap_h
#define skel__CmdOptsMap_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__StringUtils.h"
#include "skel__Types.h"

#include <map>
#include <sstream>
#include <string>
#include <vector>

//****************************************************************************
// skel__CmdOpts
//****************************************************************************

/**
 * Parses command line into a map structure
 *
 * All commands are assumed to be of form -<opt> <opt_value> or --<switch>.
 * So, a single dash (-) indicates a unary boolean switch and two dashes (--)
 * indicate a binary <key, value> pair.
 *
 * Note: unary (-) switch keys will still be mapped to an empty string value
 */
class skel__CmdOptsMap {
public:
    /**
     * Default constructor
     */
    skel__CmdOptsMap() {}

    /**
     * Constructor initializes from command arguments
     *
     * @param argc - the number of command line arguments
     * @param argv - character string array of command line arguments
     */
    skel__CmdOptsMap(const int argc, const char** argv);

    /**
     * Parses command line
     *
     * Must be called before using accessor methods such as get_*_value
     *
     * @param argc - the number of command line arguments
     * @param argv - character string array of command line arguments
     * @return TRUE if no parse errors, FALSE otherwise
     */
    skel__Bool parse(const int argc, const char** argv);

    /**
     * Search for value mapped to option key
     *
     * parse() must be called prior to using this method.
     *
     * @param out_val - The output value mapped to parameter opt_key,
     *      unmodified if opt_key does not exist in command options map
     * @param opt_key - The key to search for in command map
     * @return TRUE if key exists, FALSE otherwise
     */
    template <class T>
    skel__Bool read_into(T& out_val, const std::string& opt_key) const {
        const cmd_map_t::const_iterator it(cmd_opts.find(opt_key));
        if (it != cmd_opts.end()) {
            out_val = string_as_T<T>(it->second);
            return skel__TRUE;
        }
        return skel__FALSE;
    }

    /**
     * Mirror skel__ConfigFile interface, returns path unmodified if existing
     *
     * parse() must be called prior to using this method.
     *
     * @param path - output path as entered on command line, argument is
     *  unmodified if not found
     * @param opt_key - The key to search for in command map
     * @return TRUE if key exists, FALSE otherwise
     */
    skel__Bool resolve_path(std::string& path,
                            const std::string& opt_key) const {
        return this->read_into(path, opt_key);
    }

    /**
     * parse() must be called prior to using this method
     *
     * @return TRUE if option key exists, FALSE otherwise
     */
    inline skel__Bool key_exists(const std::string& opt_key) const {
        return this->cmd_opts.find(opt_key) != this->cmd_opts.end();
    }

    /**
     * Obtain all registered option keys
     */
    void get_keys(std::vector<std::string>& keys) const {
        for (cmd_map_t::const_iterator it = cmd_opts.begin();
             it != cmd_opts.end();
             ++it) {
            keys.push_back(it->first);
        }
    }

    /**
     * @return TRUE if no option keys exist, FALSE o/w
     */
    inline bool empty() const { return cmd_opts.empty(); }

    /**
     * Allow clients to set options programmatically
     *
     * @param opt_key - The string key to map to opt_val
     * @param opt_val - The value to assign
     */
    template <typename t_value>
    void set_option(const std::string& opt_key, const t_value& opt_val) {
        this->cmd_opts[opt_key] = skel__2Str(opt_val);
    }

    /**
     * Overload for character type
     *
     * Allow clients to set options programmatically
     *
     * @param opt_key - The string key to map to opt_val
     * @param opt_val - The string value to assign
     */
    void set_option(const std::string& opt_key, const char* opt_val) {
        this->cmd_opts[opt_key] = opt_val;
    }

    /**
     * Conditionally sets option value if 'opt_key' not already existing
     *
     * @param opt_key - The string key to map to opt_val
     * @param opt_val - The value to conditionally assign
     */
    template <typename t_value>
    void set_option_if_absent(const std::string& opt_key,
                              const t_value& opt_val) {
        if (!this->key_exists(opt_key)) {
            this->set_option(opt_key, opt_val);
        }
    }

    /**
     * Extract only command line options destined for child configurations
     * @param out - output command line options (@WARNING - will be cleared!)
     */
    void get_child_cmd(skel__CmdOptsMap& out) const;

private:
    /**
     * Utility method to convert from a string type to any other data type
     *
     * Copied from ConfigFile.h (protected member)
     *
     * @param s - string to convert
     * @return the value of the converted string
     */
    template <class T>
    static T string_as_T(const std::string& s) {
        return skel__StringUtils::string_as_T<T>(s);
    }

    // Data structure for mapping <key, value> pairs
    typedef std::map<std::string, std::string> cmd_map_t;

    // Internal map of command line options
    cmd_map_t cmd_opts;
};

#endif  // skel__CmdOptsMap_h
