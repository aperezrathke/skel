//****************************************************************************
// skel__StaticAssert.h
//****************************************************************************

/**
 * @brief Compile time assertion support
 */

#ifndef skel__StaticAssert_h
#define skel__StaticAssert_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"

//****************************************************************************
// U_STATIC_ASSERT
//****************************************************************************

#ifdef SKEL___BUILD_CXX_11
#   include <type_traits>
    /**
     * Compile time assert for expression x. Note, C++11 does not support
     * static_assert w/o a msg. To overcome this, use trick that boost uses
     * and convert input expression to a string (C++17 does support w/o msg).
     */
#   define SKEL___STATIC_ASSERT(x) static_assert( x, #x )
    /**
     * Compile time assert for expression x with error message.
     */
#   define SKEL___STATIC_ASSERT_MSG(x, msg) static_assert(x, msg)
#else
    /**
     * C++11 not supported, defer to boost implementations
     */
#   include <boost/static_assert.hpp>
#   define SKEL___STATIC_ASSERT(x) BOOST_STATIC_ASSERT(x)
#   define SKEL___STATIC_ASSERT_MSG(x, msg) BOOST_STATIC_ASSERT_MSG(x, msg)
#endif // SKEL___BUILD_CXX_11

#endif  // skel__StaticAssert_h
