//****************************************************************************
// skel__Quantile.h
//****************************************************************************

/**
 * @brief - Obtain quantiles from data set, currently only support sorted data
 */

/**
 * THIS CODE IS BASED ON THE GNU SCIENTIFIC LIBRARY (GSL) METHOD:
 *
 *  gsl_stats_quantile_from_sorted_data(...)
 *
 * Therefore, am including GSL's license header below:
 */

/* statistics/quantiles_source.c
 *
 * Copyright (C) 1996, 1997, 1998, 1999, 2000, 2007 Jim Davies, Brian Gough
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef skel__Quantile_h
#define skel__Quantile_h

#include "skel__Build.h"
#include "skel__Assert.h"
#include "skel__Types.h"

#include <vector>

/**
 * Effectively namespace for quantile utilities
 */
class skel__Quantile {
public:
    /**
     * @param sorted_data - array of ascending sorted data
     * @param n - number of elements contained in sorted data
     * @param f - fraction in [0,1]
     * @return interpolated quantile corresponding to f
     */
    static skel__Real get_q_from_sorted(const skel__Real* const sorted_data,
                                        const size_t n,
                                        const skel__Real f) {
        skel__Assert(is_sorted(sorted_data, n));
        skel__AssertBoundsInc(f, SKEL___TO_REAL(0.0), SKEL___TO_REAL(1.0));
        const skel__Real index = f * (n - 1);
        const size_t lhs = (int)index;
        const skel__Real delta = index - lhs;
        skel__Real result;

        if (n == 0) {
            return SKEL___TO_REAL(0.0);
        }

        if (lhs == (n - 1)) {
            result = sorted_data[lhs];
        } else {
            skel__Assert((lhs + 1) < n);
            result =
                (1 - delta) * sorted_data[lhs] + delta * sorted_data[(lhs + 1)];
        }

        return result;
    }

    /**
     * Overload for working with real-valued column vector
     * @param sorted_data - ascending sorted real-valued column vector
     * @param f - fraction in [0,1]
     * @return interpolated quantile corresponding to f
     */
    static skel__Real get_q_from_sorted(const skel__VecCol& sorted_data,
                                        const skel__Real f) {
        if (sorted_data.empty()) {
            return SKEL___TO_REAL(0.0);
        }
        return get_q_from_sorted(&(sorted_data.at(0)), sorted_data.n_elem, f);
    }

    /**
     * Overload for working with real-valued std::vector
     * @param sorted_data - ascending sorted real-valued column vector
     * @param f - fraction in [0,1]
     * @return interpolated quantile corresponding to f
     */
    static skel__Real get_q_from_sorted(
        const std::vector<skel__Real>& sorted_data,
        const skel__Real f) {
        if (sorted_data.empty()) {
            return SKEL___TO_REAL(0.0);
        }
        return get_q_from_sorted(&(sorted_data[0]), sorted_data.size(), f);
    }

private:
#ifdef SKEL___BUILD_ENABLE_ASSERT
    /**
     * @return TRUE if array is ascending sorted, FALSE o/w
     */
    static bool is_sorted(const skel__Real* const v, const size_t n) {
        for (size_t i = 1; i < n; ++i) {
            if (v[i - 1] > v[i]) {
                return false;
            }
        }
        return true;
    }
#endif  // SKEL___BUILD_ENABLE_ASSERT
};

#endif  // skel__Quantile
