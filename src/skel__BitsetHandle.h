//****************************************************************************
// skel__BitsetHandle.h
//****************************************************************************

/**
 * Wrapper interface handles to either a skel__Bitset or a bit set
 * from a pooled array. This unifies both interfaces but also allows pooled
 * bit set to call on the parent (outer) array for shared properties.
 *
 * The metaphor 'soft' is meant to imply that the wrapper does not own the
 * underlying bit buffer. It does not manage (allocate/destroy) the underlying
 * bit buffer, it simply provides bit manipulation functionality.
 */

#ifndef skel__BitsetHandle_h
#define skel__BitsetHandle_h

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__Assert.h"
#include "skel__Bitset.h"
#include "skel__BitsetCompileTimeProperties.h"
#include "skel__FindLowestBitPositionUtil.h"

#include <algorithm>

//****************************************************************************
// skel__BitsetHandle
//****************************************************************************

/**
 * A class that wraps around a raw bit buffer and allows bit manipulation
 */
template <typename t_BitsetRunTimeProperties>
class skel__BitsetHandle : public skel__BitsetCompileTimeProperties {
public:
    /**
     * Constructor to initialize bit buffer and runtime properties
     */
    skel__BitsetHandle(block_type* bits,
                       const t_BitsetRunTimeProperties& runtime_properties)
        : m_bits(bits), m_runtime_properties(runtime_properties) {}

    // BEGIN HACK
    /**
     * Constructor which wraps a const buffer, this should only be used if
     * creating a const handle.
     * http://stackoverflow.com/questions/6936124/why-does-c-not-have-a-const-constructor
     */
    skel__BitsetHandle(const block_type* bits,
                       const t_BitsetRunTimeProperties& runtime_properties)
        : m_bits(const_cast<block_type*>(bits)),
          m_runtime_properties(runtime_properties) {}
    // END HACK

    /**
     * @return The number of bits in the bit set
     */
    inline size_type size() const { return m_runtime_properties.size(); }

    /**
     * @return The number of blocks necessary to represent a bit set
     */
    inline size_type num_blocks() const {
        return m_runtime_properties.num_blocks();
    }

    /**
     * @return The number of bytes necessary to represent a bit set
     */
    inline size_type num_bytes() const {
        return m_runtime_properties.num_bytes();
    }

    /**
     * We need to expose underlying buffer!
     * Non-const version
     */
    inline block_type* get_buffer() {
        skel__Assert(m_bits);
        return m_bits;
    };

    /**
     * We need to expose underlying buffer!
     * Const version
     */
    inline const block_type* get_buffer() const {
        skel__Assert(m_bits);
        return m_bits;
    };

        // bitset operations - based on boost::dynamic_bitset

#ifdef SKEL___BUILD_CXX_11
    template <typename t_bitset>
    skel__BitsetHandle& operator=(const t_bitset&& rhs);
#else
    skel__BitsetHandle& operator=(const skel__BitsetHandle&);
    template <typename t_bitset>
    skel__BitsetHandle& operator=(const t_bitset& rhs);
#endif  // SKEL___BUILD_CXX_11

    template <typename t_bitset>
    skel__BitsetHandle& operator&=(const t_bitset& rhs);

    template <typename t_bitset>
    skel__BitsetHandle& operator|=(const t_bitset& rhs);

    template <typename t_bitset>
    skel__BitsetHandle& operator^=(const t_bitset& rhs);

    template <typename t_bitset>
    skel__BitsetHandle& operator-=(const t_bitset& rhs);

    skel__BitsetHandle& operator<<=(const size_type n);
    skel__BitsetHandle& operator>>=(size_type n);
    skel__Bitset operator<<(size_type n) const;
    skel__Bitset operator>>(size_type n) const;

    // basic bit operations - based on boost::dynamic_bitset

    skel__BitsetHandle& set(size_type n, bool val = true);
    skel__BitsetHandle& set();
    skel__BitsetHandle& reset(size_type n);
    skel__BitsetHandle& reset();
    skel__BitsetHandle& flip(size_type n);
    skel__BitsetHandle& flip();
    bool test(size_type n) const;
    bool any() const;
    bool none() const;
    skel__Bitset operator~() const;
    size_type count() const;

    template <typename t_bitset>
    bool is_subset_of(const t_bitset& a) const;

    template <typename t_bitset>
    bool is_proper_subset_of(const t_bitset& a) const;

    template <typename t_bitset>
    bool intersects(const t_bitset& b) const;

    // lookup / bit iteration routines - based on boost::dynamic_bitset

    size_type find_first() const;
    size_type find_next(size_type pos) const;

    /**
     * Converts this handle into a stand-alone 'hard' bit set which manages
     * its own internal bit buffer
     * @return the converted parameter bit set
     */
    skel__Bitset& to_hard_bitset(skel__Bitset& out_bit_set) const;

    // comparison

    template <typename t_BitsetRunTimeProperties_, typename t_bitset>
    friend bool operator==(
        const skel__BitsetHandle<t_BitsetRunTimeProperties_>& a,
        const t_bitset& b);

private:
    bool m_unchecked_test(size_type pos) const;
    size_type m_do_find_from(size_type first_block) const;
    index_type m_lowest_bit_position(block_type b) const;

    // Utilities - based on boost::dynamic_bitset
    block_type& m_highest_block();
    const block_type& m_highest_block() const;
    void m_zero_unused_bits();
    inline block_width_type count_extra_bits() const {
        return bit_index(size());
    }
    inline static size_type block_index(size_type pos) {
        return pos / bits_per_block;
    }
    inline static block_width_type bit_index(size_type pos) {
        return static_cast<block_width_type>(pos % bits_per_block);
    }
    inline static block_type bit_mask(size_type pos) {
        return block_type(1) << bit_index(pos);
    }

    /**
     * The raw bit buffer for bit manipulation
     */
    block_type* m_bits;

    /**
     * Properties to determine bit, block, and byte counts
     */
    const t_BitsetRunTimeProperties m_runtime_properties;
};

//****************************************************************************
// skel__BitsetUtils
//****************************************************************************

// BEGIN EVIL HACK - expose boost::dynamic_bitset::m_bits

// http://www.gotw.ca/gotw/076.htm
// In order to allow efficient interoperability between soft bit sets and hard
// bit sets, it would make our lives easier if we had access to the underlying
// buffer used by boost::dynamic_bitset (skel__Bitset). This buffer is private
// so we need to do some evil hacks to expose it. The hack in question involves
// specializing a template member function to get at what we need. This is
// evil.

// First, create a unique type to specialize on:
struct skel__BitsetExposePrivateMemberHack_t {
    skel__BitsetCompileTimeProperties::block_type** mpp_bits;
};

// Next, specialize template member function to expose private member:
namespace boost {
    template <>
    template <>
    inline void skel__Bitset::append(skel__BitsetExposePrivateMemberHack_t first,
        skel__BitsetExposePrivateMemberHack_t last) {
        *first.mpp_bits =
            (skel__BitsetCompileTimeProperties::block_type*)(&(this->m_bits[0]));
    }
}  // namespace boost
// END EVIL HACK

/**
 * An adapter/bridge namespace for operations common to various bitset
 * implementations such as boost::dynamic_bitset and uSoftBitsetHandle
 */
namespace skel__BitsetUtils {
    /**
     * Generic function to get underlying bit buffer
     */
    template <typename t_bitset>
    inline skel__BitsetCompileTimeProperties::block_type* get_bitset_buffer(
        const t_bitset& bitset) {
        skel__Assert(false && "No template match for bit set type!");
        return NULL;
    }

    // BEGIN EVIL HACK  - expose skel__Bitset::m_bits

    // Finally, create our interface function to utilize the loophole:
    /**
     * Specialized template to get underlying bit buffer in a skel__Bitset
     */
    inline skel__BitsetCompileTimeProperties::block_type* get_bitset_buffer(
        skel__Bitset& bitset) {
        skel__BitsetCompileTimeProperties::block_type* out_buffer = NULL;
        skel__BitsetExposePrivateMemberHack_t evil_hack = {&out_buffer};
        bitset.append(evil_hack, evil_hack);
        skel__Assert(out_buffer != NULL);
        return out_buffer;
    }

    /**
     * Specialize template to get underlying const bit buffer in a skel__Bitset
     */
    inline const skel__BitsetCompileTimeProperties::block_type*
        get_const_bitset_buffer(const skel__Bitset& bitset) {
        return get_bitset_buffer(const_cast<skel__Bitset&>(bitset));
    }

    // END EVIL HACK

    /**
     * Specialized template to get underlying bit buffer in a pooled bitset handle
     */
    template <typename t_BitsetRunTimeProperties>
    inline skel__BitsetCompileTimeProperties::block_type* get_bitset_buffer(
        skel__BitsetHandle<t_BitsetRunTimeProperties>& bitset) {
        return &(bitset.get_buffer()[0]);
    }

    /**
     * Specialized template to get underlying const bit buffer in a pooled bitset
     * handle
     */
    template <typename t_BitsetRunTimeProperties>
    inline const skel__BitsetCompileTimeProperties::block_type*
        get_const_bitset_buffer(
            const skel__BitsetHandle<t_BitsetRunTimeProperties>& bitset) {
        return &(bitset.get_buffer()[0]);
    }

    /**
     * Generic function to get number of bytes in a bitset
     */
    template <typename t_bitset>
    inline skel__BitsetCompileTimeProperties::size_type get_bitset_num_bytes(
        const t_bitset& bitset) {
        skel__Assert(false && "No template match for bit set type!");
        return 0;
    }

    /**
     * Specialized template to determine number of bytes in a skel__Bitset
     */
    inline skel__BitsetCompileTimeProperties::size_type get_bitset_num_bytes(
        const skel__Bitset& bitset) {
        return bitset.num_blocks() * sizeof(skel__Bitset::block_type);
    }

    /**
     * Specialized template to determine number of bytes in a pooled bitset handle
     */
    template <typename t_BitsetRunTimeProperties>
    inline skel__BitsetCompileTimeProperties::size_type get_bitset_num_bytes(
        const skel__BitsetHandle<t_BitsetRunTimeProperties>& bitset) {
        return bitset.num_bytes();
    }

    /**
     * @return minimum number of bytes between the two bit set
     */
    template <typename t_BitsetA, typename t_BitsetB>
    inline skel__BitsetCompileTimeProperties::size_type get_bitset_min_num_bytes(
        const t_BitsetA& bitset_a,
        const t_BitsetB& bitset_b) {
        return std::min<>(get_bitset_num_bytes(bitset_a),
                          get_bitset_num_bytes(bitset_b));
    }

}  // end of namespace skel__BitsetUtils

/////////////////////////////////////

//****************************************************************************
// skel__BitsetWrapperRunTimeProperties
//****************************************************************************

/**
 * Run time properties policy for use in uSoftBitsetHandle
 * Wraps a skel__Bitset
 */
class skel__BitsetWrapperRunTimeProperties {
public:
    /**
     * @return The number of bits in the bit set
     */
    inline skel__Bitset::size_type size() const {
        return m_parent_bit_set.size();
    }

    /**
     * @return The number of blocks necessary to represent a bit set
     */
    inline skel__Bitset::size_type num_blocks() const {
        return m_parent_bit_set.num_blocks();
    }

    /**
     * @return The number of bytes necessary to represent a bit set
     */
    inline skel__Bitset::size_type num_bytes() const {
        return skel__BitsetUtils::get_bitset_num_bytes(m_parent_bit_set);
    }

    /**
     * Constructor
     */
    explicit skel__BitsetWrapperRunTimeProperties(
        const skel__Bitset& parent_bit_set)
        : m_parent_bit_set(parent_bit_set) {}

private:
    /**
     * Reference to bit set used for defining the run time properties
     */
    const skel__Bitset& m_parent_bit_set;
};

//****************************************************************************
// skel__BitsetHandle
//****************************************************************************

//-----------------------------------------------------------------------------
// bitset operations

// @WARNING - assignment operator assumes lhs and rhs are same buffer size.
//  If this is not the case, then no overflow will occur but it is possible
//  that an unallocated portion of memory will be read (leading to a segfault).

#ifdef SKEL___BUILD_CXX_11

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator=(
        const t_bitset&& rhs) {
    skel__Assert(size() == rhs.size());
    skel__Assert(num_blocks() == rhs.num_blocks());
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(skel__BitsetUtils::get_const_bitset_buffer(rhs));
    memcpy(get_buffer(), rhs_buff, num_bytes());
    return *this;
}

#else

// Overload default assignment operator
template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator=(
        const skel__BitsetHandle<t_BitsetRunTimeProperties>& rhs) {
    skel__Assert(size() == rhs.size());
    skel__Assert(num_blocks() == rhs.num_blocks());
    const block_type* rhs_buff(skel__BitsetUtils::get_const_bitset_buffer(rhs));
    memcpy(get_buffer(), rhs_buff, num_bytes());
    return *this;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator=(
        const t_bitset& rhs) {
    skel__Assert(size() == rhs.size());
    skel__Assert(num_blocks() == rhs.num_blocks());
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(skel__BitsetUtils::get_const_bitset_buffer(rhs));
    memcpy(get_buffer(), rhs_buff, num_bytes());
    return *this;
}

#endif  // SKEL___BUILD_CXX_11

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator&=(
        const t_bitset& rhs) {
    skel__Assert(size() == rhs.size());
    skel__Assert(num_blocks() == rhs.num_blocks());
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(skel__BitsetUtils::get_const_bitset_buffer(rhs));
    const size_type num_blocks_invariant = this->num_blocks();
    for (size_type i = 0; i < num_blocks_invariant; ++i) {
        m_bits[i] &= rhs_buff[i];
    }
    return *this;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator|=(
        const t_bitset& rhs) {
    skel__Assert(size() == rhs.size());
    skel__Assert(num_blocks() == rhs.num_blocks());
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(skel__BitsetUtils::get_const_bitset_buffer(rhs));
    const size_type num_blocks_invariant = this->num_blocks();
    for (size_type i = 0; i < num_blocks_invariant; ++i) {
        m_bits[i] |= rhs_buff[i];
    }
    return *this;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator^=(
        const t_bitset& rhs) {
    skel__Assert(size() == rhs.size());
    skel__Assert(num_blocks() == rhs.num_blocks());
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(skel__BitsetUtils::get_const_bitset_buffer(rhs));
    const size_type num_blocks_invariant = this->num_blocks();
    for (size_type i = 0; i < num_blocks_invariant; ++i) {
        m_bits[i] ^= rhs_buff[i];
    }
    return *this;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator-=(
        const t_bitset& rhs) {
    skel__Assert(size() == rhs.size());
    skel__Assert(num_blocks() == rhs.num_blocks());
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* rhs_buff(skel__BitsetUtils::get_const_bitset_buffer(rhs));
    const size_type num_blocks_invariant = this->num_blocks();
    for (size_type i = 0; i < num_blocks_invariant; ++i)
        m_bits[i] &= ~rhs_buff[i];
    return *this;
}

//-----------------------------------------------------------------------------

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
bool skel__BitsetHandle<t_BitsetRunTimeProperties>::is_subset_of(
    const t_bitset& a) const {
    skel__Assert(size() == a.size());
    skel__Assert(num_blocks() == a.num_blocks());
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    const block_type* a_buff(skel__BitsetUtils::get_const_bitset_buffer(a));
    for (size_type i = 0; i < num_blocks(); ++i) {
        if (m_bits[i] & ~a_buff[i]) {
            return false;
        }
    }
    return true;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
bool skel__BitsetHandle<t_BitsetRunTimeProperties>::is_proper_subset_of(
    const t_bitset& a) const {
    skel__Assert(size() == a.size());
    skel__Assert(num_blocks() == a.num_blocks());
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));

    bool proper = false;

    const block_type* a_buff(skel__BitsetUtils::get_const_bitset_buffer(a));
    for (size_type i = 0; i < num_blocks(); ++i) {
        const block_type& bt = m_bits[i];
        const block_type& ba = a_buff[i];

        if (bt & ~ba)
            return false;  // not a subset at all
        if (ba & ~bt)
            proper = true;
    }

    return proper;
}

template <typename t_BitsetRunTimeProperties>
template <typename t_bitset>
bool skel__BitsetHandle<t_BitsetRunTimeProperties>::intersects(
    const t_bitset& b) const {
    skel__Assert(sizeof(block_type) == sizeof(typename t_bitset::block_type));
    size_type common_blocks =
        (num_blocks() < b.num_blocks()) ? num_blocks() : b.num_blocks();
    const block_type* b_buff(skel__BitsetUtils::get_const_bitset_buffer(b));
    for (size_type i = 0; i < common_blocks; ++i) {
        if (m_bits[i] & b_buff[i]) {
            return true;
        }
    }
    return false;
}

//-----------------------------------------------------------------------------

template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator<<=(
        const size_type n) {
    // If shift amount is greater than bit set size, then just reset everything
    // to zero
    if (n >= size()) {
        return reset();
    }

    // else
    if (n > 0) {
        const size_type last = num_blocks() - 1;   // num_blocks() is >= 1
        const size_type div = n / bits_per_block;  // div is <= last
        const block_width_type r = bit_index(n);
        block_type* const b = &m_bits[0];

        if (r != 0) {
            block_width_type const rs = bits_per_block - r;

            for (size_type i = last - div; i > 0; --i) {
                b[i + div] = (b[i] << r) | (b[i - 1] >> rs);
            }
            b[div] = b[0] << r;
        } else {
            for (size_type i = last - div; i > 0; --i) {
                b[i + div] = b[i];
            }
            b[div] = b[0];
        }

        // zero out div blocks at the less significant end
        std::fill_n(b, div, static_cast<block_type>(0));

        // zero out any 1 bit that flowed into the unused part
        m_zero_unused_bits();  // thanks to Lester Gong
    }

    return *this;
}

// Based on boost::dynamic_bitset
template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::operator>>=(size_type n) {
    if (n >= size()) {
        return reset();
    }

    // else
    if (n > 0) {
        const size_type last = num_blocks() - 1;   // num_blocks() is >= 1
        const size_type div = n / bits_per_block;  // div is <= last
        const block_width_type r = bit_index(n);
        block_type* const b = &m_bits[0];

        if (r != 0) {
            block_width_type const ls = bits_per_block - r;

            for (size_type i = div; i < last; ++i) {
                b[i - div] = (b[i] >> r) | (b[i + 1] << ls);
            }
            // r bits go to zero
            b[last - div] = b[last] >> r;
        } else {
            for (size_type i = div; i <= last; ++i) {
                b[i - div] = b[i];
            }
            // note the '<=': the last iteration 'absorbs'
            // b[last-div] = b[last] >> 0;
        }

        // div blocks are zero filled at the most significant end
        std::fill_n(b + (num_blocks() - div), div, static_cast<block_type>(0));
    }

    return *this;
}

template <typename t_BitsetRunTimeProperties>
skel__Bitset skel__BitsetHandle<t_BitsetRunTimeProperties>::operator<<(
    size_type n) const {
    skel__Bitset r;
    return to_hard_bitset(r) <<= n;
}

template <typename t_BitsetRunTimeProperties>
skel__Bitset skel__BitsetHandle<t_BitsetRunTimeProperties>::operator>>(
    size_type n) const {
    skel__Bitset r;
    return to_hard_bitset(r) >>= n;
}

//-----------------------------------------------------------------------------
// global bitset operators

template <typename t_BitsetRunTimeProperties, typename t_bitset>
skel__Bitset operator&(const skel__BitsetHandle<t_BitsetRunTimeProperties>& x,
                       const t_bitset& y) {
    skel__Assert(
        sizeof(skel__BitsetHandle<t_BitsetRunTimeProperties>::block_type) ==
        sizeof(skel__Bitset::block_type));
    skel__Bitset b;
    x.to_hard_bitset(b);
    skel__BitsetCompileTimeProperties::block_type* b_buff(
        skel__BitsetUtils::get_bitset_buffer(b));
    skel__BitsetHandle<skel__BitsetWrapperRunTimeProperties> b_wrapped(
        b_buff, skel__BitsetWrapperRunTimeProperties(b));
    b_wrapped &= y;
    return b;
}

template <typename t_BitsetRunTimeProperties, typename t_bitset>
skel__Bitset operator|(const skel__BitsetHandle<t_BitsetRunTimeProperties>& x,
                       const t_bitset& y) {
    skel__Assert(
        sizeof(skel__BitsetHandle<t_BitsetRunTimeProperties>::block_type) ==
        sizeof(skel__Bitset::block_type));
    skel__Bitset b;
    x.to_hard_bitset(b);
    skel__BitsetCompileTimeProperties::block_type* b_buff(
        skel__BitsetUtils::get_bitset_buffer(b));
    skel__BitsetHandle<skel__BitsetWrapperRunTimeProperties> b_wrapped(
        b_buff, skel__BitsetWrapperRunTimeProperties(b));
    b_wrapped |= y;
    return b;
}

template <typename t_BitsetRunTimeProperties, typename t_bitset>
skel__Bitset operator^(const skel__BitsetHandle<t_BitsetRunTimeProperties>& x,
                       const t_bitset& y) {
    skel__Assert(
        sizeof(skel__BitsetHandle<t_BitsetRunTimeProperties>::block_type) ==
        sizeof(skel__Bitset::block_type));
    skel__Bitset b;
    x.to_hard_bitset(b);
    skel__BitsetCompileTimeProperties::block_type* b_buff(
        skel__BitsetUtils::get_bitset_buffer(b));
    skel__BitsetHandle<skel__BitsetWrapperRunTimeProperties> b_wrapped(
        b_buff, skel__BitsetWrapperRunTimeProperties(b));
    b_wrapped ^= y;
    return b;
}

template <typename t_BitsetRunTimeProperties, typename t_bitset>
skel__Bitset operator-(const skel__BitsetHandle<t_BitsetRunTimeProperties>& x,
                       const t_bitset& y) {
    skel__Assert(
        sizeof(skel__BitsetHandle<t_BitsetRunTimeProperties>::block_type) ==
        sizeof(skel__Bitset::block_type));
    skel__Bitset b;
    x.to_hard_bitset(b);
    skel__BitsetCompileTimeProperties::block_type* b_buff(
        skel__BitsetUtils::get_bitset_buffer(b));
    skel__BitsetHandle<skel__BitsetWrapperRunTimeProperties> b_wrapped(
        b_buff, skel__BitsetWrapperRunTimeProperties(b));
    b_wrapped -= y;
    return b;
}

//-----------------------------------------------------------------------------
// basic bit operations

template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::set(size_type pos,
                                                       bool val) {
    skel__Assert(pos < size());

    if (val) {
        m_bits[block_index(pos)] |= bit_mask(pos);
    } else {
        reset(pos);
    }

    return *this;
}

template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::set() {
    memset(&m_bits[0], ~block_type(0), num_bytes());
    m_zero_unused_bits();
    return *this;
}

template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::reset(size_type pos) {
    skel__Assert(pos < size());
    m_bits[block_index(pos)] &= ~bit_mask(pos);
    return *this;
}

template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::reset() {
    memset(&m_bits[0], block_type(0), num_bytes());
    return *this;
}

template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::flip(size_type pos) {
    skel__Assert(pos < size());
    m_bits[block_index(pos)] ^= bit_mask(pos);
    return *this;
}

template <typename t_BitsetRunTimeProperties>
skel__BitsetHandle<t_BitsetRunTimeProperties>&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::flip() {
    for (size_type i = 0; i < num_blocks(); ++i) {
        m_bits[i] = ~m_bits[i];
    }
    m_zero_unused_bits();
    return *this;
}

template <typename t_BitsetRunTimeProperties>
bool skel__BitsetHandle<t_BitsetRunTimeProperties>::m_unchecked_test(
    size_type pos) const {
    // @TODO - see intrinsic for bit testing: (MS specific)
    //  _bittest, _bittest65
    // https://msdn.microsoft.com/en-us/librarY/h65k4tze(v=vs.120).aspx
    return (m_bits[block_index(pos)] & bit_mask(pos)) != 0;
}

template <typename t_BitsetRunTimeProperties>
bool skel__BitsetHandle<t_BitsetRunTimeProperties>::test(size_type pos) const {
    skel__Assert(pos < size());
    return m_unchecked_test(pos);
}

template <typename t_BitsetRunTimeProperties>
bool skel__BitsetHandle<t_BitsetRunTimeProperties>::any() const {
    for (size_type i = 0; i < num_blocks(); ++i) {
        if (m_bits[i]) {
            return true;
        }
    }
    return false;
}

template <typename t_BitsetRunTimeProperties>
inline bool skel__BitsetHandle<t_BitsetRunTimeProperties>::none() const {
    return !any();
}

template <typename t_BitsetRunTimeProperties>
skel__Bitset skel__BitsetHandle<t_BitsetRunTimeProperties>::operator~() const {
    skel__Bitset b;
    return to_hard_bitset(b).flip();
}

#include "skel__BitsetHandleCountBits.h"

// --------------------------------
// lookup

// look for the first bit "on", starting
// from the block with index first_block
//
template <typename t_BitsetRunTimeProperties>
typename skel__BitsetHandle<t_BitsetRunTimeProperties>::size_type
    skel__BitsetHandle<t_BitsetRunTimeProperties>::m_do_find_from(
        size_type first_block) const {
    size_type i = first_block;

    // skip null blocks
    while ((i < num_blocks()) && (m_bits[i] == 0)) {
        ++i;
    }

    if (i >= num_blocks()) {
        return npos;  // not found
    }

    return i * bits_per_block + m_lowest_bit_position(m_bits[i]);
}

template <typename t_BitsetRunTimeProperties>
typename skel__BitsetHandle<t_BitsetRunTimeProperties>::index_type
    skel__BitsetHandle<t_BitsetRunTimeProperties>::m_lowest_bit_position(
        block_type b) const {
    skel__Assert(sizeof(block_type) == 8);
    index_type lowest_bit_position = 0;
#ifdef SKEL___BUILD_COMPILER_MSVC
    // Disable C4552 warning:
    // "!= operator has no effect; expected operator with side-effect".
    // skel__Verify is meant to be an assert when enabled, and to still execute
    // code when asserts are disabled.
#pragma warning(push)
#pragma warning(disable : 4552)
#endif  // SKEL___BUILD_COMPILER_MSVC
    skel__Verify(0 != skel__BitsetUtils::find_lowest_bit_position(
                          &lowest_bit_position, b));
#ifdef SKEL___BUILD_COMPILER_MSVC
#pragma warning(pop)
#endif  // SKEL___BUILD_COMPILER_MSVC
    return lowest_bit_position;
}

template <typename t_BitsetRunTimeProperties>
typename skel__BitsetHandle<t_BitsetRunTimeProperties>::size_type
    skel__BitsetHandle<t_BitsetRunTimeProperties>::find_first() const {
    return m_do_find_from(0);
}

template <typename t_BitsetRunTimeProperties>
typename skel__BitsetHandle<t_BitsetRunTimeProperties>::size_type
    skel__BitsetHandle<t_BitsetRunTimeProperties>::find_next(
        size_type pos) const {
    const size_type sz = size();
    if (pos >= (sz - 1) || sz == 0) {
        return npos;
    }

    ++pos;

    const size_type blk = block_index(pos);
    const block_width_type ind = bit_index(pos);

    // shift bits up to one immediately after current
    const block_type fore = m_bits[blk] >> ind;

    return fore ? pos + static_cast<size_type>(m_lowest_bit_position(fore))
                : m_do_find_from(blk + 1);
}

// Converts this handle into a stand-alone PkBitSet
template <typename t_BitsetRunTimeProperties>
skel__Bitset& skel__BitsetHandle<t_BitsetRunTimeProperties>::to_hard_bitset(
    skel__Bitset& out_bitset) const {
    skel__Assert(sizeof(block_type) == sizeof(skel__Bitset::block_type));
    out_bitset.resize(size(), false);
    block_type* out_buff(skel__BitsetUtils::get_bitset_buffer(out_bitset));
    memcpy(out_buff,
           &(m_bits[0]),
           skel__BitsetUtils::get_bitset_min_num_bytes(out_bitset, *this));
    return out_bitset;
}

//-----------------------------------------------------------------------------
// comparison

template <typename t_BitsetRunTimeProperties, typename t_bitset>
inline bool operator==(const skel__BitsetHandle<t_BitsetRunTimeProperties>& a,
                       const t_bitset& b) {
    const typename skel__BitsetHandle<t_BitsetRunTimeProperties>::block_type*
        b_buff(skel__BitsetUtils::get_const_bitset_buffer(b));
    return (a.size() == b.size()) &&
           (0 == memcmp(&(a.get_buffer()[0]),
                        b_buff,
                        skel__BitsetUtils::get_bitset_min_num_bytes(a, b)));
}

template <typename t_BitsetRunTimeProperties, typename t_bitset>
inline bool operator!=(const skel__BitsetHandle<t_BitsetRunTimeProperties>& a,
                       const t_bitset& b) {
    return !(a == b);
}

// Based on boost::dynamic_bitset
// gives a reference to the highest block
//
template <typename t_BitsetRunTimeProperties>
inline typename skel__BitsetHandle<t_BitsetRunTimeProperties>::block_type&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::m_highest_block() {
    return const_cast<block_type&>(
        static_cast<const skel__BitsetHandle*>(this)->m_highest_block());
}

// Based on boost::dynamic_bitset
// gives a const-reference to the highest block
//
template <typename t_BitsetRunTimeProperties>
inline const typename skel__BitsetHandle<t_BitsetRunTimeProperties>::block_type&
    skel__BitsetHandle<t_BitsetRunTimeProperties>::m_highest_block() const {
    skel__Assert(size() > 0 && num_blocks() > 0);
    return m_bits[num_blocks() - 1];
}

// Based on boost::dynamic_bitset
// If size() is not a multiple of bits_per_block
// then not all the bits in the last block are used.
// This function resets the unused bits (convenient
// for the implementation of many member functions)
//
template <typename t_BitsetRunTimeProperties>
inline void
    skel__BitsetHandle<t_BitsetRunTimeProperties>::m_zero_unused_bits() {
    skel__Assert(num_blocks() == calc_num_blocks(size()));

    // if != 0 this is the number of bits used in the last block
    const block_width_type extra_bits = count_extra_bits();

    if (extra_bits != 0) {
        m_highest_block() &= ~(~static_cast<block_type>(0) << extra_bits);
    }
}

// Utility macros

/**
 * Define a stand-alone bit set type that leverages compiler intrinsics for
 * faster bit iteration
 */
typedef skel__BitsetHandle<skel__BitsetWrapperRunTimeProperties>
    skel__WrappedBitset;

/**
 * Macro to obtain a wrapped bit set handle from a skel__Bitset
 */
#define SKEL___WRAP_BITSET(b)                                      \
    skel__WrappedBitset(skel__BitsetUtils::get_bitset_buffer((b)), \
                        skel__BitsetWrapperRunTimeProperties((b)))

#endif  // skel__BitsetHandle_h
