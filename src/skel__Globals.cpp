//****************************************************************************
// skel__Globals.cpp
//****************************************************************************

/**
 * Houses global data that is configurable at run time
 */

#include "skel__Build.h"
#include "skel__Assert.h"
#include "skel__Config.h"
#include "skel__Rand.h"
#include "skel__Stats.h"
#include "skel__Thread.h"

/**
 * Namespace to house global data
 */
namespace skel__G {
/**
 * If threads enabled, this value will hold the number of worker threads
 * available (the main thread is not included in this count). To access:
 * skel__G::NUM_WORKER_THREADS_VAR_NAME (will cause build errors if threads not
 * enabled).
 */
SKEL___DECLARE_NUM_WORKER_THREADS

/**
 * If threads enabled, this declares an extern for a parallel mapper. To
 * access: skel__G:PARALLEL_MAPPER_VAR_NAME (will cause build errors if threads
 * not enabled).
 */
SKEL___DECLARE_PARALLEL_MAPPER

/**
 * Random number generator (each thread has its own instance)
 */
SKEL___DECLARE_TLS_DATA(skel__Rand, rng);

/**
 * Initialize global data from runtime configuration
 */
void init(skel__SpConstConfig_t cfg) {
    SKEL___SCOPED_STAT_TIMER(skel__STAT_GlobalInitTime);
    SKEL___INC_STAT_COUNTER(skel__STAT_GlobalInitCount);
#ifdef SKEL___BUILD_ENABLE_THREADS
    // Initialize thread count
    SKEL___NUM_WORKER_THREADS_VAR_NAME = SKEL___DEFAULT_NUM_THREADS;
    cfg->read_into(SKEL___NUM_WORKER_THREADS_VAR_NAME,
                   skel__Opt_num_worker_threads);
    skel__Assert(SKEL___NUM_WORKER_THREADS_VAR_NAME > 0);
    SKEL___NUM_WORKER_THREADS_VAR_NAME =
        (SKEL___NUM_WORKER_THREADS_VAR_NAME < 1)
            ? 1
            : SKEL___NUM_WORKER_THREADS_VAR_NAME;

    // Initialize parallel mapper
    SKEL___PARALLEL_MAPPER_VAR_NAME.init(SKEL___NUM_WORKER_THREADS_VAR_NAME);
#endif  // SKEL___BUILD_ENABLE_THREADS

    // Initialize random number generator (rng)
    SKEL___INIT_TLS_DATA_0_PARAM(rng);
    // @HACK - Force Intel compilers to seed RNG
    SKEL___TLS_DATA_FOR_EACH_0_PARAM(rng, reseed);
}

/**
 * Initializes global data to default values
 */
void default_init() {
    SKEL___SCOPED_STAT_TIMER(skel__STAT_GlobalInitTime);
    SKEL___INC_STAT_COUNTER(skel__STAT_GlobalInitCount);
#ifdef SKEL___BUILD_ENABLE_THREADS
    // Initialize thread count
    SKEL___NUM_WORKER_THREADS_VAR_NAME = SKEL___DEFAULT_NUM_THREADS;

    // Initialize parallel mapper
    SKEL___PARALLEL_MAPPER_VAR_NAME.init(SKEL___NUM_WORKER_THREADS_VAR_NAME);
#endif  // SKEL___BUILD_ENABLE_THREADS

    // Initialize random number generator (rng)
    SKEL___INIT_TLS_DATA_0_PARAM(rng);
    // @HACK - Force Intel compilers to seed RNG
    SKEL___TLS_DATA_FOR_EACH_0_PARAM(rng, reseed);
}

/**
 * If global data is initialized, teardown() must be called before main()
 * exits!
 * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
 * This seems to be a deadlock bug caused by Microsoft's implementation of
 * std::thread
 */
void teardown() {
#ifdef SKEL___BUILD_ENABLE_THREADS
    SKEL___PARALLEL_MAPPER_VAR_NAME.teardown();
#endif  // SKEL___BUILD_ENABLE_THREADS
}

}  // namespace skel__G
