//****************************************************************************
// skel__TestRng.h
//****************************************************************************

/**
 * Commandlet for testing random number generation
 *
 * Usage:
 * -test_rng
 */

#ifdef skel__TestRng_h
#   error "Test Rng included multiple times!"
#endif  // skel__TestRng_h
#define skel__TestRng_h

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_TESTS

#include "skel__CmdOptsMap.h"
#include "skel__ExitCodes.h"
#include "skel__Globals.h"
#include "skel__Thread.h"
#include "skel__Types.h"

#include <iostream>

namespace {

namespace skel__TestRng {

    /**
     * Utility to report the generated number and the interval it should fall within
     */
    template <typename t_>
    void report_interval(t_ rn, t_ lower_, t_ upper_) {
        std::cout << "Generated " << rn << " in [" << lower_ << ", " << upper_
                  << ")\n";
    }

    /**
     * For interval in [0,1)
     */
    void generate_and_report_interval(SKEL___THREAD_ID_0_PARAM) {
        report_interval(
            skel__Rng.unif_real(), SKEL___TO_REAL(0.0), SKEL___TO_REAL(1.0));
    }

    /**
     * For interval in [lower_, upper_] in real numbers
     */
    void generate_and_report_interval(skel__Real lower_,
                                      skel__Real upper_ SKEL___THREAD_ID_PARAM) {
        report_interval(skel__Rng.unif_real(lower_, upper_), lower_, upper_);
    }

    /**
     * For interval in [lower_, upper_] in integer numbers
     */
    void generate_and_report_interval(int lower_,
                                      int upper_ SKEL___THREAD_ID_PARAM) {
        report_interval(skel__Rng.unif_int(lower_, upper_), lower_, upper_);
    }

}  // skel__TestRng namespace
}  // Anonymous namespace

/**
 * Entry point for test
 */
int skel__TestRngMain(const skel__CmdOptsMap& cmd_opts) {
    skel__G::default_init();
    using namespace skel__TestRng;
    generate_and_report_interval(SKEL___MAIN_THREAD_ID_0_ARG);
    generate_and_report_interval(SKEL___TO_REAL(-1.0),
                                 SKEL___TO_REAL(1.0) SKEL___MAIN_THREAD_ID_ARG);
    generate_and_report_interval(
        SKEL___TO_REAL(-5.0), SKEL___TO_REAL(-1.0) SKEL___MAIN_THREAD_ID_ARG);
    generate_and_report_interval(-100, 100 SKEL___MAIN_THREAD_ID_ARG);
    generate_and_report_interval(-100, 0 SKEL___MAIN_THREAD_ID_ARG);
    generate_and_report_interval(0, 100 SKEL___MAIN_THREAD_ID_ARG);
    skel__G::teardown();
    return skel__ExitCode_normal;
}

#endif  // SKEL___BUILD_ENABLE_TESTS
