//****************************************************************************
// skel__TestDirichletDistribution.h
//****************************************************************************

/**
 * Commandlet for testing Dirichlet distribution methods
 *
 * Usage:
 * -test_dir_distrib
 */

#ifdef skel__TestDirichletDistribution_h
#   error "Dirichlet Distribution Test included multiple times!"
#endif  // skel__TestDirichletDistribution_h
#define skel__TestDirichletDistribution_h

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_TESTS

#include "skel__Assert.h"
#include "skel__CmdOptsMap.h"
#include "skel__DirichletDistribution.h"
#include "skel__ExitCodes.h"
#include "skel__Logf.h"
#include "skel__Rand.h"
#include "skel__Types.h"

#include <vector>

/**
 * Entry point for test
 */
int skel__TestDirichletDistributionMain(const skel__CmdOptsMap& cmd_opts) {
    skel__Logf("Running Test Dirichlet Distribution.\n");

    // Alpha range
    const skel__Real alpha_min = SKEL___TO_REAL(0.5);
    const skel__Real alpha_max = SKEL___TO_REAL(10.0);
    skel__Assert(alpha_min <= alpha_max);
    skel__Assert(alpha_min > SKEL___TO_REAL(0.0));

    // Pseudo-count size
    const skel__UInt K = 10;
    skel__Assert(K > 1);

    // Random number generator
    skel__Rand rng;

    // Create pseudo-counts vector
    std::vector<skel__Real> ALPHA(K);
    for (size_t i = 0; i < K; ++i) {
        ALPHA[i] = rng.unif_real(alpha_min, alpha_max);
        skel__AssertBoundsInc(ALPHA[i], alpha_min, alpha_max);
        skel__Logf("ALPHA[%d]: %f\n", ((int)i), ((double)ALPHA[i]));
    }

    // Test generated samples form a pmf
    std::vector<skel__Real> THETA(K, SKEL___TO_REAL(0.0));
    skel__DirichletDistribution::sample(rng, K, &(ALPHA[0]), &(THETA[0]));
    skel__Real sum = SKEL___TO_REAL(0.0);
    for (size_t i = 0; i < K; ++i) {
        const skel__Real theta = THETA[i];
        skel__AssertBoundsInc(theta, SKEL___TO_REAL(0.0), SKEL___TO_REAL(1.0));
        skel__Logf("THETA[%d]: %f\n", ((int)i), ((double)theta));
        sum += theta;
    }
    skel__AssertRealEq(sum, SKEL___TO_REAL(1.0));
    skel__Logf("SUM(THETA) = %f\n", ((double)sum));

    // Test density calls
    const skel__Real p =
        skel__DirichletDistribution::pdf(K, &(ALPHA[0]), &(THETA[0]));
    const skel__Real lnp =
        skel__DirichletDistribution::lnpdf(K, &(ALPHA[0]), &(THETA[0]));
    skel__Assert(p == exp(lnp));
    skel__Logf("Probability density of sample: %f\n", ((double)p));
    skel__Logf("Log(probability density) of sample: %f\n", ((double)lnp));

    return skel__ExitCode_normal;
}

#endif  // SKEL___BUILD_ENABLE_TESTS
