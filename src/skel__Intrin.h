//****************************************************************************
// skel__Intrin.h
//****************************************************************************

/**
 * Platform independent include for compiler intrinsics
 */

#ifndef skel__Intrin_h
#define skel__Intrin_h

#include "skel__Build.h"

// http://stackoverflow.com/questions/11228855/header-files-for-x86-simd-intrinsics
#ifdef SKEL___BUILD_COMPILER_MSVC
/* Microsoft C/C++-compatible compiler */
#   include <intrin.h>
#elif defined(SKEL___BUILD_COMPILER_ICC)
/* Intel compiler */
#   include <immintrin.h>
#elif defined(SKEL___BUILD_COMPILER_GCC)
#   if (defined(__x86_64__) || defined(__i386__))
/* GCC-compatible compiler, targeting x86/x86-64 */
#       include <x86intrin.h>
#   endif 
#else
#   error Unrecognized compiler
#endif

#endif  // skel__Intrin_h
