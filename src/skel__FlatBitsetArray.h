//****************************************************************************
// skel__FlatBitsetArray.h
//****************************************************************************

/**
 * @brief Container class for storing bit sets of a fixed size where the bit
 * set size is determined at run time. This does not use a pool allocator.
 */

#ifndef skel__FlatBitsetArray_h
#define skel__FlatBitsetArray_h

#include "skel__Build.h"
#include "skel__Assert.h"
#include "skel__BitsetArrayRunTimeProperties.h"
#include "skel__BitsetCompileTimeProperties.h"
#include "skel__BitsetHandle.h"

#include <vector>

/**
 * A flattened memory bit set array
 */
class skel__FlatBitsetArray : public skel__BitsetCompileTimeProperties {
private:
    /**
     * Run time properties policy for use in skel__BitsetHandle
     */
    typedef skel__BitsetArrayRunTimeProperties<skel__FlatBitsetArray>
        bitset_run_time_properties_t;

public:
    // Typedefs

    /**
     * A handle to non-const bit set
     */
    typedef skel__BitsetHandle<bitset_run_time_properties_t> bitset_handle_t;

    /**
     * A handle to a const bit set
     */
    typedef const skel__BitsetHandle<bitset_run_time_properties_t>
        bitset_const_handle_t;

    /**
     * Default constructor
     */
    skel__FlatBitsetArray() : m_num_bits(0), m_num_blocks(0), m_size(0) {}

    /**
     * Constructs array to store bit sets with parameter
     * number of bits
     */
    explicit skel__FlatBitsetArray(const size_type num_bits_)
        : m_num_bits(num_bits_),
          m_num_blocks(this->calc_num_blocks(num_bits_)),
          m_size(0) {}

    /**
     * Constructor
     * @param num_bits - the size of each bit field within the array
     * @param num_elements - number of addressable bit sets
     */
    skel__FlatBitsetArray(const size_type num_bits_,
                          const size_type num_elements_ = 0)
        : m_num_bits(num_bits_),
          m_num_blocks(this->calc_num_blocks(num_bits_)),
          m_size(num_elements_),
          m_block_buffer(num_elements_ * this->calc_num_blocks(num_bits_), 0) {
        skel__Assert(num_bits() > 0);
    }

    /**
     * @return The number of bits in each bit set
     */
    inline size_type num_bits() const { return m_num_bits; }

    /**
     * Sets the bit length for all bit sets in this array.
     * Clears underlying memory store (even if num_bits is same).
     */
    inline void set_num_bits(const size_type num_bits_) {
        clear();
        m_num_bits = num_bits_;
        m_num_blocks = this->calc_num_blocks(num_bits_);
    }

    /**
     * @return The number of blocks necessary to represent a bit set
     */
    inline size_type num_blocks() const { return m_num_blocks; }

    /**
     * @return The number of blocks necessary to represent all bit sets in this
     * array
     */
    inline size_type num_total_blocks() const { return size() * num_blocks(); }

    /**
     * @return The number of bytes necessary to represent a bit set
     */
    inline size_type num_bytes() const {
        return num_blocks() * sizeof(block_type);
    }

    /**
     * @return The number of bytes necessary to represent all bit sets in this
     * array
     */
    inline size_type num_total_bytes() const { return size() * num_bytes(); }

    /**
     * @return The number of bit sets in this array
     */
    inline size_type size() const { return m_size; }

    /**
     * @return TRUE if there are no elements in this array, FALSE otherwise
     */
    inline bool empty() const { return 0 == size(); }

    /**
     * @return A bit set interface to front bit buffer
     */
    inline bitset_handle_t front() {
        return bitset_handle_t(get_front_bit_buffer(),
                               bitset_run_time_properties_t(*this));
    }

    /**
     * @return A const bit set interface to front bit buffer
     */
    inline bitset_const_handle_t front() const {
        return bitset_const_handle_t(get_front_bit_buffer(),
                                     bitset_run_time_properties_t(*this));
    }

    /**
     * @return a bit set interface to back bit buffer
     */
    inline bitset_handle_t back() {
        return bitset_handle_t(get_back_bit_buffer(),
                               bitset_run_time_properties_t(*this));
    }

    /**
     * @return a const bit set interface to back bit buffer
     */
    inline bitset_const_handle_t back() const {
        return bitset_const_handle_t(get_back_bit_buffer(),
                                     bitset_run_time_properties_t(*this));
    }

    /**
     * @return a bit set interface to bit buffer at parameter idx
     */
    inline bitset_handle_t operator[](const size_type idx) {
        return bitset_handle_t(get_bit_buffer(idx),
                               bitset_run_time_properties_t(*this));
    }

    /**
     * @return a bit set interface to bit buffer at parameter idx
     */
    inline bitset_const_handle_t operator[](const size_type idx) const {
        return bitset_const_handle_t(get_bit_buffer(idx),
                                     bitset_run_time_properties_t(*this));
    }

    /**
     * Appends parameter bit set to array
     */
    template <typename t_bitset>
    void push_back(const t_bitset& bit_set);

    /**
     * Appends bit set collection to pooled array
     */
    template <typename t_bitset_array>
    void push_back_array(const t_bitset_array& bit_set_array);

    /**
     * Appends a zeroed bit buffer
     * Updates size
     */
    inline void push_back_zeroed() {
        assert(m_block_buffer.size() == this->num_total_blocks());
        m_block_buffer.resize(m_block_buffer.size() + this->num_blocks(), 0);
        ++m_size;
        assert(m_block_buffer.size() == this->num_total_blocks());
    }

    /**
     * Removes last element
     */
    inline void pop_back() {
        assert(m_block_buffer.size() == this->num_total_blocks());
        m_block_buffer.resize(m_block_buffer.size() - this->num_blocks());
        --m_size;
        assert(m_block_buffer.size() == this->num_total_blocks());
    }

    /**
     * Clears array similar to std:vector::clear
     * Does not reset bit state
     */
    inline void clear() {
        m_block_buffer.clear();
        m_size = 0;
    }

    /**
     * Pre-allocates memory
     */
    inline void reserve(const size_type n) {
        m_block_buffer.reserve(n * this->num_blocks());
    }

    /**
     * Resizes array similar to std::vector::resize()
     */
    inline void resize(const size_type n) {
        assert(m_block_buffer.size() == this->num_total_blocks());
        m_block_buffer.resize(n * this->num_blocks());
        m_size = n;
        assert(m_block_buffer.size() == this->num_total_blocks());
    }

private:
    //************ Utilities for assertion checking

    /**
     * @return TRUE if index is within bounds [0, max_size), FALSE otherwise
     */
    inline static bool bounds_check(const size_type idx,
                                    const size_type max_size) {
        return (0 <= idx) && (max_size > idx);
    }

    //************ Internal bit set buffer utilities

    /**
     * @return The bit buffer at parameter idx
     */
    inline block_type* get_bit_buffer(const size_type idx) {
        return const_cast<block_type*>(
            static_cast<const skel__FlatBitsetArray*>(this)->get_bit_buffer(
                idx));
    }

    /**
     * @return The const bit buffer at parameter idx
     */
    inline const block_type* get_bit_buffer(const size_type idx) const {
        // Assert index is valid based on object size
        skel__Assert(bounds_check(idx, size()));
        // Convert index to byte offset
        const size_t block_offset = idx * this->num_blocks();
        // Assert index is valid within underlying byte buffer
        skel__Assert(bounds_check((block_offset + (this->num_blocks() - 1)),
                                  m_block_buffer.size()));
        // Cast bytes to blocks
        return static_cast<const block_type*>(&(m_block_buffer[block_offset]));
    }

    /**
     * @return Non-constant pointer to first bit buffer allocated
     */
    inline block_type* get_front_bit_buffer() {
        return const_cast<block_type*>(
            static_cast<const skel__FlatBitsetArray*>(this)
                ->get_front_bit_buffer());
    }

    /**
     * @return Constant pointer to first bit buffer allocated
     */
    inline const block_type* get_front_bit_buffer() const {
        skel__Assert(!empty());
        skel__Assert(bounds_check(0, size()));
        skel__Assert(num_blocks() == m_block_buffer.size());
        return static_cast<const block_type*>(&(m_block_buffer[0]));
    }

    /**
     * @return Non-constant pointer to last bit buffer allocated
     */
    inline block_type* get_back_bit_buffer() {
        return const_cast<block_type*>(
            static_cast<const skel__FlatBitsetArray*>(this)
                ->get_back_bit_buffer());
    }

    /**
     * @ return Constant pointer to last bit buffer allocated
     */
    inline const block_type* get_back_bit_buffer() const {
        skel__Assert(!empty());
        return get_bit_buffer(this->size() - 1);
    }

    /**
     * The number of bits within each bit set
     */
    size_type m_num_bits;

    /**
     * The number of blocks necessary to represent a bit set
     */
    size_type m_num_blocks;

    /**
     * The number of bit buffers in this collection
     */
    size_type m_size;

    /**
     * The underlying byte memory for all bit sets in this array
     */
    std::vector<block_type> m_block_buffer;
};

/**
 * Typedef for a non const bit set handle
 */
typedef skel__FlatBitsetArray::bitset_handle_t skel__FlatBitsetHandle;

/**
 * Typedef for a const bit set handle
 */
typedef skel__FlatBitsetArray::bitset_const_handle_t
    skel__FlatBitsetConstHandle;

/**
 * Appends parameter bit set to array
 */
template <typename t_bitset>
void skel__FlatBitsetArray::push_back(const t_bitset& bitset) {
    skel__Assert(bitset.size() == num_bits());
    this->push_back_zeroed();
    memcpy(
        // note: push_back_zeroed() automatically updates size
        this->get_back_bit_buffer(),
        &(skel__BitsetUtils::get_const_bitset_buffer(bitset)[0]),
        std::min<>(this->num_bytes(),
                   skel__BitsetUtils::get_bitset_num_bytes(bitset)));
}

/**
 * Appends bit set collection to pooled array
 */
template <typename t_bitset_array>
void skel__FlatBitsetArray::push_back_array(
    const t_bitset_array& bitset_array) {
    this->reserve(size() + bitset_array.size());
    for (size_type i = 0; i < bitset_array.size(); ++i) {
        this->push_back(bitset_array[i]);
    }
}

/**
 * Specialize appending from a flat array
 * Assumes arrays have same configuration!
 * (same number of bits, bytes, blocks per element)
 */
template <>
void skel__FlatBitsetArray::push_back_array(
    const skel__FlatBitsetArray& bitset_array) {
    skel__Assert(bitset_array.size() > 0);
    skel__Assert(bitset_array.num_bits() == this->num_bits());
    skel__Assert(bitset_array.num_bytes() == this->num_bytes());
    skel__Assert(bitset_array.num_blocks() == this->num_blocks());
    const size_t old_size = this->size();
    this->resize(old_size + bitset_array.size());
    memcpy(this->get_bit_buffer(old_size),
           bitset_array.get_front_bit_buffer(),
           bitset_array.num_total_bytes());
}

#endif  // skel__FlatBitsetArray_h
