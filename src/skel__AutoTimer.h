//****************************************************************************
// skel__AutoTimer.h
//****************************************************************************

/**
 * @brief Portable utility meant to mimic boost::timer::auto_cpu_timer
 */

#ifndef skel__AutoTimer_h
#define skel__AutoTimer_h

#include "skel__Build.h"

#ifdef SKEL___BUILD_ENABLE_INTERVAL_LOGGING

#include "skel__Logf.h"
#include "skel__Time.h"

/**
 * Simple timer which reports elapsed duration between creation and destruction
 */
class skel__AutoTimer {
public:
    /**
     * Default constructor, initialize creation time
     */
    skel__AutoTimer()
        : m_start_secs_wall(skel___time_secs_wall()),
          m_start_secs_cpu(skel___time_secs_cpu()) {}

    /**
     * Destructor reports lifetime of timer in seconds
     */
    ~skel__AutoTimer() {
        skel__Logf("%fs wall, %fs CPU\n",
                   skel___time_secs_wall() - m_start_secs_wall,
                   skel___time_secs_cpu() - m_start_secs_cpu);
    }

private:
    /**
     * Creation times
     */
    double m_start_secs_wall;
    double m_start_secs_cpu;
};

/**
 * Macro for creating an auto-timer
 */
#define SKEL___DECLARE_AUTO_TIMER(name_) skel__AutoTimer auto_timer_##name_

#else

/**
 * Disable macro for creating an auto-timer
 */
#define SKEL___DECLARE_AUTO_TIMER(name_)

#endif  // SKEL___BUILD_ENABLE_INTERVAL_LOGGING

#endif  // skel__AutoTimer_h
