//****************************************************************************
// skel__IsFinite.h
//****************************************************************************

/**
 * @brief Relatively portable isfinite() implementation
 */

/**
 * Based on discussion:
 * https://stackoverflow.com/questions/23092615/a-portable-check-for-infinity-and-not-a-number
 */

#ifndef skel__IsFinite_h
#define skel__IsFinite_h

#include "skel__Build.h"

#ifdef SKEL___BUILD_CXX_11
#   include <cmath>
#else
#   include <limits>
#endif  // SKEL___BUILD_CXX_11

/**
 * Default implementation always returns true
 */
template <typename t_real>
inline bool skel__IsFinite(const t_real r) {
    return true;
}

/**
 * Specialization for floats
 * @return true if 'r' is finite (not nan and not inf)
 */
template <>
inline bool skel__IsFinite(const float r) {
#ifdef SKEL___BUILD_CXX_11
    return std::isfinite(r);
#else
    typedef float real_t__;
    const real_t__ r_max = std::numeric_limits<real_t__>::max();
    const volatile real_t__ r_ = r;
    return (r_ == r_) && (r >= -r_max) && (r <= r_max);
#endif  // SKEL___BUILD_CXX_11
}

/**
 * Specialization for doubles
 * @return true if x is finite (not nan and not inf)
 */
template <>
inline bool skel__IsFinite(const double r) {
#ifdef SKEL___BUILD_CXX_11
    return std::isfinite(r);
#else
    typedef double real_t__;
    const real_t__ r_max = std::numeric_limits<real_t__>::max();
    const volatile real_t__ r_ = r;
    return (r_ == r_) && (r >= -r_max) && (r <= r_max);
#endif  // SKEL___BUILD_CXX_11
}

#endif  // skel__IsFinite_h
