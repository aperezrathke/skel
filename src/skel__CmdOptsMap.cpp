//****************************************************************************
// skel__CmdOpts.cpp
//
// Implementation for command line parsing utilities
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "skel__Build.h"
#include "skel__CmdOptsMap.h"
#include "skel__Assert.h"

//****************************************************************************
// Anonymous namespace utilities
//****************************************************************************

namespace {
// Binary switches start with a double dash (--) and are followed by an
// additional argument specifying the switch <key, value> mapping
// @param opt_key - command line switch to test
// @return TRUE if parameter string is a binary switch, FALSE otherwise
inline static skel__Bool is_binary_switch(const std::string& opt_key) {
    return (opt_key.length() > 2) && (opt_key[0] == '-') &&
           (opt_key[1] == '-') && (opt_key[2] != '-');
}

// Unary switches start with a single dash (-) and any arguments
// after it are ignored (the switch key is mapped to an empty string)
// @param opt_key - command line switch to test
// @return TRUE if parameter string is a unary switch, FALSE otherwise
inline static skel__Bool is_unary_switch(const std::string& opt_key) {
    return (opt_key.length() > 1) && (opt_key[0] == '-') && (opt_key[1] != '-');
}

/**
 * Removes leading dashes from command line key
 * @param out_key - output key with no leading dashes
 * @param dash_prefix - the encountered dash prefix
 * @param cmd_key - input command line key
 */
void trim_dash_prefix(std::string& out_key,
                      std::string& dash_prefix,
                      const std::string& cmd_key) {
    out_key = cmd_key;
    dash_prefix.clear();
    if (is_binary_switch(cmd_key)) {
        skel__Assert(cmd_key.size() >= 2);
        out_key.erase(0 /*pos*/, 2 /*len*/);
        dash_prefix = "--";
    } else if (is_unary_switch(cmd_key)) {
        skel__Assert(cmd_key.size() >= 1);
        out_key.erase(0 /*pos*/, 1 /*len*/);
        dash_prefix = "-";
    }
}

/**
 * Utility to (re)append dash prefix to a key missing it
 * @param cmd_key - output command line key with dash prefix
 * @param dash_prefix - the dash prefix to combine
 * @param key_sans_prefix - key resulting from 'trim_dash_prefix(...)'
 */
void combine_dash_prefix(std::string& cmd_key,
                         const std::string& dash_prefix,
                         const std::string& key_sans_prefix) {
    cmd_key = dash_prefix + key_sans_prefix;
}

}  // namespace

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Constructor initializes from command arguments
 *
 * @param argc - the number of command line arguments
 * @param argv - character string array of command line arguments
 */
skel__CmdOptsMap::skel__CmdOptsMap(const int argc, const char** argv) {
    this->parse(argc, argv);
}

/**
 * Parses command line
 *
 * Must be called before using accessor methods such as get_*_value
 *
 * @param argc - the number of command line arguments
 * @param argv - character string array of command line arguments
 * @return TRUE if no parse errors, FALSE otherwise
 */
skel__Bool skel__CmdOptsMap::parse(const int argc, const char** argv) {
    // 0th argument is assumed to be exe name so skip it

    int i = 1;
    while (i < argc) {
        std::string opt_key(argv[i]);
        std::string opt_val("");

        if (is_binary_switch(opt_key)) {
            if ((i + 1) < argc) {
                opt_val = std::string(argv[i + 1]);
                ++i;
            } else {
                printf(
                    "Error: command line is malformed. Binary switch %s is "
                    "missing associated value.\n",
                    opt_key.c_str());
                // Binary <key, value> pair is missing value. We should return
                // FALSE to indicate a malformed command line.
                return skel__FALSE;
            }
        }
        // Check for proper formatting (if not binary switch, then must be unary
        // switch)
        else if (!is_unary_switch(opt_key)) {
            printf(
                "Error: command line is malformed. Unrecognized format for "
                "argument %s\n",
                opt_key.c_str());
            return skel__FALSE;
        }

        this->cmd_opts[opt_key] = opt_val;
        ++i;
    }

    return skel__TRUE;
}

/**
 * Extract only command line options destined for child configurations
 * @param out - output command line options (@WARNING - will be cleared!)
 */
void skel__CmdOptsMap::get_child_cmd(skel__CmdOptsMap& out) const {
    // Early out if &out == this
    skel__Assert(&out != this);
    if (&out == this) {
        return;
    }

    // Clear output commands
    out = skel__CmdOptsMap();

    // Prefix string denoting child keys
    const std::string child_prefix(SKEL___CONFIG_CHILD_CMDARG_PREFIX);
    if (child_prefix.empty()) {
        // Early out if no prefix specified
        out = *this;
        return;
    }

    // Check for existence of child keys
    std::vector<std::string> keys;
    std::string key;
    std::string key_sans_dash;
    std::string dash_prefix;
    std::string val;
    this->get_keys(keys);
    for (std::vector<std::string>::iterator it = keys.begin(); it != keys.end();
         it++) {
        key = *it;
        // Remove command line unary/binary switch prefixes
        trim_dash_prefix(key_sans_dash, dash_prefix, key);
        // Determine if key is targeted to a child configuration
        if (skel__StringUtils::starts_with(key_sans_dash, child_prefix)) {
            skel__Assert(!key_sans_dash.empty());
            skel__Verify(this->read_into(val, key));
            // Strip outermost child prefix
            key_sans_dash.erase(0 /*pos*/, 1 /*len*/);
            if (!key_sans_dash.empty()) {
                combine_dash_prefix(key, dash_prefix, key_sans_dash);
                out.set_option(key, val);
            } else {
                printf(
                    "Warning: Child command argument prefix %s encountered "
                    "with empty key (ignoring).\n",
                    child_prefix.c_str());
            }
        }
    }
}
