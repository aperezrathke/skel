//****************************************************************************
// skel__Assert.h
//****************************************************************************

/**
 * @brief Assertion hooks
 *
 * skel__Assert(expr_) - If assertions are enabled, expr_ is evaluated and
 *  execution is halted if expr_ == false. If assertions are disabled, then
 *  this code is a noop and has no performance penalty.
 *
 * skel__Verify(expr_) - If assertions are enabled, then will behave exactly as
 *  skel__Assert. If assertions are disabled, then will still evaluate expr_ but
 *  program will no longer halt if expr_ evaluates to false.
 */

#ifndef skel__Assert_h
#define skel__Assert_h

#include "skel__Build.h"
#include "skel__Types.h"

/**
 * Make sure that NDEBUG is defined if asserts are not enabled. C++ standard
 * specifies to define NDEBUG if assertions are to be turned off.
 */
#ifndef SKEL___BUILD_ENABLE_ASSERT
#   ifndef NDEBUG
#       define NDEBUG
#   endif // NDEBUG
#endif // SKEL___BUILD_ENABLE_ASSERT

/**
 * Add compiler messages on MSVC and GCC compilers to let us know that
 * assertions are enabled.
 */
#if !defined(NDEBUG) || defined(SKEL___BUILD_ENABLE_ASSERT)
#   ifdef SKEL___BUILD_COMPILER_MSVC
#       pragma message("Assertions enabled.")
#   elif (defined(SKEL___BUILD_COMPILER_GCC) || defined(SKEL___BUILD_COMPILER_ICC))
#       pragma message "Assertions enabled."
#   endif // compiler check
#endif // !defined(NDEBUG) || defined(SKEL___BUILD_ENABLE_ASSERT)

#include <assert.h>

//****************************************************************************
// Override
//****************************************************************************

#ifdef SKEL___BUILD_COMPILER_MSVC
#   ifdef _WIN64
#       include <intrin.h>
#   endif // _WIN64
#endif // SKEL___BUILD_COMPILER_MSVC

/**
 * This part is ugly, but, at the end of the day, to toggle assertions, just
 * define SKEL___BUILD_ENABLE_ASSERT within skel__Build.h or through compiler
 * command line and this part will override (if need be) to give the desired
 * behavior.
 *
 * We want to route to the std assert implementation in the following
 * conditions:
 * - Asserts are disabled via undefined SKEL___BUILD_ENABLE_ASSERT directive
 * - or standard NDEBUG preprocessor directive is not defined and asserts are
 *   enabled
 */
#if (!defined(SKEL___BUILD_ENABLE_ASSERT) || (defined(SKEL___BUILD_ENABLE_ASSERT) && !defined(NDEBUG)))
    // Defer to std assert implementation
#   define skel__Assert assert
#else
    /** 
     * Else, in CMake build system, it's hard to modify the NDEBUG flag which
     * is always defined in release builds. So if we really want asserts in
     * release mode, we need to 'override' by routing to our own version.
     */
    inline void skel__Assert(const bool expr) {
        if (!expr) {
            /**
             * Call compiler/platform specific break routine. Note: asm int 3
             * is inline assembly to call interrupt 3 which usually
             * programmatically adds a breakpoint (or traps to the debugger)
             */
    #ifdef SKEL___BUILD_COMPILER_MSVC
            /**
             * https://msdn.microsoft.com/en-us/library/45yd4tzz.aspx
             * see also __debugbreak() intrinsic:
             * https://msdn.microsoft.com/en-us/library/f408b4et.aspx
             */
        #ifdef _WIN64
            // inline assembly is not supported for 64-bit architectures
            __debugbreak();
        #else
            __asm int 3;
        #endif // _WIN64
    #elif defined(SKEL___BUILD_COMPILER_GCC)
            // http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
            __asm__("int $3");
    #elif defined(SKEL___BUILD_COMPILER_ICC)
            /**
             * Intel compiler supports both microsoft and gcc style asm tags
             * but recommends gcc/gnu-style
             * https://software.intel.com/en-us/node/513428
             */
            __asm__("int $3");
    #else
    #       error Unrecognized platform
    # endif // compiler check
        }
    }
#endif // override assert

//****************************************************************************
// Macros
//****************************************************************************

// Additional includes for assertion utilities
#ifdef SKEL___BUILD_ENABLE_ASSERT
#   include <cmath>
#   include <limits>
#endif // SKEL___BUILD_ENABLE_ASSERT

/**
 * Asserts that a == b and a > 0 (is positive)
 */
#define skel__AssertPosEq(a, b) skel__Assert(((a) > 0) && ((a) == (b)))

/**
 * Bounds checking that element i is within [l, u)
 */
#define skel__AssertBounds(i_, l_, u_) \
    skel__Assert((((i_) >= (l_)) && ((i_) < (u_))))

/**
 * Bounds checking inclusive that element i is within [l, u]
 */
#define skel__AssertBoundsInc(i_, l_, u_) \
    skel__Assert((((i_) >= (l_)) && ((i_) <= (u_))))

//****************************************************************************
// Macros for floating-point numbers
//****************************************************************************

/**
 * Asserts on floating point equality within a threshold
 */
#define skel__AssertRealEq(a, b) skel__Assert(SKEL___REAL_CMP_EQ(a, b))

/**
 * Asserts on floating point a <= b within a threshold
 */
#define skel__AssertRealLte(a, b) skel__Assert(SKEL___REAL_CMP_LTE((a), (b)))

/**
 * Asserts on floating point a >= b within a threshold
 */
#define skel__AssertRealGte(a, b) skel__Assert(SKEL___REAL_CMP_GTE((a), (b)))

/**
 * Asserts that a == b and a > 0 (is positive) for real values
 */
#define skel__AssertRealPosEq(a, b)              \
    do {                                         \
        skel__Assert((a) > SKEL___TO_REAL(0.0)); \
        skel__AssertRealEq(a, b);                \
    } while (0)

/**
 * Bounds checking that real element i is within [l, u)
 */
#define skel__AssertRealBounds(i_, l_, u_) \
    do {                                   \
        skel__AssertRealGte((i_), (l_));   \
        skel__Assert((i_) < (u_));         \
    } while (0)

/**
 * Bounds checking inclusive that real element i is within [l, u]
 */
#define skel__AssertRealBoundsInc(i_, l_, u_) \
    do {                                      \
        skel__AssertRealGte((i_), (l_));      \
        skel__AssertRealLte((i_), (u_));      \
    } while (0)

//****************************************************************************
// Verify
//****************************************************************************

/**
 * Code within a verify(<code>) block will always be executed even if asserts
 * are disabled. However, if asserts are enabled, it will additionally trigger
 * an assert if code evaluates to a false conditional.
 */
#ifdef SKEL___BUILD_ENABLE_ASSERT
#   define skel__Verify skel__Assert
#else
#   define skel__Verify(expr_) expr_
#endif  // SKEL___BUILD_ENABLE_ASSERT

#endif  // skel__Assert_h
