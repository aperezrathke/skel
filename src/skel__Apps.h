//****************************************************************************
// skel__Apps.h
//****************************************************************************

#ifndef skel__Apps_h
#define skel__Apps_h

#include "skel__Build.h"
#include "skel__CmdOptsMap.h"

/**
 * App prints usage information
 *
 * Usage:
 * -help | -app_help
 */
extern int skel__AppHelpMain(const skel__CmdOptsMap& cmd_opts);

#endif  // skel__Apps_h
