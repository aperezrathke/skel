#!/usr/bin/python
# -*- coding: utf-8 -*-

##############################################################################
# Sets up local workspace by copying template scripts and configurations into
# a local directory which is ignored by source control.
##############################################################################

###########################################
# Imports
###########################################

import os
import shutil
import sys

###########################################
# Variables
###########################################

# Ignore our own script
name_of_this_script = os.path.basename(sys.argv[0])

# Obtain path to template directory
src_template_dir_path = os.path.dirname(os.path.realpath(sys.argv[0]))

# Will create a path to a local workspace directory in parent folder
dst_local_workspace_dir_path = os.path.join(src_template_dir_path, '..', 'local')

###########################################
# Create local workspace
###########################################

# Exit if local workspace already exists
if os.path.exists(dst_local_workspace_dir_path):
    print('Error: Path to local workspace exists already:\n\t' \
        + dst_local_workspace_dir_path)
    print('Please manually remove this existing workspace prior to running this script.')
    exit(0)

# Copy template directory tree to local workspace
print('Creating local workspace.')
print('Copying: ' + src_template_dir_path)
print('\tTo: ' + dst_local_workspace_dir_path)
shutil.copytree(src_template_dir_path, dst_local_workspace_dir_path)

# Remove this script from workspace
os.remove(os.path.join(dst_local_workspace_dir_path,
          name_of_this_script))

print('Local workspace successfully created.')
