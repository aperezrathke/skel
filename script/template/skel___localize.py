#!/usr/bin/python
# -*- coding: utf-8 -*-

##############################################################################
# Replaces all instances of skel__ or SKEL__ with specified project prefix.
# This includes both the file name and the file text. Note, skel__ will be
# case-sensitive replaced. However, SKEL__ will be replaced with upper-case
# version of desired prefix. Prefix must be valid C++ (e.g. - cannot start with
# a number).
#
# Usage:
#
#   python <path_to_this_script> [args]
#
# The following arguments are accepted:
#   -d|--directory <directory_to_process> : specifies the folder to localize
#   -p|--prefix <project_prefix> : specifies the valid C++ project prefix
#   [-nr|--no_recurse] : optional, default is to recursively process
#       subfolders, this disables behavior such that only -d folder will be
#       processed
#   [-nc|--no_cmake] : optional, default is replace all prefixes within
#       CMakesList.txt to new project prefix, this flag disables this behavior
#   [-nclfmt|--no_clang_format]: optional, default is to replace all prefixes
#       within .clang-format to new project prefix, flag disables behavior
##############################################################################

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse
# For parsing file paths, etc
import os
import sys
# For conveniently passing user arguments 
from collections import namedtuple

###########################################
# Globals (read-only)
###########################################

# Ignore this script
SCRIPT_PATH = os.path.realpath(sys.argv[0])

# Directory containing this script
SCRIPT_DIR = os.path.dirname(SCRIPT_PATH)

# SKELeton core prefix
SKEL_PREFIX = "skel__"

# SKELeton core prefix uppercase
SKEL_PREFIX_UPPER = SKEL_PREFIX.upper()

###########################################
# Defaults
###########################################

# Default directory to localize
DEFAULT_DIR = os.path.join(SCRIPT_DIR, "../..")

# Default project prefix
DEFAULT_PREFIX = SKEL_PREFIX

###########################################
# Named tuple(s)
###########################################

# Store user configuration
Config = namedtuple('Config', "prefix, prefix_upper, recurse, cmake, clfmt")

###########################################
# Methods
###########################################

# https://stackoverflow.com/questions/3041986/apt-command-line-interface-like-yes-no-input
# http://code.activestate.com/recipes/577058/
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def localize_file(root, filename, cfg):
    """Localize a single file."""
    filepath = os.path.join(root, filename)
    filename_localized = filename
    if filename.startswith(SKEL_PREFIX):
        filename_no_prefix = filename[len(SKEL_PREFIX):]
        filename_localized = cfg.prefix + filename_no_prefix
    filepath_localized = os.path.join(root, filename_localized)

    # Read file into memory
    contents = ""
    with open(filepath) as fin:
        contents = fin.read()

    # Replace file contents
    print("... creating " + filepath_localized)
    with open(filepath_localized, 'w') as fout:
        contents = contents.replace(SKEL_PREFIX, cfg.prefix)
        contents = contents.replace(SKEL_PREFIX_UPPER, cfg.prefix_upper)
        fout.write(contents)

    # Remove old file if necessary
    if filepath != filepath_localized:
        print("... removing " + filepath)
        os.remove(filepath)

def localize_dir(root, filenames, cfg):
    """Localize files in single directory."""

    # Limit localization to the following file extensions:
    allowed_exts = set(['.bat', '.c', '.cpp', '.cxx', '.h', '.html', '.hpp',
                        '.inl', '.py', '.R', '.sh', '.sln', '.txt', '.vcxproj',
                        '.filters'])

    # Utility to obtain file extension
    ext = lambda f: os.path.splitext(f)[1].lower()

    # Utility returns true if we should process CMakeLists.txt file
    check_cmake = lambda f: (f == "CMakeLists.txt") and cfg.cmake
    # Utility returns true if we should process .clang-format file
    check_clfmt = lambda f: (f == ".clang-format") and cfg.clfmt

    # Process each file in this folder
    for filename in filenames:
        # Check file filters
        clfmt_pass = check_clfmt(filename)
        if (ext(filename) not in allowed_exts) and (not clfmt_pass):
            continue
        if not (filename.startswith(SKEL_PREFIX) or check_cmake(filename) or \
                clfmt_pass):
            continue
        filepath = os.path.join(root, filename)
        if filepath == SCRIPT_PATH:
            continue
        # File meets localization requirements
        print("localizing " + filepath)
        localize_file(root, filename, cfg)

def localize_tree(directory, cfg):
    """Replaces SKELeton prefix conventions with project prefix convention."""
    print('Processing "' + directory + '"')
    if cfg.recurse:
        for root, directories, filenames in os.walk(directory):
            localize_dir(root, filenames, cfg)
    else:
        # Shortening varname just to avoid line greater than 79 chars!
        d = directory
        filenames = \
            [f for f in os.listdir(d) if os.path.isfile(os.path.join(d, f))]
        localize_dir(directory, filenames, cfg)

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print("======================= SKELeton Localizer =======================")

    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory',
        default=DEFAULT_DIR,
        help='Directory to localize')
    parser.add_argument('-p', '--prefix',
        default=DEFAULT_PREFIX,
        help='Valid C++, project-specific prefix')
    parser.add_argument('-nr', '--no_recurse',
        action='store_true',
        default=False,
        help='Avoids recursively localizing subfolders')
    parser.add_argument('-nc', '--no_cmake',
        action='store_true',
        default=False,
        help='Avoids localizing CMakesLists.txt')
    parser.add_argument('-nclfmt', '--no_clang_format',
        action='store_true',
        default=False,
        help='Avoids localizing .clang-format')

    # Parse command line
    args = parser.parse_args()

    # Create 'typed' configuration
    cfg = Config(prefix=args.prefix,
                 prefix_upper=args.prefix.upper(),
                 recurse=not args.no_recurse,
                 cmake=not args.no_cmake,
                 clfmt=not args.no_clang_format)

    # Print command line
    directory = os.path.realpath(args.directory)
    print('\t--directory = ' + directory)
    print('\t--prefix = ' + cfg.prefix)
    print('\t--recurse = ' + str(cfg.recurse))
    print('\t--cmake = ' + str(cfg.cmake))
    print('\t--clfmt = ' + str(cfg.clfmt))

    # Verify user wishes to proceed
    print('\n****** WARNING! ******')
    inform_recurse = ' RECURSIVELY ' if cfg.recurse else ' '
    print('This application will' + inform_recurse + 'modify "' + directory \
        + '" by replacing all instances of prefix "' + SKEL_PREFIX \
        + '" with new prefix "' + cfg.prefix \
        + '". This includes replacement of both file names and text content.\n')

    proceed = query_yes_no("Do you wish to proceed?", "no")

    if proceed:
        localize_tree(directory, cfg)
    else:
        print("No localization was performed.")

    print("Exiting.")

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
