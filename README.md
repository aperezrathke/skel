# README #

SKELeton is a C++ framework primarily for use with scientific applications, but may also be useful for general purpose projects.

Similar to [web frameworks](https://en.wikipedia.org/wiki/Web_framework), the idea is that you develop your application within the ecosystem defined by SKELeton.

![Csppline](Rplot.svg)

## What is this repository for? ###

SKELeton defines the starting "application skeleton". Within this skeleton, you can add your own "apps", "commandlets", and "tests". An app defines a main utility which is typically always compiled. A commandlet is a small accessory utility which may be compiled out if desired. A test is a utility testing some functionality and should always be compiled out of full release builds. 

For a minimal example of how to hook a basic utility into the framework, see skel\_\_TestGlobalInit.h. To see the (configurable) build flags controlling which utilities get compiled, see skel\_\_Build.h.

SKELeton provides source files for:

* low-level portable asserts that can override most compiler/build system configurations if need be
* asserts for working with real-valued numbers
* text parsers for tabular data and run-length encoded tuples
* simple lock free multi-threaded work buffer
* string conversion utilities
* thread-aware random number generator interface
* command line and configuration file parsing
* compiler detection flags
* portable compiler intrinsic headers
* portable isfinite()
* simple, portable timers (wall clock and CPU timers)
* common typedefs and Armadillo matrix library integration
* penalized (smoothed) natural cubic splines similar to Matlab csaps() and R smooth.splines()
* C++ port of GSL's Dirichlet distribution

In general, portable refers to the following compilers: Visual Studio (MSVC), G++, Intel compiler (ICC). MSVC assumes Windows OS. G++ and ICC have been tested under Linux.

## How do I get set up? ###

To begin, simply download the code and add the files to your project. Ideally, this will be done before you have actually created any source files!

The SKEL files are meant to be modified to suit your project needs. For example, the command line parsing utilities are meant to be modified with the command line switches specific to your project.

To localize the files to your project, run the python (2.7) script "skel\_\_\_localize.py" which will replace the "SKEL\_\_" naming prefix with the naming prefix for your project.

This code has dependencies on the following libraries (this list may not be exhaustive):

* [Armadillo](http://arma.sourceforge.net/) with [LAPACK](http://www.netlib.org/lapack/)
* C++11 atomic
* C++11 thread
* C++11 condition variable
* C++11 random

It is assumed that the compiler include and binary paths are properly configured with these libraries. If the C++11 standard libraries are not available, then the corresponding [Boost](http://www.boost.org/) libraries are assumed to be properly configured.

### Windows

A visual studio 2022 project is supplied. It assumes the existence of the following environment variables:

* BOOST\_ROOT - location of boost directory containing headers and binaries

SKELeton provides [LAPACK binaries](https://bitbucket.org/aperezrathke/vs-lapack) compatible with visual studio 2022.

### Linux

CMake build scripts are provided. They have been tested solely under linux.

First, to initialize the build system, run the script skel\_\_\_cmake\_init.sh. Then, run the script skel\_\_\_cmake\_build.sh to compile the resulting configurations. The compiled executables can be found within the CMakeBuild folder.